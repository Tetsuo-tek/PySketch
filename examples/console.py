#!/usr/bin/env pysketch-executor

###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import argparse
import queue
import threading
import ollama

from PySketch.console       import Console
from PySketch.loop          import LoopTimerMode, EventLoop
from PySketch.log           import LogMachine, getGlobalLoggerInstance, escapes, raw, msg, dbg, wrn, err, cri, printPair
from PySketch.fs            import *

###############################################################################
# GLOBAL

LogMachine()
logger = getGlobalLoggerInstance()
logger.enableConsoleLog(False)

l = EventLoop("Console")
console = Console()

parser = argparse.ArgumentParser(description="Robot LLM prompt")
parser.add_argument("sketchfile", help="Sketch program file")
parser.add_argument("--llm-model", default="llama3.2-vision:11b-instruct-q8_0", help="Language model to inference")

args = parser.parse_args()

###############################################################################
# SKETCH

def setup():
    t = 0.050

    l.setFastInterval(t, t*20)
    l.setSlowInterval(0.200)
    l.setTickMode(LoopTimerMode.TIMEDLOOP_RT)

    l.init(False)

    cmdProgs = listDirectories("./commands", onlyDirectories=False)

    '''
    for progFile in cmdProgs:
        if progFile.endswith(".py"):
            console.addCommand(os.path.splitext(progFile)[0])
    '''

    return True

def loop():
    checkCommand()
    l.tick()
    return True

###############################################################################

def main():
    console.exec()

def close():
    pass
    
###############################################################################

def checkCommand():
    global cmdModule

    row = console.get()

    if row is None:
        return

    logger.enableConsoleLog(True)

    print("!!!", row)

    logger.enableConsoleLog(False)
    console.taskDone()

###############################################################################
###############################################################################