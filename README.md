# Sk/PySketch

SpecialK/PySketch is a binding of the [SpecialK](https://gitlab.com/Tetsuo-tek/SpecialK) flow client protocol, also with some useful paradigm, that enables the development and execution of satellites in Python for [SkRobot](https://gitlab.com/Tetsuo-tek/SkRobot).

Sketches are minimal pulsating programs; the `setup()` and `loop()` tick mandatory functions define the sketch concept. All job managed and the programming flow is to be considered always minimal and often asynchronous, to obtain simply but useful tools.

Sk/Python sketches have a well-defined `shebang`, to start automatically from shell:

```python
#!/usr/bin/env pysketch-executor
```

The engine executor will provide to use Python (2 or 3) and the sketch itself to generate a program flow relative to an external satellite for the [FlowNetwork](https://gitlab.com/Tetsuo-tek/SpecialK/-/tree/master/LibSkCore/Core/System/Network/FlowNetwork); PySketch programming flow is freely inspired to [Arduino C++ platform](https://www.arduino.cc/), also basing it on SpecialK point-of-view.

Scketch code can be embedded into other satellite binaries to automate specific collaborative executions through Sk/C++ programs; PySketch code can also be transported to the FlowNetwork to parallelize or remote their execution with temporary instances.

A [sketch template](https://gitlab.com/Tetsuo-tek/pysketch-template) is available, useful to start writing sat-code; some other Satellite [sketch-examples](https://gitlab.com/Tetsuo-tek/SkRobot/-/tree/main/examples) are available from SkRobot repository.

## Installation

```sh
pip install git+https://gitlab.com/Tetsuo-tek/PySketch
```

At this point, perhaps you want install [pck](https://gitlab.com/Tetsuo-tek/pck) to manage packs containing also sketch-examples, other than SkRobot and generically SpecialK related applications/tools/examples.

Enjoy!