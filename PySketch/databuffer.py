###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import struct

class DataBuffer(bytearray):
    def __init__(self, data=bytearray()):
        super().__init__()
        self[:] = data

###############################################################################

    def setByteArray(self, data):
        self[:] = data

###############################################################################

    def setString(self, txt):
        self[:] =(txt.encode("utf-8"))

    def toString(self):
        return self.decode("utf-8")

###############################################################################

    def setUInt8(self, val,  pos=0):
        data, len = self.processData(val)
        self[pos:pos+1] = struct.pack("<{}B".format(len), *data)
 
    def setInt8(self, val, pos=0):
        data, len = self.processData(val)
        self[pos:pos+1] = struct.pack("<{}b".format(len), *data)
 
    def setUInt16(self, val, pos=0):
        data, len = self.processData(val)
        self[pos:pos+2] = struct.pack("<{}H".format(len), *data)
 
    def setInt16(self, val, pos=0):
        data, len = self.processData(val)
        self[pos:pos+2] = struct.pack("<{}h".format(len), *data)
 
    def setUInt32(self, val, pos=0):
        data, len = self.processData(val)
        self[pos:pos+4] = struct.pack("<{}I".format(len), *data)
 
    def setInt32(self, val,  pos=0):
        data, len = self.processData(val)
        self[pos:pos+4] = struct.pack("<{}i".format(len), *data)
 
    def setUInt64(self, val, pos=0):
        data, len = self.processData(val)
        self[pos:pos+8] = struct.pack("<{}Q".format(len), *data)
 
    def setInt64(self, val, pos=0):
        data, len = self.processData(val)
        self[pos:pos+8] = struct.pack("<{}q".format(len), *data)
 
    def setFloat(self, val, pos=0):
        data, len = self.processData(val)
        self[pos:pos+4] = struct.pack("<{}f".format(len), *data)
 
    def setDouble(self, val, pos=0):
        data, len = self.processData(val)
        self[pos:pos+8] = struct.pack("<{}d".format(len), *data)

    def processData(self, data):
        if (type(data) == list):
            return data, len(data)

        return [data], 1

###############################################################################

    def toUInt8(self, n=1, pos=0):
        val = struct.unpack("<{}B".format(n), self[pos:])
        return val

    def toInt8(self, n=1, pos=0):
        val = struct.unpack("<{}b".format(n), self[pos:])
        return val
    
    def toUInt16(self, n=1, pos=0):
        val = struct.unpack("<{}H".format(n), self[pos:])
        return val
        
    def toInt16(self, n=1, pos=0):
        val = struct.unpack("<{}h".format(n), self[pos:])
        return val

    def toUInt32(self, n=1, pos=0):
        val = struct.unpack("<{}I".format(n), self[pos:])
        return val

    def toInt32(self, n=1, pos=0):
        val = struct.unpack("<{}i".format(n), self[pos:])
        return val
    
    def toUInt64(self, n=1, pos=0):
        val = struct.unpack("<{}Q".format(n), self[pos:])
        return val

    def toInt64(self, n=1, pos=0):
        val = struct.unpack("<{}q".format(n), self[pos:])
        return val

    def toFloat(self, n=1, pos=0):
        val = struct.unpack("<{}f".format(n), self[pos:])
        return val

    def toDouble(self, n=1, pos=0):
        val = struct.unpack("<{}d".format(n), self[pos:])
        return val

###############################################################################

    def part(self, start=0, len=-1):
        if end == -1:
            end = len(self)

        return self[start:start+len]

###############################################################################
    
    def isEmpty(self):
        return (self.size() == 0)

    def size(self):
        return len(self)

    def clear(self):
        self[:] = bytearray()

###############################################################################