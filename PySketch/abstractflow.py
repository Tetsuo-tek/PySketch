###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import ctypes
import sys
import os

from abc import ABC, ABCMeta, abstractmethod

from PySketch.socketdevice  import SocketDevice
from PySketch.flowproto     import FlowChanID, FlowChannel_T, FlowCommand, Flow_T, FlowProto, Variant_T
from PySketch.log           import msg, dbg, wrn, err, cri, printPair

###############################################################################

def ProtocolRecvError(sck, msg):
    cri(msg)
    if sck.isConnected():
        sck.disconnect()

###############################################################################

class FlowChannel:
    def __init__(self):
        self.chan_t = FlowChannel_T.ChannelNotValid
        self.chanID = -1
        self.hashID = ""
        self.name = ""
        #self.udm = ""
        self.mime = ""
        self.flow_t = Flow_T.FT_BLOB
        self.t = Variant_T.T_NULL
        self.min = None
        self.max = None
        self.hasHeader = False
        self.isPublishingEnabled = False
        self.header = b""
        self.mpPath = ""

###############################################################################

class AbstractFlow(ABC):

    __metaclass__ = ABCMeta

    def __init__(self, loop):
        self._loop = loop
        self._sck = None
        self._unixPath = ""
        self._tcpAddress = ""
        self._tcpPort = 0
        self._userName = ""
        self._userToken = ""
        self._isAuthorized = False
        self._channelsNames = {}
        self._channelsIndexes = {}
        self._p = FlowProto()

        firstArg = os.path.basename(sys.argv[0])
        
        if firstArg == "pysketch-executor.py" or firstArg == "pysketch-executor":
            nodeName = os.path.basename(sys.argv[1])
            self.setNodeName(nodeName)
        
        else:
            self.setNodeName("NoName")

###############################################################################

    def setNodeName(self, name):
        self._nodeName = name
        dbg("Flow NodeName: {}".format(self._nodeName))

###############################################################################

    def localConnect(self, path):
        if self._sck is not None:
            err("SocketDevice is ALREADY initialized")
            return False
        
        self._sck = SocketDevice()

        if not self._sck.localConnect(path):
            self._sck = None
            err("CANNOT connect to flow-service: {}".format(path))
            return False

        self._unixPath = path
        self._tcpAddress = ""
        self._tcpPort = 0

        self._open()
        return True

    def tcpConnect(self, host, port):
        if self._sck is not None:
            err("SocketDevice is ALREADY initialized")
            return False
        
        self._sck = SocketDevice()

        if not self._sck.tcpConnect(host, port):
            self._sck = None
            err("CANNOT connect to flow-service: {}:{}".format(host, port))
            return False

        self._tcpAddress = host
        self._tcpPort = port
        self._unixPath = ""

        self._open()
        return True

    def _open(self):
        if self.channelsCount() > 0:
            self._resetChannels()

        if self.isUnixSocket():
            self._onOpenUnixSocket()
        else:
            self._onOpenTcpSocket()

        self._p.setup(self._sck)

        self._onOpen()
        dbg("Connected")

    def _resetChannels(self):
        self._channelsNames = {}
        self._channelsIndexes = {}

    def close(self):
        if self._sck is None:
            err("SocketDevice is NOT initialized yet")
            return
        
        self._sck.disconnect()
        self._onClose()
        self._sck = None

        dbg("Close")# < da inserire nel tick all'interno di onDisconnected()

###############################################################################

    def login(self, userName, userToken):
        if (self._sck is None) or (not self._sck.isConnected()):
            err("SocketDevice is NOT initialized yet")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_LOGIN)
        self._p.sendString(userName)
        self._p.sendString(userToken)
        self._p.sendEndOfTransaction()

        self._sck.waitForReadyRead()

        cmd = self._p.recvStartOfCmdTransaction()

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return False

        self._userName = userName
        self._userToken = userToken
        self._isAuthorized = True

        self._onLogin()

        self.setVariable("@nodeName", self._nodeName)
        return True

###############################################################################

    def setCurrentDbName(self, dbName):
        if (self._sck is None) or (not self._sck.isConnected()):
            err("SocketDevice is NOT initialized yet")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_SET_DATABASE)
        self._p.sendString(dbName)
        self._p.sendEndOfTransaction()
        
        #NO RESPONSE
        return True

    def setVariable(self, key, val):
        if (self._sck is None) or (not self._sck.isConnected()):
            err("SocketDevice is NOT initialized yet")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_SET_VARIABLE_JSON)
        self._p.sendString(key)
        self._p.sendJSON(val)
        self._p.sendEndOfTransaction()

        #NO RESPONSE
        return True
    
    def delVariable(self, key):
        if (self._sck is None) or (not self._sck.isConnected()):
            err("SocketDevice is NOT initialized yet")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_DEL_VARIABLE)
        self._p.sendString(key)
        self._p.sendEndOfTransaction()
        
        #NO RESPONSE
        return True

    def flushAll(self):
        if (self._sck is None) or (not self._sck.isConnected()):
            err("SocketDevice is NOT initialized yet")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_FLUSHALL)
        self._p.sendEndOfTransaction()
        
        #NO RESPONSE
        return True

###############################################################################

    def checkService(self):
        if (self._sck is None) or (not self._sck.isConnected()):
            err("SocketDevice is NOT initialized yet")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_CHK_SERVICE)
        self._p.sendEndOfTransaction();

        # NO RESPONSE
        return True

###############################################################################

    def isUnixSocket(self):
        return (len(self._unixPath) > 0)

    def isTcpSocket(self):
        return (self._tcpPort != 0)

    def getUnixPath(self):
        return self._unixPath

    def getTcpAddress(self):
        return self._tcpAddress

    def getTcpPort(self):
        return self._tcpPort

###############################################################################

    def isConnected(self):
        if self._sck is None:
            return False
        
        return self._sck.isConnected()

    def isAuthorized(self):
        return self._isAuthorized

###############################################################################

    def containsChannel(self, name):
        return (name in self._channelsNames)

    def channels(self):
        return list(self._channelsNames.keys())

    def channelByName(self, name):
        try:
            return self._channelsNames[name]

        except KeyError:
            return None

    def channelByID(self, chanID):
        try:
            return self._channelsIndexes[chanID]

        except KeyError:
            return None

    def channelID(self, name):
        if name in self._channelsNames:
            err("Channel UNKNOWN:", name)
            return -1
        
        ch = self._channelsNames[name]
        return ch.chanID

    def channelsCount(self):
        return len(self._channelsIndexes)

###############################################################################

    @abstractmethod
    def _onOpenUnixSocket(self):
        pass

    @abstractmethod
    def _onOpenTcpSocket(self) :
        pass
    
    @abstractmethod
    def _onLogin(self):
        return

    @abstractmethod
    def _onOpen(self):
        pass

    @abstractmethod
    def _onClose(self):
        pass

    @abstractmethod
    def _onDisconnected(self):
        pass

###############################################################################

