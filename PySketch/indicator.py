import os
import sys

###############################################################################

class Indicator():
    def __init__(self):
        self._indicators = ['/', '-', '\\', '|']
        self._indicatorID = 0
        self._checkTerm()

    def _checkTerm(self):
        self._termSz = os.get_terminal_size()
        self._cols = self._termSz.columns
        self._emptyLine = ' ' * (self._cols-1)

    def start(self, label=""):
        sys.stdout.write("\r{}\r".format(self._emptyLine))
        sys.stdout.write("{}".format(label))
        sys.stdout.flush()

    def tick(self, label=""):
        self._indicatorID = (self._indicatorID + 1) % len(self._indicators)

        sys.stdout.write("\r{}\r".format(self._emptyLine))
        sys.stdout.write("[{}] - {}\r".format(self._indicators[self._indicatorID], label))
        sys.stdout.flush()

    def stop(self, label=""):
        sys.stdout.write("{}\r{}\n".format(self._emptyLine, label))
        sys.stdout.flush()

###############################################################################
