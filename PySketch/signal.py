###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import sys
import inspect
import copy

from enum                   import Enum

from PySketch.log           import msg, dbg, wrn, err, cri, printPair

###############################################################################

def functSignature(funct):
    if sys.version_info >= (3, 0):
        return inspect.signature(funct)

    return inspect.getargspec(funct).args

###############################################################################

class Signal():
    def __init__(self, loop):
        self._loop = loop
        self._targets = {}

    def attach(self, target):
        isSignal = (inspect.ismethod(target) and isinstance(target.__self__, Signal))

        tgtPack = {
            "funct" : target,
            "isSignal" : isSignal
        }

        if target in self._targets:
            if isSignal:
                wrn("Signal ALREADY attached: {}".format(target.__self__.__class__.__name__))

            else:
                wrn("Slot NOT found: {}{}".format(target.__name__, functSignature(target)))

            return False

        self._targets[target] = tgtPack

        if isSignal:
            dbg("Signal ATTACHED: {}".format(target.__self__))

        else:
            dbg("Slot ATTACHED: {}{}".format(target.__name__, functSignature(target)))

        return True

    def detach(self, target):
        if target not in self._targets:
            wrn("Target NOT found: {}{}".format(target.__name__, functSignature(target)))
            return False

        isSignal = self._targets[target]["isSignal"]

        del self._targets[target]

        if isSignal:
            dbg("Signal DETACHED: {}".format(target.__self__.__class__.__name__))

        else:
            dbg("Slot DETACHED: {}{}".format(target.__name__, functSignature(target)))

        return True

    def trigger(self, args=None):
        if self.isEmpty():
            return

        cp = self._targets.copy()

        for target, tgtPack in cp.items():
            if args is None:
                target()

            else:
                if tgtPack["isSignal"]:
                    target(args)

                else:
                    if type(args) == list or type(args) == tuple:
                        target(*args)

                    elif type(args) == dict:
                        target(**args)

                    else:
                        err("triggering args UNRECOGNIZED container type: {}".format(type(args)))

    def isEmpty(self):
        return (len(self._targets)==0)

    def count(self):
        return len(self._targets)

    def isAttached(self, target):
        return (target in self._targets)

###############################################################################

def emit(sig, args=[]):
    sig.trigger(args)

###############################################################################
