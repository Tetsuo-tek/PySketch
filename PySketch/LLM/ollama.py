###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import ollama
import inspect
import traceback

from abc                        import ABC, ABCMeta, abstractmethod
from queue                      import Queue
from tqdm                       import tqdm
from httpx                      import RequestError, HTTPStatusError
from pydantic                   import BaseModel, Field
from typing                     import List, Optional
from typing_extensions          import Literal

from PySketch.elapsedtime       import ElapsedTime
from PySketch.signal            import Signal
from PySketch.log               import msg, dbg, wrn, err, cri, printPair

from PySketch.LLM.nlp           import LangProcessor

###############################################################################

class Summary(BaseModel):
    title: str = Field(

        ...,

        description="The title of the summary."
    )

    summary_text: str = Field(

        ...,

        description="The summary text, keeping the original language of the content."
    )

    key_points: Optional[List[str]] = Field(

        None,

        description="An optional list of key points from the original text, in the original language."
    )

    class Config:
        schema_extra = {
            "example": {
                "title": "Summary Title",
                "summary_text": "This is a summary of the provided text...",
                "key_points": ["Key point 1", "Key point 2"]
            }
        }

class Generator(ABC):

    __metaclass__ = ABCMeta

    def __init__(self,  llm):
        self._llm = llm
        self._l = self._llm._l
        self._clnt = self._llm._clnt
        self._stream = None
        self._sysmsg = None
        self._prompt = None
        self._images = None
        self._options = None
        self._termination = False
        self._generated = ""

        self.started    = Signal(self._l)
        self.newToken   = Signal(self._l)
        self.finished   = Signal(self._l)
        self.terminated = Signal(self._l)

    def setSystemMessage(self, sysmsg):
        self._sysmsg = sysmsg

        if self._sysmsg is None:
            dbg("System message RESET")
            return

        dbg("System message SETUP: {}".format(self._sysmsg[:50]))

    def generate(self, model, prompt, images=[], sysmsg=None, options={"temperature": 0}, stream=True):
        self._generated = ""
        self._prompt = prompt
        self._images = images
        self._options = options
        
        args = {"model" : model, "prompt" : self._prompt, "stream" : stream}

        if sysmsg is not None:
            args["system"] = sysmsg

        if len(images) > 0:
            args["images"] = images

        if len(options) > 0:
            args["options"] = options
        
        ret = self._llm.ollamaApiWrapper(wrapper=type(self), api=self._clnt.generate, args=args)

        if ret is not None:
            if not stream:
                return ret["message"]

            self._stream = iter(ret)
            self._l.fastZone.attach(self.tick)
        
            dbg("Generating from prompt: {} ..".format(self._prompt[:50]))
            self.started.trigger([self._prompt])
        
        return (self._stream is not None)

    def _onStreamFinish(self):
        self._l.fastZone.detach(self.tick)
        self.finished.trigger()
        self._stream = None
        self.setSystemMessage(None)

    def tick(self):
        if self._stream is None:
            return False

        if self._termination:
            self.terminated.trigger()
            self._termination = False
            self._onStreamFinish()
            return False

        try:
            part = next(self._stream)
            #dbg("Response stream-part received: {}".format(part))
            
        except ollama._types.ResponseError as e:
            err("TICK - RESPONSE ERROR -> {}".format(e))
            return False

        except StopIteration:
            self._onStreamFinish()
            return False

        tk = ""

        #print("\n!!!!!", part, "\n")

        # FROM chat
        if "message" in part:
            message = part["message"]
            tk = message["content"]

        # FROM generate
        elif "response" in part:
            tk = part["response"]

        if len(tk) == 0:
            return True

        self.newToken.trigger([tk])
        self._generated += tk
        return True

    def terminate(self):
        if not self._termination:
            wrn("Terminating generation ..")
            self._termination = True

    def prompt(self):
        return self._prompt

    def images(self):
        return self._images

    def options(self):
        return self._options

    def sysmsg(self):
        return self._sysmsg

    def llm(self):
        return self._llm

###############################################################################

class Tool():
    def __init__(self, service):
        self._service = service
        self._llm = self._service._llm
        self._l = self._llm._l
        self._funct = None
        self._src = None
        self._name = None
        self._signature = None
        self._arguments = None
        self._lastError = None
        self._lastErrorTrace = None
        self._attachment = None
        
        self.jobErrorIssued = Signal(self._llm._l)

    def load(self, name, funct):
        self._funct = funct
        self._name = name
        self._src = inspect.getsource(self._funct)
        self._signature = inspect.signature(self._funct)
        
        self._arguments = {}

        for name, param in self._signature.parameters.items():
            kind = param.kind
            annotation = param.annotation

            if param.default != inspect.Parameter.empty:
                dflt = param.default
            else:
                dflt = None

            self._arguments[name] = {
                "kind": kind,
                "annotation" : annotation,
                "default": dflt
            }

        return True

    def call(self, args):
        ret = None
        self._lastError = None
        self._lastErrorTrace = None
        self._attachment = None

        dbg("Calling tool: {}({})".format(self._name, args))

        try:
            ret = self._funct(**args)

        except Exception as e:
            self._lastError = e
            self._lastErrorTrace = traceback.format_exc()

            self.jobErrorIssued.trigger([self._name, self._lastError, self._lastErrorTrace])
            err("Tool {} RUNTIME ERROR >>> {} -> {} <<<".format(self._name, self._lastError, self._lastErrorTrace))

            prompt = "The called Tool has issued a RUNTIME-ERROR:\n\n{} -> {}\n\n" \
                "Try to debug the related source code:\n\n{}\n; ".format(self._lastError, self._lastErrorTrace, self._src)

            self._service.generate(prompt=prompt)
            return None

        return ret

    def name(self):
        return self._name

    def funct(self):
        return self._funct

    def arguments(self):
        return self._arguments

    def toString(self):
        return self._src

    def hasAttachment(self):
        return (self._attachment is not None)

    def setAttachment(self, data):
        self._attachment = data
        dbg("Added attachment to the job")

    def attachment(self):
        return self._attachment

    def hasError(self):
        return (self._lastErrorTrace is not None)

    def lastErrorTrace(self):
        return self._lastErrorTrace

    def lastError(self):
        return self._lastError

###############################################################################

class ToolService(Generator):
    def __init__(self, llm):
        super(ToolService, self).__init__(llm)

        self._toolsDeclare = []
        self._toolCalls = None
        self._toolCallsIterable = None
        self._toolRets = None
        self._remainingJobs = 0
        self._tools = {}

        self.callsPrepared      = Signal(self._l)
        self.jobExecStarted     = Signal(self._l)
        self.jobExecFinished    = Signal(self._l)
        self.jobResultIssued    = Signal(self._l)
        self.jobErrorIssued     = Signal(self._l)
        self.callsFinished      = Signal(self._l)

    def declare(self, toolDeclares, toolInits):
        self._toolsDeclare = list(toolDeclares.values())

        for name, funct in toolDeclares.items():
            if name in self._tools:
                err("The Tool ALREADY inserted: {}".format(name))
                return False

            tool = Tool(self)

            if not tool.load(name, funct):
                return False

            tool.jobErrorIssued.attach(self.jobErrorIssued.trigger)

            self._tools[tool.name()] = tool
            dbg("Tool ADDED: {}({})".format(tool.name(), tool.arguments()))
        
        for init in toolInits:
            init({
                "loop" : self._l,
                "llm" : self._llm,
                "chatter" : self
            })

        return True

    def reset(self):
        self._toolCalls = None
        self._toolCallsIterable = None
        self._toolRets = None
        self._remainingJobs = 0

    def invoke(self, args):
        self.reset()

        args["tools"] = self._toolsDeclare
        args["stream"] = False

        ret = self._llm.ollamaApiWrapper(wrapper=type(self), api=self._clnt.chat, args=args)

        if ret is None:
            return False

        message = ret["message"]

        if "tool_calls" in message:
            self._toolCalls = message["tool_calls"]
            self._toolCallsIterable = iter(self._toolCalls)
            self._toolRets = {}
            self._remainingJobs = len(self._toolCalls)

            self.callsPrepared.trigger([self._toolCalls])
            return True

        if self.hasTools():
            wrn("Tools ARE available, but anyone is called")
            
        return True

    def next(self):
        if self._toolCallsIterable is None:
            return

        toolName = None
        callingTool = None

        self._remainingJobs -= 1

        try:
            callingTool = next(self._toolCallsIterable)
            toolName = callingTool.function.name
            
        except StopIteration:
            self.callsFinished.trigger()
            self._toolCallsIterable = None
            return

        if not self.exists(toolName):
            err("Tool NOT found: {}".format(toolName))
            return

        tool = self._tools[toolName]
        callingArgs = callingTool.function.arguments
        self.jobExecStarted.trigger([tool.name(), callingArgs])
        
        ret = tool.call(callingArgs)
        
        self.jobExecFinished.trigger([tool.name()])

        if ret is None:
            if tool.hasError():
                err("Tools returned 'None' with errors")

        else:
            self._toolRets[tool.name()] = ret
            dbg("Tool has returned [type: {}]".format(type(ret)))
            
        self.jobResultIssued.trigger([tool.name(), ret])

    def hasTools(self):
        return (len(self._tools) > 0)

    def count(self):
        return len(self._tools)

    def declaration(self):
        return self._toolsDeclare

    def exists(self, name):
        return (name in self._tools)
        
    def tool(self, name):
        if not self.exists(name):
            err("Tool NOT found: {}".format(name))
            return None

        return self._tools[name]

    def hasJobs(self):
        return (self._remainingJobs > 0)

    def remainingJobs(self):
        return self._remainingJobs

    def jobsResults(self):
        return self._toolRets

    def currentCalls(self):
        return self._toolCalls

###############################################################################

class PromptProperties(BaseModel):

    english_version: str = Field(
        ...,

        title = "English Translation of the Prompt",

        description = (
            "The mandatory English translation of the input Prompt. This field must contain the translated version "
            "of the input Prompt, which the system is expected to process. For instance, given a non-English Prompt, "
            "the system should translate it to English and store the result in this field."
        ),

        example = "This is a translated version of the input Prompt."
    )

    topics: List[str] = Field(
        ...,

        title = "Topics from the Prompt",

        description = (
            "A required list of topics that are exactly extracted from the input Prompt and translated in engish. "
            "The system should extract the **exact words** used in the original Prompt, translated in english language. "
            "No new concepts or paraphrasing should be generated; only words or phrases exactly as they appear in the Prompt. "
            "For example, if the Prompt contains 'machine learning' and 'artificial intelligence' (in the original language), "
            "these exact terms should be included as topics, translated in english, without any alteration or generation of new related terms."
        ),

        example = ["machine learning", "artificial intelligence", "neural networks"]
    )

'''
intent: Literal[
        "introduction", "summary_explanation", "in_depth_explanation", "deep_lecture", "judgment", 
        "comparison", "classification", "step_by_step_reasoning", "complaint_about_errors", "congratulations"
    ] = Field(

    ...,

    title="Specific Intent of the Prompt",

    description=(
        "This field represents the precise intent of the user's input Prompt as determined by the system. "
        "The intent must be inferred by analyzing the linguistic structure, vocabulary, and context of the Prompt. "
        "Each possible intent has a defined purpose and level of detail:\n\n"
        "- **introduction**: Requests a brief overview or basic explanation of a topic.\n"
        "- **summary_explanation**: Seeks a condensed yet comprehensive explanation of a topic or issue.\n"
        "- **in_depth_explanation**: Requires a detailed explanation of a specific concept or topic, possibly involving technical or nuanced aspects.\n"
        "- **deep_lecture**: Requests a lecture-style explanation with significant depth, possibly requiring examples, applications, and theoretical background.\n"
        "- **judgment**: Asks for an evaluative or critical response.\n"
        "- **comparison**: Requests a direct comparison between two or more items, concepts, or ideas.\n"
        "- **classification**: Involves categorizing a concept, topic, or entity into predefined groups.\n"
        "- **step_by_step_reasoning**: Requires a systematic, logical breakdown of a problem or process.\n"
        "- **complaint_about_errors**: Highlights an error or issue, seeking clarification or a solution.\n"
        "- **congratulations**: Conveys positive feedback, praise, or acknowledgment."
    ),

    examples={
        "introduction": "What is quantum computing?",
        "summary_explanation": "Can you summarize the benefits of machine learning in healthcare?",
        "in_depth_explanation": "Explain the differences between supervised and unsupervised learning in detail.",
        "deep_lecture": "Teach me about the Fourier Transform as if I were a graduate student.",
        "judgment": "Is nuclear energy a better option than solar energy?",
        "comparison": "Compare Python and JavaScript for web development.",
        "classification": "What type of machine learning algorithm is a decision tree?",
        "step_by_step_reasoning": "Explain step by step how to solve a linear equation.",
        "complaint_about_errors": "Why does the software crash when I upload a large file?",
        "congratulations": "Great job on the new feature implementation!"
    }
)
'''


'''
language: Literal[
    "en", "es", "fr", "de", "it", "pt", "ru", "zh", "ja", "ko", 
    "ar", "hi", "bn", "pa", "jv", "ms", "te", "th", "tr", "vi", 
    "pl", "uk", "ro", "el", "ne", "he", "ta", "ml"
] = Field(
    ...,

    title = "Language of the Prompt",

    description = (
        "The language of the original input Prompt. This field must be one of the following languages: "
        "English (en), Spanish (es), French (fr), German (de), Italian (it), Portuguese (pt), Russian (ru), "
        "Chinese (zh), Japanese (ja), Korean (ko), Arabic (ar), Hindi (hi), Bengali (bn), Punjabi (pa), "
        "Javanese (jv), Malay (ms), Telugu (te), Thai (th), Turkish (tr), Vietnamese (vi), "
        "Polish (pl), Ukrainian (uk), Romanian (ro), Greek (el), Nepali (ne), Hebrew (he), Tamil (ta), "
        "Malayalam (ml)."
    ),

    example = "it"
)
'''


class PromptAnalyzer():
    def __init__(self, chatter):
        self._chatter = chatter

        self._prompt = None
        self._lang = None

        self._nlp = LangProcessor()
        
    def init(self, nlpModel="en_core_web_trf"):
        if not self._nlp.load(nlpModel):
            err("CANNOT init Spacy")
            return False

        return True

    def analyze(self, model, prompt):
        self._prompt = prompt

        propsPrompt = "Extract the following requested fields: '{}'".format(prompt)
        
        self._nlp.parse(self._prompt)
        self._originalSentences = self._nlp.splitToSentences()
        self._lang = self._nlp.detectedLanguage()

        self._preProcessPack = self._chatter.structure(model=model, prompt=propsPrompt, classModel=PromptProperties)
        
        self._nlp.parse(self._preProcessPack.english_version)
        self._englishSentences = self._nlp.splitToSentences()
        self._keywords = self._nlp.grabTokens()
        self._entities = self._nlp.extractEntities()

        #self._originalSentences = self._nlp.splitToSentences(self._prompt)
        #self._englishSentences = self._nlp.splitToSentences(self._preProcessPack.english_version)

    def lang(self):
        return self._lang # preProcessPack.language

    def topics(self):
        return self._preProcessPack.topics

    def originalVersion(self):
        return self._prompt
    
    def originalSentences(self):
        return self._originalSentences.copy()

    def englishVersion(self):
        return self._preProcessPack.english_version

    def englishSentences(self):
        return self._englishSentences.copy()

    def keywords(self):
        return self._keywords

    def entities(self):
        return self._entities

###############################################################################

class Chatter(ToolService):
    def __init__(self, llm):
        super(Chatter, self).__init__(llm)
        self._m = None
        self._history = Queue()

        self.finished.attach(self._onGenerationFinish)
        self.callsPrepared.attach(self._onToolCallsPrepare)
        self.jobResultIssued.attach(self._onToolResult)

    def addToHistory(self, role, data):
        message = {"role": role}
        message.update(data)
        self._history.put(message)
        
    def _buildHistoryMessages(self, lastMessage):
        while self._history.qsize() > 6:
            self._history.get()
            # SUMMARIZING THE OLD GOTTEN MESSAGES
            
        messages = None

        if self._sysmsg is not None:
            messages = []
            messages.extend(self.history())
            messages.append({"role": "system", "content": self._sysmsg})

        else:
            messages = self.history()

        messages.append(lastMessage)
        return messages

    def structure(self, model, prompt, classModel, images=[], options={}):
        dbg("Structuring prompt [{}]: {} ..".format(model, classModel.__name__))

        message = {"role": "user", "content": prompt}

        if len(images) > 0:
            message["images"] = images

        args = {"model" : model, "format" : classModel.model_json_schema(), "messages" : self._buildHistoryMessages(message), "stream" : False}
        
        if len(options) > 0:
            args["options"] = options
        
        ret = None
        i = 0

        while ret is None:
            ret = self._llm.ollamaApiWrapper(wrapper=type(self), api=self._clnt.chat, args=args)
            i += 1

            if i == 3:
                err("CANNOT structure prompt: {}".format(prompt))
                return None

        return classModel.model_validate_json(ret.message.content)

    def chat(self, model, prompt, images=[], options={}, bypassTools=False):
        self._m = model
        self._generated = ""
        self._prompt = prompt
        self._images = images
        self._options = options

        dbg("Chat from prompt [{}]: {} ..".format(model, self._prompt[:50]))

        message = {"role": "user", "content": self._prompt}
            
        if len(self._images) > 0:
            message["images"] = self._images

        args = {"model" : model, "messages" : self._buildHistoryMessages(message), "stream" : True}

        if len(self._options) > 0:
            args["options"] = self._options
        
        # HERE WE ARE CHECKING AND EVENTUALLY EXECUTING
        if not bypassTools and self.hasTools():
            wrn("Token stream is disabled because tools are available")
            
            if not self.invoke(args):
                return False

            if self.hasJobs():
                self._l.invokeSlot(self.next)
                return True
            
            return self.chat(model=model, prompt=self._prompt, images=self._images, options=self._options, bypassTools=True)

        ret = self._llm.ollamaApiWrapper(wrapper=type(self), api=self._clnt.chat, args=args)
    
        if ret is None:
            return False

        self.started.trigger([self._prompt])
        self._stream = iter(ret)
        self._l.fastZone.attach(self.tick)
        self._history.put(message)

        return True

    def _onToolCallsPrepare(self, toolCalls):
        self.addToHistory(role="assistant", data={"tool_calls": toolCalls})

    def _onToolResult(self, name, result):
        self.addToHistory(role="tool", data={"content": "{} -> {}".format(name, result)})

        if self.hasJobs():
            self._l.invokeSlot(self.next)
        
        else:
            # IT IS REDOUNDANT, BUT COULD BE USEFUL ON SOME LLM
            sysmsg = "You excel at contextualizing, summarizing and presenting various types of content and results; " \
                "you delve into details, avoiding adding non-existent information, providing relevant and consistent " \
                "information related to the data being analyzed.\n\n" \
                "[important] When results are composed of a list of information items, examine and summarize the content, item by item and step by step, " \
                "avoiding to overlook any details within the data. \n\n" \
                "If elements contain URLs, always notify them precisely and punctually, using Markdown syntax so that they can be clicked.\n\n" \
                "In the following structured data there are the last returned values and informations by tool-calls:\n\n{}\n\n" \
                "[important] Reporting data to the user requires to omit all raw data structural details, " \
                "answering with the same language of the user request.".format(self.jobsResults())

            self.setSystemMessage(sysmsg)
            self.chat(model=self._m, prompt=self._prompt, images=self._images, options=self._options, bypassTools=True)
            self.setSystemMessage(None)

    def _onGenerationFinish(self):
        self.addToHistory(role="assistant", data={"content": self._generated})

    def history(self):
        return list(self._history.queue)
        
    def clear(self):
        self._history = Queue()

###############################################################################
###############################################################################

class Puller():
    def __init__(self, llm):
        self._llm = llm
        self._l = llm._l
        self._m = None
        self._stream = None
        self._currDigest = ""
        self._bars = {}
        self._clnt = llm._clnt

    def get(self, model):
        self._m = model
        
        msg("Pulling model: {} ..".format(self._m))

        args = {"model" : self._m, "stream" : True}
        ret = self._llm.ollamaApiWrapper(wrapper=type(self), api=self._clnt.pull, args=args)

        if ret is not None:
            self._stream = iter(ret)
            
        return (self._stream is not None)

    def tick(self):
        try:
            progress = next(self._stream)
            digest = progress.get("digest", "")

            if digest != self._currDigest and self._currDigest in self._bars:
                self._bars[self._currDigest].close()

            if not digest:
                print(progress.get("status"))
                return True

            if digest not in self._bars and (total := progress.get("total")):
                self._bars[digest] = tqdm(total=total, desc="pulling {}".format(digest[7:19]), unit="B", unit_scale=True)

            if completed := progress.get("completed"):
                self._bars[digest].update(completed - self._bars[digest].n)

            self._currDigest = digest

        except ollama._types.ResponseError as e:
            err("PULLER - RESPONSE ERROR -> {}".format(e))
            return False

        except StopIteration:
            wrn("Pulling FINISHED: {} ..".format(self._m))
            return False

        return True

###############################################################################
###############################################################################

class Ollama():
    def __init__(self, loop, addr="127.0.0.1", port=11434):
        self._l = loop
        self._host = "http://{}:{}".format(addr, port)

        msg("Testing Ollama-server connection: {} ..".format(self._host))

        self._clnt = ollama.Client(host=self._host)
        self._l.fastZone.attach(self.tick)

    def ollamaApiWrapper(self, wrapper, api, args={}):
        ret = None

        try:
            ret = api(**args)

        except RequestError as e:
            err("{} - Connection ERROR -> {}".format(wrapper, e))
            return None

        except HTTPStatusError as e:
            err("{} - HTTP ERROR -> {}".format(wrapper, e))
            return None

        except ollama._types.ResponseError as e:
            err("{} - RESPONSE ERROR -> {}".format(wrapper, e))
            return None

        except Exception as e:
            err("{} - ERROR -> {}".format(wrapper, e))
            return None
        
        return ret

###############################################################################

    def exists(self, model):
        return (model in self.models())

###############################################################################

    def remove(self, model):
        args = {"model" : model}
        return (self.ollamaApiWrapper(wrapper=type(self), api=self._clnt.delete, args=args) is not None)

###############################################################################

    def embed(self, model, texts):
        dbg("Embedding text [{}] ..".format(model))
        
        #print("!!!!", texts)
        if type(texts) == list:
            for txt in texts:
                if len(txt) == 0:
                    return []

        elif type(texts) == str:
            if len(texts) == 0:
                return []

            texts = texts.lower()

        args = {"model" : model, "input" : texts}
        ret = self.ollamaApiWrapper(wrapper=type(self), api=self._clnt.embed, args=args)

        if ret is None:
            return []

        return ret["embeddings"]

###############################################################################

    def processes(self):
        response = self.ollamaApiWrapper(wrapper=type(self), api=self._clnt.ps)
        
        if response is None:
            return {}

        models = {}

        for model in response.models:
            name = model.model
            m =  {
                "name" : name,
                "digest" : model.digest,
                "expires" : model.expires_at,
                "size" : model.size,
                "vram" : model.size_vram,
                "details" : model.details
            }

            models[name] = m

        return models

    def models(self):
        response = self.ollamaApiWrapper(wrapper=type(self), api=self._clnt.list)

        if response is None:
            return {}

        models = {}

        for model in response.models:
            name = model.model

            m =  {
                "name" : name,
                "size" : model.size.real
            }

            if model.details:
                m["format"] = model.details.format
                m["family"] = model.details.family
                m["params"] = model.details.parameter_size
                m["q"] = model.details.quantization_level

            else:
                m["format"] = "NONE"
                m["family"] = "NONE"
                m["params"] = "NONE"
                m["q"] = "NONE"

            models[name] = m

        return models

###############################################################################

    def puller(self):
        return Puller(self)

    def generator(self):
        return Generator(self)

    def chatter(self):
        return Chatter(self)

###############################################################################

    def tick(self):
        pass

###############################################################################

    def url(self):
        return self._host

###############################################################################

    def loop(self):
        return self._l

###############################################################################
