import sys
import spacy
import subprocess

from langdetect                     import detect
from collections                    import Counter

from PySketch.log                   import msg, dbg, wrn, err

class LangProcessor():
    def __init__(self):
        self._modelName = None
        self._lang = None
        self._nlp = None
        self._doc = None

    def _installSpacyModel(self, modelName):
        try:
            subprocess.run([sys.executable, "-m", "spacy", "download", modelName], check=True)
            dbg("Spacy model successflully INSTALLED: {}".format(modelName))

        except subprocess.CalledProcessError:
            err("Spacy model installing ERROR: {}".format(modelName))
            return False
            
        return True

    def _loadSpacyModel(self, modelName):
        nlp = None

        try:
            nlp = spacy.load(modelName)
            dbg("Spacy model successflully LOADED: {}".format(modelName))

            if "sentencizer" not in nlp.pipe_names:
                nlp.add_pipe("sentencizer", before="parser")
                dbg("Sentencizer ADDED to pipeline")

        except OSError:
            err("Spacy model loading ERROR: {}".format(modelName))
            return None

        return nlp

    def load(self, modelName="en_core_web_trf"):
        if self._nlp is not None:
            dbg("Spacy model is ALREADY loaded: {}".format(self._modelName))
            return True

        nlp = self._loadSpacyModel(modelName)

        if nlp is None:
            if not self._installSpacyModel(modelName):
                return False
            
            nlp = self._loadSpacyModel(modelName)
        
        if nlp is None:
            return False
            
        self._modelName = modelName
        self._nlp = nlp

        dbg("Spacy model is READY: {}".format(self._modelName))
        return True

    def isLoaded(self):
        return (self._nlp is not None)

    def parse(self, txt):
        if not self.isLoaded():
            err("Model is NOT open")
            return None

        try:
            self._lang = detect(txt)
            
        except Exception as e:
            err("Language detection error: {}".format(e))

        self._doc = self._nlp(txt)
        return self._doc

    def splitToSentences(self):
        if not self.isLoaded():
            err("Model is NOT open")
            return None

        sentences = [sent.text for sent in self._doc.sents]
        dbg("Sentences PARSED: {}".format(sentences))
        return sentences

    def grabTokens(self, filterStopWords=True, filterPunctuation=True):
        if not self.isLoaded():
            err("Model is NOT open")
            return None

        tokens = [
            token.text.lower()
            for token in self._doc
            if (not filterStopWords or not token.is_stop)
            and (not filterPunctuation or not token.is_punct)
        ]

        dbg("Text TOKENIZED: {}".format(tokens))
        return tokens

    def tokenSimilarity(self, token1_index, token2_index):
        if not self.isLoaded():
            err("Model is NOT open")
            return None

        token1 = self._doc[token1_index]
        token2 = self._doc[token2_index]

        similarity = token1.similarity(token2)

        dbg("Token similarity CALCULATED between '{}' and '{}': {}".format(token1.text, token2.text, similarity))
        return similarity

    def grabTopicWords(self, types=["NOUN"]):
        if not self.isLoaded():
            err("Model is NOT open")
            return None

        words = set()

        for token in self._doc:
            if token.pos_ in types: # or token.pos_ == "PROPN":
                words.add(token.text)

        return list(words)

    def extractPosTags(self):
        if not self.isLoaded():
            err("Model is NOT open")
            return None

        pos_tags = [(token.text, token.pos_, token.tag_) for token in self._doc]
        dbg("POS tags EXTRACTED: {}".format(pos_tags))
        return pos_tags

    def extractNounChunks(self):
        if not self.isLoaded():
            err("Model is NOT open")
            return None

        noun_chunks = [chunk.text for chunk in self._doc.noun_chunks]
        dbg("Noun chunks EXTRACTED: {}".format(noun_chunks))
        return noun_chunks

    def extractEntities(self):
        if not self.isLoaded():
            err("Model is NOT open")
            return None

        entities = [(ent.text, ent.label_) for ent in self._doc.ents]
        dbg("Entities EXTRACTED: {}".format(entities))
        return entities

    def analyzeDependencies(self):
        if not self.isLoaded():
            err("Model is NOT open")
            return None

        dependencies = [(token.text, token.dep_, token.head.text) for token in self._doc]
        dbg("dependencies ANALYZED: {}".format(dependencies))
        return dependencies

    def lemmatizeText(self):
        if not self.isLoaded():
            err("Model is NOT open")
            return None

        lemmas = [(token.text, token.lemma_) for token in self._doc]
        dbg("Lemmas EXTRACTED: {}".format(lemmas))
        return lemmas

    def extractStopWords(self):
        if not self.isLoaded():
            err("Model is NOT open")
            return None

        stop_words = [token.text for token in self._doc if token.is_stop]
        dbg("Stop words EXTRACTED: {}".format(stop_words))
        return stop_words

    def extractRelativeClauses(self):
        if not self.isLoaded():
            err("Model is NOT open")
            return None

        relative_clauses = [(token.text, token.dep_) for token in self._doc if token.dep_ == "relcl"]
        dbg("Relative clauses EXTRACTED: {}".format(relative_clauses))
        return relative_clauses

    def detectedLanguage(self):
        return self._lang


    # MUST REVIEW
    '''
    def extractTopics(self, txt):
        lang = detect(txt)
        if not self._spcy.load():
            return None, None

        words = re.findall(r'\b\w+\b', txt)
        wordCounts = Counter(words)
        mostCommonWords = wordCounts.most_common(40)

        mostCommonWords = [word for word, count in mostCommonWords]
        topics = self._spcy.grabTopicWords(" ".join(mostCommonWords))

        return mostCommonWords, topics
    '''

    '''
        mostCommonWords, topics = self.extractTopics(prompt)

        tokens = self._spcy.extractNer(prompt_EN)
        topics = self._spcy.grabTopicWords(prompt_EN)
        posTags = self._spcy.extractPosTags(prompt_EN)
        nounChunks = self._spcy.extractNounChunks(prompt_EN)
        deps = self._spcy.analyzeDependencies(prompt_EN)
        ner = self._spcy.extractNer(prompt_EN)
        
        lemmas = self._spcy.lemmatizeText(prompt_EN)
        stopWords = self._spcy.extractStopWords(prompt_EN)
        relative_clauses = self._spcy.extractRelativeClauses(prompt_EN)

        print("tokens:", tokens)
        print("topics", topics)
        print("posTags", posTags)
        print("nounChunks", nounChunks)
        print("deps", deps)
        print("ner:", ner)
        print("sentences:", sentences)
        print("Lemmas:", lemmas)
        print("stopWords:", stopWords)
        print("relative_clauses:", relative_clauses)
    '''