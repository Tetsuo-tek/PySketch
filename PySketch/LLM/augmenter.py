from abc                        import ABC, ABCMeta, abstractmethod
from queue                      import Queue

from PySketch.signal            import Signal
from PySketch.fs                import *
from PySketch.elapsedtime       import ElapsedTime
from PySketch.database          import Database, Table, Query
from PySketch.store             import ChromaDB
from PySketch.log               import msg, dbg, wrn, err, cri, printPair

from PySketch.LLM.nlp           import LangProcessor

###############################################################################
# LANGCHAIN AND OTHERs

import pymupdf4llm

from markitdown import MarkItDown

from langchain_community.document_loaders import (
    UnstructuredMarkdownLoader,
    WebBaseLoader,
    UnstructuredURLLoader,
    NewsURLLoader,
    UnstructuredHTMLLoader,
    BSHTMLLoader
)

from langchain_text_splitters   import (
    HTMLHeaderTextSplitter,
    HTMLSectionSplitter,
    RecursiveCharacterTextSplitter
)

from bs4 import BeautifulSoup #,Tag


###############################################################################

class AbstractEmbedder(ABC):

    __metaclass__ = ABCMeta

    def __init__(self, service):
        self._service = service
        self._nlp = self._service.nlp()
        self._dbstore = self._service.sqlDB()

        self._rid = None

        self._contentTable = None
        #self._entityTable = None
        #self._contentEntityTable = None
        self._chunkTable = None

        self._blocks = Queue()

    def _init(self):  
        self._contentTable = Table(self._dbstore, "Content")
        #self._entityTable = Table(self._dbstore, "Entity")
        #self._contentEntityTable = Table(self._dbstore, "ContentEntity")
        self._chunkTable = Table(self._dbstore, "Chunk")

        #return (self._contentTable is not None and self._entityTable is not None and self._contentEntityTable is not None and self._chunkTable is not None)
        return (self._contentTable is not None and self._chunkTable is not None)

    def _buildChunks(self, metadatas, chunks):
        self._dbstore.begin()

        for i, (metadata, chunk) in enumerate(zip(metadatas, chunks)):
            chunk = chunk.strip()
            
            if len(chunk) == 0:
                continue

            if not self._chunkTable.insert(["idContent", "seq", "data"], (self._rid, i, chunk)):
                self._dbstore.rollback()
                return False

            chunkID = self._chunkTable.lastID()

            eid = "{}-{}-{}".format(self._rid, chunkID, i)
            metadata.update({"eid" : eid})

            self._blocks.put({
                "eid" : eid,
                "meta" : "{}".format(metadata),
                "contentID" : self._rid,
                "chunkID" : chunkID,
                "chunk" : chunk
            })

        self._dbstore.commit()
        dbg("'Content [rid={}]' embeddings-write sequence PREPARED: {} items".format(self._rid, len(chunks)))

    def _store(self, src, data):
        self._dbstore.begin()

        if not self._contentTable.insert(["source", "data"], (src, data)):
            self._dbstore.rollback()
            self._rid = None
            return False

        self._rid = self._contentTable.lastID()

        '''
        self._nlp.parse(data)
        self._entities = self._nlp.extractEntities()

        if len(self._entities) == 0:
            err("Content entities NOT found")
            self._dbstore.rollback()
            return False

        ignoreEntityTypes = ["CARDINAL", "ORDINAL", "DATE", "TIME", "QUANTITY"]

        for entity in self._entities:
            if entity[1] in ignoreEntityTypes:
                continue

            entityID = None

            if self._entityTable.exists(where="text=?", values=(entity[0],)):
                q = Query(self._dbstore)
                
                if not q.exec(query="SELECT id FROM Entity WHERE text=?", values=(entity[0],)):
                    return False

                entityID = q.next()["id"]
                
            else:   
                if not self._entityTable.insert(["text", "type"], (entity[0], entity[1])):
                    self._dbstore.rollback()
                    return False
                
                entityID = self._entityTable.lastID()

            if not self._contentEntityTable.insert(["idContent", "idEntity"], (self._rid, entityID)):
                self._dbstore.rollback()
                return False
        '''

        self._dbstore.commit()

        dbg("Content [rid={}] STORED".format(self._rid))
        return True

    def _splitMarkdownFromFile(self, filePath):
        mdLoader = UnstructuredMarkdownLoader(filePath, mode="elements")
        documents = mdLoader.load()

        if len(documents) == 0:
            err("It has NOT relevant data: {}".format(filePath))
            return None

        dbg("Mardown file SPLITTED: {} documents".format(len(documents)))
        return documents

    def _splitMarkdownFromData(self, data):
        md5 = md5Digest(data.encode())
        tempFileName = "{}.md".format(md5)

        dbg("Saving Markdown buffer on file: {}".format(tempFileName))

        if not writeFile(tempFileName, data, mode=""):
            return None

        documents = self._splitMarkdownFromFile(tempFileName)
        removeFile(tempFileName)        

        return documents
    
    def _joinDocuments(self, documents):
        document = ""

        for d in documents:
            document = "{}\n\n".format(d.page_content)

        return document

    def _splitDocuments(self, documents, chunkSize=1000, chunkOverlap=100):
        textSplitter = RecursiveCharacterTextSplitter(chunk_size=chunkSize, chunk_overlap=chunkOverlap)
        documents = textSplitter.split_documents(documents)

        metadatas = []
        chunks = []

        for d in documents:
            if len(d.page_content) <= 30:
                continue

            chunks.append(d.page_content)
            metadatas.append(d.metadata)
        
        dbg("Content [rid={}] CHUNKED: {} chunks".format(self._rid, len(chunks)))
        return (metadatas, chunks)

    def count(self):
        return self._blocks.qsize()

    def isEmpty(self):
        return self._blocks.empty()

    def blocks(self):
        return list(self._blocks.queue)

    def rid(self):
        return self._rid

###############################################################################

class MarkdownSplitter(AbstractEmbedder):
    def __init__(self, service):
        super(MarkdownSplitter, self).__init__(service)
        
    def split(self, filePath, chunkSize=1000, chunkOverlap=100):
        if not self._init():
            return False

        dbg("Splitting Markdown document: {} ..".format(filePath))
        documents = self._splitMarkdownFromFile(filePath)

        if documents is None:
            return False

        md = self._joinDocuments(documents)
        metadatas, chunks = self._splitDocuments(documents)

        if len(chunks) == 0:
            err("It has NOT relevant data: {}".format(title))
            return False

        if not self._store(filePath, md):
            return False

        self._buildChunks(metadatas, chunks)
        return True

###############################################################################

class GenericSplitter(AbstractEmbedder):
    def __init__(self, service):
        super(GenericDocument, self).__init__(service)
        
    def split(self, filePath, chunkSize=1000, chunkOverlap=100):
        if not self._init():
            return False

        fileName = baseName(filePath)
        #md5 = md5Digest(filePath)
        suffix = pathNameSuffix(filePath)

        dbg("Splitting '{}' document: {} ..".format(suffix, filePath))
        
        markitdown = MarkItDown()
        result = markitdown.convert(filePath)

        documents = self._splitMarkdownFromData(result.text_content)

        if documents is None:
            return False
        
        md = self._joinDocuments(documents)
        metadatas, chunks = self._splitDocuments(documents)

        if len(chunks) == 0:
            err("It has NOT relevant data: {}".format(title))
            return False

        if not self._store(filePath, md):
            return False

        self._buildChunks(metadatas, chunks)
        return True

###############################################################################

class PdfSplitter(AbstractEmbedder):
    def __init__(self, service):
        super(PdfSplitter, self).__init__(service)
        
    def split(self, filePath, chunkSize=1000, chunkOverlap=100):
        if not self._init():
            return False

        dbg("Splitting PDF document: {} ..".format(filePath))

        fileName = baseName(filePath)
        md5 = md5Digest(filePath)

        pages = pymupdf4llm.to_markdown(
            doc=filePath,   
            #pages=[0, 2],
            page_chunks=True,
            #write_images=True,
            #image_path="input",
            #image_format="jpg",
            #dpi=300
        )

        md = ""

        for page in pages:
            md += "{}\n\n".format(page["text"])

        documents = self._splitMarkdownFromData(md)

        if documents is None:
            return False

        metadatas, chunks = self._splitDocuments(documents)

        if len(chunks) == 0:
            err("It has NOT relevant data: {}".format(title))
            return False

        if not self._store(filePath, md):
            return False

        self._buildChunks(metadatas, chunks)
        return True

###############################################################################

class HtmlSplitter(AbstractEmbedder):
    def __init__(self, service):
        super(HtmlSplitter, self).__init__(service)

    def split(self, url, title, html, chunkSize=1000, chunkOverlap=100):
        if not self._init():
            return False
            
        if self._contentTable.exists(where="source=?", values=(url,)):
            wrn("HTML content '{}' ALREADY exists in the store [IGNORED]: {}".format(title, url))
            return False

        dbg("Splitting HTML buffer: {} - {} B".format(title, len(html.encode())))

        headersToSplitOn = [
            ("h1", "Header 1"),
            ("h2", "Header 2"),
            ("h3", "Header 3"),
            ("h4", "Header 4")
        ]

        htmlSplitter = HTMLSectionSplitter(headersToSplitOn)
        documents = htmlSplitter.split_text(html)

        if len(documents) == 0:
            err("It has NOT relevant data: {}".format(title))
            return False

        metadatas, chunks = self._splitDocuments(documents)

        if len(chunks) == 0:
            err("It has NOT relevant data: {}".format(title))
            return False

        content = "\n\n".join(chunks)

        soup = BeautifulSoup(html, 'html.parser')
        html = soup.prettify()

        if not self._store(url, html):
            return False

        self._buildChunks(metadatas, chunks)
        return True
        
###############################################################################

class EmbedderService():
    def __init__(self, llm):
        self._llm = llm
        self._l = self._llm._l

        self._embeddingModel = None

        self._dbstore = Database()
        self._vstore = ChromaDB()

        self._blocks = Queue()

        self._collectionName = None
        self._collection = None

        self._initialized = False
        self._running = False

        self._chrono = ElapsedTime()

        self._nlp = LangProcessor()

        self._contentSchema = {
            "source" : "TEXT UNIQUE NOT NULL",
            "data" : "CLOB NOT NULL"
        }

        '''
        self._entitySchema = {
            "text" : "TEXT UNIQUE NOT NULL",
            "type" : "TEXT NOT NULL"
        }
        
        self._contentEntitySchema = {
            "idContent" : "INTEGER NOT NULL CONSTRAINT Content_id REFERENCES Content (id) ON DELETE CASCADE",
            "idEntity" : "INTEGER NOT NULL CONSTRAINT Entity_id REFERENCES Entity (id) ON DELETE CASCADE"
        }
        '''

        self._chunkSchema = {
            "idContent" : "INTEGER NOT NULL CONSTRAINT Content_id REFERENCES Content (id) ON DELETE CASCADE",
            "seq" : "INTEGER NOT NULL",
            "data" : "CLOB NOT NULL"
        }

        self.started            = Signal(self._l)
        self.stopped            = Signal(self._l)
        self.newContent         = Signal(self._l)
        self.itemEmbedded       = Signal(self._l)

    def init(self, sqlDbPath, vDbPath, collectionName, embeddingModel, nlpModelName="en_core_web_trf"):
        if self._initialized:
            err("EmbedderService is ALREADY running")
            return False

        self._embeddingModel = embeddingModel
        
        if not self._nlp.load(nlpModelName):
            err("CANNOT init Spacy")
            return False
            
        if not self._dbstore.open(sqlDbPath) or not self._vstore.open(embeddingModel=None, path=vDbPath):
            return False

        if not self._dbstore.hasTable("Content"):
            if not self._dbstore.createTable("Content", self._contentSchema):
                return False

        '''
        if not self._dbstore.hasTable("Entity"):
            if not self._dbstore.createTable("Entity", self._entitySchema):
                return False

        if not self._dbstore.hasTable("ContentEntity"):
            if not self._dbstore.createTable("ContentEntity", self._contentEntitySchema):
                return False
        '''

        if not self._dbstore.hasTable("Chunk"):
            if not self._dbstore.createTable("Chunk", self._chunkSchema):
                return False
        
        self._collection = self._vstore.openCollection(name=collectionName, algo="cosine")

        if self._collection is None:
            return False

        self._collectionName = collectionName
        self._initialized = True
        return True

    def enqueue(self, splitter):        
        for item in splitter.blocks():
            self._blocks.put(item)
            
        self.newContent.trigger([splitter.rid()])

        if not self.isRunning() and not self.isEmpty():
            self._running = True
            self._l.slowZone.attach(self._tick)
            self.started.trigger()

    def _tick(self):
        if self.isEmpty():
            self._running = False
            self._l.slowZone.detach(self._tick)
            self.stopped.trigger()
            return

        pack = self._blocks.get()

        eid = pack["eid"]
        data = pack["chunk"]

        if len(data) == 0:
            wrn("Chunk is EMPTY: {}".format(eid))
            return
                
        self._chrono.start()
            
        embeddings = self._llm.embed(model=self._embeddingModel, texts=data.lower()) # [[..]]

        if len(embeddings) == 0:
            wrn("Embedding is EMPTY: {}".format(eid))
            return

        dbg("Chunk EMBEDDED [{:.3f} s]".format(self._chrono.stop()))

        ids = [eid]

        self.itemEmbedded.trigger([pack])

        del pack["eid"]
        del pack["meta"]
        del pack["chunk"]

        #metadatas = [pack["meta"]]
        metadatas = [pack]

        self._chrono.start()
        self._collection.upsert(ids=ids, metadatas=metadatas, embeddings=embeddings)
        dbg("Chunk embeddings STORED [{:.3f} s]".format(self._chrono.stop()))

    def close(self):
        if not self._initialized:
            err("Embedder is NOT running yet")
            return False

        if not self.isEmpty():
            self.clear()

        self._dbstore.close()
        self._vstore.close()

        self._initialized = False
        return True

    def isEmpty(self):
        return self._blocks.empty()

    def count(self):
        return self._blocks.qsize()

    def clear(self):
        self._blocks.queue.clear()
    
    def destroy(self):
        self.clear()
        
        self._dbstore.begin()
        contentTable = Table(self._dbstore, "Content")
        contentTable.clear()
        self._dbstore.commit()

        self._dbstore.vacuum()

        self._vstore.deleteCollection(name=self._collectionName)
        self._collection = self._vstore.openCollection(name=self._collectionName)

    def sqlDB(self):
        return self._dbstore

    def vectorDB(self):
        return self._vstore

    def llm(self):
        return self._llm

    def embeddingModel(self):
        return self._embeddingModel

    def collection(self):
        return self._collection

    def isInitialized(self):
        return self._initialized

    def isRunning(self):
        return self._running

    def nlp(self):
        return self._nlp

###############################################################################

class Augmenter():
    def __init__(self, embedderService):
        self._embedderService = embedderService
        self._embeddingModel = self._embedderService.embeddingModel()
        self._llm = self._embedderService.llm()
        self._dbstore = self._embedderService.sqlDB()

    def retrieve(self, texts, where=None, minSimilarity=0.65, maxResults=3):
        result = None

        embeddings = None

        if type(texts) == list:
            for i in range(len(texts)):
                texts[i] = texts[i].lower()

        elif type(texts) == str:
            texts = texts.lower()

        else:
            err("Texts MUST be 'str' or 'list' types, bbut is '{}'".fortmat(type(texts)))

        embeddings = self._llm.embed(model=self._embeddingModel, texts=texts)

        if len(embeddings) == 0:
            err("CANNOT retrieve; input prompt is EMPTY")
            return []
        
        collection = self._embedderService.collection()

        if where is None:
            result = collection.query(query_embeddings=embeddings, n_results=maxResults)
        else:
            result = collection.query(query_embeddings=embeddings, where=where, n_results=maxResults)

        ids = result["ids"][0]
        dists = result["distances"][0]
        metas = result["metadatas"][0]
        
        ctx = [
            #{"chunkID": chunkID1, "meta": meta1, "data": "..", "sim" : x},
            #{"chunkID": chunkID2, "meta": meta2, "data": "..", "sim" : y},

            # ...
        ]

        q = Query(self._dbstore)
       
        for embeddingID, dist, meta in zip(ids, dists, metas):
            similarity = 1-dist

            if similarity >= minSimilarity:
                chunkID = meta["chunkID"]

                if not q.exec(query="SELECT data FROM Chunk WHERE id=?", values=(chunkID,)):
                    return []

                chunkRecord = q.next()

                ctx.append({"chunkID" : chunkID, "eid" : embeddingID,  "meta" : meta, "data" : chunkRecord["data"], "sim" : similarity})
                wrn("[*] Sim: {:+.2} -> {}".format(similarity, embeddingID, meta))

            else:
                dbg("[!] Sim: {:+.2} -> {}".format(similarity, embeddingID, meta))

        if len(ctx) == 0:
            wrn("Result NOT found from RAG store")

        return ctx

###############################################################################
