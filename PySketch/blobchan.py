###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import os

from PySketch.log           import msg, dbg, wrn, err, cri, printPair
from PySketch.signal        import Signal
from PySketch.fs            import *
from PySketch.elapsedtime   import ElapsedTime

from queue                  import Queue

###############################################################################

class BlobService():
    def __init__(self, sat):
        self._sat = sat
        self._ch = None
        
        self._name = None
        self._asOwner = False

        self.opening = Signal(self._sat._loop)
        self.initialized = Signal(self._sat._loop)
        self.responsed = Signal(self._sat._loop)
        self.closed = Signal(self._sat._loop)

        self._sat.serviceResponse.attach(self._onServiceResponse)

###############################################################################

    def isOpen(self):
        return (self._name is not None)

    def isValid(self):
        return (self.isOpen() and self._ch is not None)
    
###############################################################################

    def create(self, blobName, maxDownloads=1, allowedUsers=["guest"]):
        if self.isOpen():
            err("BlobService is ALREADY open: {} ..".format(self._name))
            return
            
        self._asOwner = True
        self._name = blobName
        
        props = {
            "maxDownloads" : maxDownloads,
            "allowedUsers" : allowedUsers
        }

        if not self._sat.addBlobChannel(self._name, props):
            return False

        self._canonicalName = "{}.{}".format(self._sat._userName, self._name)
        
        self._sat.channelAdded.attach(self._onChannelAdded)
        self._sat.channelRemoved.attach(self._onChannelRemoved)

        self.opening.trigger()

        dbg("BlobService CREATING STORE: {} ..".format(self._name))
        return True

    def open(self, blobName):
        if self.isOpen():
            err("BlobService is ALREADY open: {} ..".format(self._name))
            return
            
        self._asOwner = False
        self._name = blobName
        self._canonicalName = self._name 

        self._sat.channelAdded.attach(self._onChannelAdded)
        self._sat.channelRemoved.attach(self._onChannelRemoved)

        if self._sat.isConnected() and self._sat.containsChannel(blobName):
            dbg("The service is ALREADY up: {} ..".format(self._name))
            self._onChannelAdded(self._sat.channelByName(blobName))

        self.opening.trigger()

        dbg("BlobService OPENING STORE: {} ..".format(self._name))
        return True

###############################################################################

    def _onChannelAdded(self, ch):
        if self.isOpen() and not self.isValid() and ch.name == self._canonicalName:
            self._ch = ch
            self._sat.subscribeChannel(self._ch.chanID)
            self.initialized.trigger()
            dbg("BlobService Channel ADDED: {} ..".format(self._name))

    def _onChannelRemoved(self, ch):
        if self._ch is not None and ch.chanID == self._ch.chanID:
            self._ch = None
            dbg("BlobService Channel REMOVED: {} ..".format(self._name))

            if not self._asOwner:
                self.close()

    def _onServiceResponse(self, chanID, response):
        if chanID == self._ch.chanID:
            status = response["status"]
            message = response["msg"]
            data = None

            if "data" in response:
                data = response["data"]

            self.responsed.trigger((status, message, data))

###############################################################################

    def sendCommand(self, cmd, args):
        if not self.isValid():
            err("BlobService is NOT valid: {}".format(self._name))
            return False

        #print("!!!!!!!!!!!!!", args)
        self._sat.sendServiceRequest(self._ch.chanID, cmd, {"args" : args})
        return True

###############################################################################

    def close(self):
        if not self.isOpen():
            err("BlobService is NOT open: {}".format(self._name))
            return False

        if self._sat.isConnected() and self._asOwner:
            self._sat.removeChannel(self._ch.chanID)

        self._sat.channelAdded.detach(self._onChannelAdded)
        self._sat.channelRemoved.detach(self._onChannelRemoved)
        self._sat.serviceResponse.detach(self._onServiceResponse)

        if self._ch is not None and self._sat.isSubscribed(self._ch.chanID):
            self._sat.unsubscribeChannel(self._ch.chanID)

        self.closed.trigger()

        msg("BlobService CLOSED: {}".format(self._name))

        self._ch = None
        self._name = None
        self._asOwner = False

        return True

###############################################################################
###############################################################################

class BlobUploader():
    def __init__(self, bc):
        self._bc = bc
        self._blob = None
        self._bSize = None
        self._readBytes = 0
        self._hashID = None
        self._remoteParentPath = None
        self._filePath = None
        self._filePath = None

        self.allocated = Signal(self._bc._sat._loop)
        self.requested = Signal(self._bc._sat._loop)
        self.started = Signal(self._bc._sat._loop)
        self.progress = Signal(self._bc._sat._loop)
        self.finished = Signal(self._bc._sat._loop)

    def start(self, filePath, remoteParentPath="$TEMP"):
        if self.isRunning():
            err("Upload is ALREADY RUNNING: {}".format(self._filePath))
            return False 

        if not pathExists(filePath):
            err("Path NOT found: {}".format(filePath))
            return False

        if not isFile(filePath):
            err("Path is NOT a file: {}".format(filePath))
            return False
            
        if not isReadable(filePath):
            err("Path is NOT readable: {}".format(filePath))
            return False

        self._remoteParentPath = remoteParentPath
        self._filePath = filePath
        self._fileName = os.path.basename(self._filePath)
        self._bSize = size(filePath)

        self._bc.responsed.attach(self._onBlobStoreResponse)
        self._bc.sendCommand("PUT_ALLOC", [])
        self.requested.trigger()

        '''
        self._fileName = os.path.basename(self._filePath)
        self._bc.responsed.attach(self._onBlobStoreResponse)
        self._bc.sendCommand("PUT", [self._fileName, self._bSize, remoteParentPath])
        msg("Upload REQUESTED [{} B]: {}".format(self._bSize, self._filePath))
        '''
        return True

    def _onBlobStoreResponse(self, status, _, data):
        if self._hashID is None:
            if status:
                self._hashID = data["uploadHashID"]
                self.allocated.trigger()

                self._bc.sendCommand("PUT", [self._hashID, self._fileName, self._bSize, self._remoteParentPath])

                self._blob = open(self._filePath, "rb")
                self._bc._sat._loop.fastZone.attach(self._uploadTick)

                msg("Upload STARTED [{} B]: {}".format(self._bSize, self._filePath))
                self.started.trigger()

            else:
                self._bc.responsed.detach(self._onBlobStoreResponse)
                msg("Upload-allocation ERROR: {}".format(data["msg"]))
                self.stop()

        else:
            self._bc.responsed.detach(self._onBlobStoreResponse)

            if status:
                msg("Upload SUCCESS [{} B]: {}".format(self._readBytes, self._filePath))
                
            else:
                msg("Upload ERROR [{} B]: {}".format(self._readBytes, self._filePath))
                self.stop()
            
            self.finished.trigger()
                
    def stop(self):
        if self._blob is not None:
            self._blob.close()
            self._blob = None

        if self._bc._sat._loop.fastZone.isAttached(self._uploadTick):
            self._bc._sat._loop.fastZone.detach(self._uploadTick)

        msg("Upload TERMINATED [{} B]: {}".format(self._bSize, self._filePath))
        return True

    def _uploadTick(self):
        data = self._blob.read(8192)

        if data is None:
            err("NO DATA: {}".format(self._bSize, self._filePath))
            return

        dataPack = bytearray(self._hashID, 'utf-8')
        dataPack.extend(data)

        self._bc._sat.publish(self._bc._ch.chanID, dataPack)

        self._readBytes += len(data)
        self.progress.trigger((self._readBytes,))

        if self._readBytes == self._bSize:
            msg("Upload COMPLETED [{} B]: {}".format(self._readBytes, self._filePath))
            self.stop()

    def isRunning(self):
        return (self._blob is not None)

###############################################################################
###############################################################################

class BlobDownloader():
    def __init__(self, bc):
        self._bc = bc
        self._blob = None
        self._bSize = None
        self._wroteBytes = 0
        self._hashID = None
        self._blobPath = None
        self._fileName = None
        self._filePath = None

        self.requested = Signal(self._bc._sat._loop)
        self.started = Signal(self._bc._sat._loop)
        self.progress = Signal(self._bc._sat._loop)
        self.finished = Signal(self._bc._sat._loop)

    def start(self, blobPath, localParentPath=""):
        if self.isRunning():
            err("Download is ALREADY RUNNING: {}".format(self._filePath))
            return False

        self._blobPath = blobPath
        self._fileName = os.path.basename(self._blobPath)

        if len(localParentPath) == 0:
            self._filePath = self._fileName
        else:
            localParentPath = os.path.abspath(localParentPath)
            self._filePath = os.path.join(localParentPath, self._fileName)

        '''
        if pathExists(self._filePath):
            err("Path ALREADY exists: {}".format(self._filePath))
            return False
        '''
            
        self._bc._sat.subscribedDataCome.attach(self._downloadTick)
        self._bc.responsed.attach(self._onBlobStoreResponse)

        self._bc.sendCommand("GET", [self._blobPath])

        msg("Download REQUESTED: {}".format(self._filePath))
        self.requested.trigger()
        return True

    def _onBlobStoreResponse(self, status, _, data):
        self._bc.responsed.detach(self._onBlobStoreResponse)

        if not status:
            self.stop()
            return

        self._hashID = data["reqHashID"]
        self._bSize = data["size"]
        self._blob = open(self._filePath, "wb")

        msg("Download STARTED [{} B]: {}".format(self._bSize, self._filePath))
        self.started.trigger()

    def _downloadTick(self, chanID, data):
        if chanID == self._bc._ch.chanID:
            if len(data) <= 32:
                err("CANNOT grab downloading Blob data buffer [size <= 32]")
                return

            if data[:32].decode('utf-8') == self._hashID:
                self._wroteBytes += self._blob.write(data[32:])
                self.progress.trigger((self._wroteBytes,))

                if self._bSize == self._wroteBytes:
                    self._bc._sat.subscribedDataCome.detach(self._downloadTick)
                    self.stop()

    def stop(self):
        if not self.isRequested():
            err("Download is requested but NOT started yet: {}".format(self._filePath))
            return False

        if self._blob is not None:
            self._blob.close()
            self._blob = None

        msg("Download TERMINATED [{} B]: {}".format(self._bSize, self._filePath))
        self.finished.trigger()
        return True
        
    def isRunning(self):
        return (self._blob is not None)

    def isRequested(self):
        return (self._filePath is not None)

###############################################################################
###############################################################################
# BATCHs Uload/Download (WANT a BlobService Open)

class BlobUploadBatch():
    def __init__(self, bc):
        self._uploadQueue = None
        self._currentSourcePath = None
        self._bc = bc
        self._progressionID = 0
        self._currentUploader = None
        self._remoteTargetPath = None
        self._sources = None

        self.started = Signal(self._bc._sat._loop)
        self.progress = Signal(self._bc._sat._loop)
        self.finished = Signal(self._bc._sat._loop)

    def upload(self, sources, remoteTargetPath="$TEMP"):
        if self.isRunning():
            err("Blob upload-bacth is ALREADY running")
            return False
            
        self._sources = sources

        if len(self._sources) == 0:
            err("Sources list is EMPTY")
            return False

        self._remoteTargetPath = remoteTargetPath
        self._uploadQueue = Queue()

        for source in self._sources:
            self._uploadQueue.put(source)
            msg("Added source to batch: {}".format(source))

        self.started.trigger()
        msg("Upload-batch STARTED {}".format(self._sources))
        self._next()
        return True

    def _next(self):
        if self._uploadQueue.empty():
            self.terminate()

        else:
            self._currentSourcePath = self._uploadQueue.get()

            if self._currentUploader is not None:
                self._currentUploader.finished.detach(self._next)

            self._currentUploader = BlobUploader(self._bc)
            self._currentUploader.finished.attach(self._next)

            if not self._currentUploader.start(self._currentSourcePath, self._remoteTargetPath):
                self.terminate()
                return
                
            self.progress.trigger((self._progressionID, self._currentSourcePath))
            self._progressionID += 1

    def terminate(self):
        if self._currentUploader is not None:
            self._currentUploader.finished.detach(self._next)

        msg("Upload-batch TERMINATED {}".format(self._sources))

        self._uploadQueue = None
        self._currentSourcePath = None
        self._currentUploader = None
        self._progressionID = 0
        self._sources = None

        self.finished.trigger()

    def isRunning(self):
        return (self._uploadQueue is not None)

###############################################################################

class BlobDownloadBatch():
    def __init__(self, bc):
        self._downloadQueue = None
        self._currentSourcePath = None
        self._bc = bc
        self._progressionID = 0
        self._currentDownloader = None
        self._localParentPath = None
        self._sources = None

        self.started = Signal(self._bc._sat._loop)
        self.progress = Signal(self._bc._sat._loop)
        self.finished = Signal(self._bc._sat._loop)

    def download(self, sources, localParentPath=""):
        if self.isRunning():
            err("Blob download-bacth is ALREADY running")
            return False

        self._sources = sources

        if len(self._sources) == 0:
            err("Sources list is EMPTY")
            return False

        self._localParentPath = localParentPath
        self._downloadQueue = Queue()

        for source in self._sources:
            self._downloadQueue.put(source)
            
            msg("Added source to batch: {}".format(source))

        self.started.trigger()
        msg("Download-batch STARTED {}".format(self._sources))
        self._next()
        return True

    def _next(self):
        if self._downloadQueue.empty():
            self.terminate()

        else:
            self._currentSourcePath = self._downloadQueue.get()

            if self._currentDownloader is not None:
                self._currentDownloader.finished.detach(self._next)

            self._currentDownloader = BlobDownloader(self._bc)
            self._currentDownloader.finished.attach(self._next)

            if not self._currentDownloader.start(self._currentSourcePath, self._localParentPath):
                self.terminate()
                return
                
            self.progress.trigger((self._progressionID, self._currentSourcePath))
            self._progressionID += 1

    def terminate(self):
        if self._currentDownloader is not None:
            self._currentDownloader.finished.detach(self._next)

        msg("Download-batch TERMINATED {}".format(self._sources))

        self._downloadQueue = None
        self._currentSourcePath = None
        self._currentDownloader = None
        self._progressionID = 0
        self._sources = None

        self.finished.trigger()

    def isRunning(self):
        return (self._downloadQueue is not None)

###############################################################################
        
