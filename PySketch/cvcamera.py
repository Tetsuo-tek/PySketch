###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import time
import cv2                  as cv

from PySketch.signal        import functSignature
from PySketch.elapsedtime   import ElapsedTime
from PySketch.log           import msg, dbg, wrn, err, cri, printPair

class CvCamera():
    def __init__(self):
        self._cap = None
        self._camID = -1
        self._w = -1
        self._h = -1
        self._fps = -1
        self._t = -1
        self._f = None
        self._chrono = ElapsedTime()
        self._fpsAvg = -1
        self._fpsCount = -1
        self._onCapTickCb = None
        self._capTimeChrono = ElapsedTime()
        self._capTime = 0.0
        self._lastTickTime = 0
        self._fourCC = "MJPG"
        self._master = None
        self._slave = None

    def init(self, camID, res, fps, fourCC, onCapTickCb):
        if self._cap is not None:
            err("CvCamera is ALREADY open")
            return False

        self._camID = camID
        self._fps = fps
        self._t = 1.0 / self._fps
        self._fpsAvg = 0.0
        self._fpsCount = 0
        self._fourCC = fourCC

        self._cap = cv.VideoCapture(self._camID, cv.CAP_V4L2)

        if not self._cap.isOpened():
            err("CANNOT open the camera [ID: {}]".format(self._camID))
            return False

        self._cap.set(cv.CAP_PROP_FOURCC, cv.VideoWriter_fourcc(*self._fourCC))
        #self._cap.set(cv.CAP_PROP_FRAME_WIDTH, float(res[0]))
        #self._cap.set(cv.CAP_PROP_FRAME_HEIGHT, float(res[1]))
        self._cap.set(cv.CAP_PROP_FPS, float(self._fps))
        
        self._w = int(self._cap.get(cv.CAP_PROP_FRAME_WIDTH))
        self._h = int(self._cap.get(cv.CAP_PROP_FRAME_HEIGHT))
        
        tempFPS = int(self._cap.get(cv.CAP_PROP_FPS))

        if tempFPS != self._fps:
            wrn("CANNOT set fps: requested {} -> available {}".format(self._fps, tempFPS))

        self._fps = tempFPS

        if self._w == -1 or self._h == -1:
            err("CANNOT set the camera resolution [res: {}]".format(res))
            return False

        if onCapTickCb is None:
            wrn("OnCaptureTick Callback DISABLED")
        
        else:
            self._onCapTickCb = onCapTickCb
            dbg("OnCaptureTick Callback ENABLED: {}{}".format(self._onCapTickCb.__name__, functSignature(self._onCapTickCb)))

        msg("CvCamera ENABLED [FourCC: {}; Res: {}x{}; FPS: {}; t: {} s]".format(self._fourCC, self._w, self._h, self._fps, self._t))
        return True

    def setAsSynchMaster(self, master):
        self._master = master
        msg("Camera is MASTER")

    def setAsSynchSlave(self, slave):
        self._slave = slave
        msg("Camera is SLAVE")

    # IT WILL NOT SLOW DOWN TO self._t THE TICK (NO WAIT OTHER THAN CAPTURING JOB TIME)
    def tick(self):
        if self._cap is None:
            err("CvCamera is NOT open yet")
            return False

        self._checkSynch()

        self._lastTickTime = self._chrono.stop()
        self._capTimeChrono.start()

        ret, self._f = self._cap.read()

        self._chrono.start()
        self._capTime = self._capTimeChrono.stop()

        self._checkSynch()

        if not ret:
            err("CANNOT capture frame")
            return False

        if self._onCapTickCb is not None:
            self._onCapTickCb(self._f)

        return True

    def _checkSynch(self):
        if self._master is not None:
            self._master.release()
            
        elif self._slave is not None:
            self._slave.wait()

    def quit(self):
        if self._cap is None:
            err("CvCamera is NOT open yet")
            return False

        self._cap.release()
        self._cap = None

        return True
    
###############################################################################
