import signal
import queue
import readline

from PySketch.log           import escapes, LogMachine, msg, dbg, wrn, err, cri, raw, printPair

class Console():
    def __init__(self):
        self._commands = []
        self._commandsQueue = queue.Queue()

    def addCommand(self, name):
        if name in self._commands:
            err("Command ALREADY inserted: {}".format(name))
            return False

        self._commands.append(name)
        return True

    def signalHandler(self, signal, frame):
        cri("Signal received, exiting ..")
        raise KeyboardInterrupt

    def complete(self, text, state):
        options = [name for name in self._commands if name.startswith(text)]

        if state < len(options):
            return options[state]

        return None

    def displayMatches(self, substitution, matches, longest_match_length):
        print()

        for match in matches:
            print(f"  {match}")

        readline.redisplay()

    def get(self):
        row = None

        try:
            row = self._commandsQueue.get_nowait()

        except queue.Empty:
            return None

        return row

    def taskDone(self):
        self._commandsQueue.task_done()

    def exec(self):
        signal.signal(signal.SIGINT, self.signalHandler)

        readline.set_completer(self.complete)
        readline.set_completer_delims(" \t\n;")
        readline.parse_and_bind("tab: complete")
        readline.set_completion_display_matches_hook(self.displayMatches)

        while True:
            try:
                row = input().strip()

                if row is None:
                    break

                if row.lower() in ["exit", "quit"] or len(row) == 0:
                    wrn("Closing ..")
                    break

                self._commandsQueue.put(row)
                self._commandsQueue.join()

            except EOFError:
                break
                
            except KeyboardInterrupt:
                break