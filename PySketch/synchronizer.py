###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

from PySketch.server        import TcpServer
from PySketch.socketdevice  import SocketDevice
from PySketch.log           import escapes, msg, dbg, wrn, err, cri, raw, printPair

class Master():
    def __init__(self, loop):
        self._loop = loop
        self._syncSvr = TcpServer()
        self._slaves = []

    def setup(self, listenAddr="127.0.0.1", listenPort=6666):
        self._syncSvr.setup(listenAddr, listenPort, self._onSyncAcceptSlave)

    def open(self):
        if self._syncSvr.isListening():
            err("Master ALREADY open")
            return False

        if not self._syncSvr.open():
            return False

        self._loop.slowZone.attach(self._syncSvr.tick)
        return True

    def release(self):
        for slave in self._slaves[:]:
            if slave.isConnected():
                slave.writeUInt8(0)
            else:
                self._slaves.remove(slave)
        '''
        if self._slave is not None:
            if self._slave.isConnected():
                self._slave.writeUInt8(0)
            else:
                self._slave = None
        '''

        return True

    def close(self):
        if not self._syncSvr.isListening():
            err("Master is NOT open")
            return False

        for slave in self._slaves:
            slave.disconnect()

        self._slaves = []
        '''
        if self._slave is not None:
            self._slave.disconnect()
            self._slave = None
        '''

        self._loop.slowZone.detach(self._syncSvr.tick)
        self._syncSvr.close()
        return True

    def _onSyncAcceptSlave(self, clnt, addr):
        slave = SocketDevice()
        slave.setSocket(clnt)
        self._slaves.append(slave)
        msg("Slave ACCEPTED: {}".format(addr))

    def isListening(self):
        return self._syncSvr.isListening()

class Slave():
    def __init__(self, loop):
        self._loop = loop
        self._masterAddr = None
        self._masterPort = -1
        self._syncClt = SocketDevice()

    def setup(self, masterAddr="127.0.0.1", masterPort=6666):
        self._masterAddr = masterAddr
        self._masterPort = masterPort

    def connect(self):
        if self._syncClt.isConnected():
            err("Slave ALREADY open")
            return False

        return self._syncClt.tcpConnect(self._masterAddr, self._masterPort)

    def wait(self):
        if self._syncClt.readUInt8() != 0:
            err("Received NOT valid data")

    def disconnect(self):
        if not self._syncClt.isConnected():
            err("Slave is NOT connected")
            return False

        self._syncClt.disconnect()
        return True

    def isConnected(self):
        return self._syncClt.isConnected()
