###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import json

import zmq
import zmq.utils.monitor    as zmq_monitor

from PySketch.signal        import functSignature
from PySketch.log           import msg, dbg, wrn, err, cri, printPair

class Publisher():
    def __init__(self):
        self._addr = None
        self._port = None
        self._mp = None

        self._onPubStartCb = None
        self._onPubStopCb = None

        self._zctx = None
        self._zpub = None
        self._zmon = None
        self._zpol = None

        self._count = 0

    def setup(self, listenAddress, listenPort, onPublishReqStartCb, onPublishReqStopCb):
        self._addr = listenAddress
        self._port = listenPort

        self._onPubStartCb = onPublishReqStartCb
        dbg("OnPubStart Callback ENABLED: {}{}".format(self._onPubStartCb.__name__, functSignature(self._onPubStartCb)))

        self._onPubStopCb = onPublishReqStopCb
        dbg("OnPubStop Callback ENABLED: {}{}".format(self._onPubStopCb.__name__, functSignature(self._onPubStopCb)))

        self._mp = "tcp://0.0.0.0:{}".format(self._port)

    def init(self):
        if self.isRunning():
            err("Publisher is ALREADY running-> {}".format(self._mp))
            return False

        try:
            self._zctx = zmq.Context()
            self._zpol = zmq.Poller()
            
            self._zpub = self._zctx.socket(zmq.PUB)
            self._zpub.setsockopt(zmq.SNDHWM, 0)

            self._zpub.bind(self._mp)

            self._zmon = self._zpub.get_monitor_socket()
            self._zpol.register(self._zmon, zmq.POLLIN)

        except zmq.error.ZMQError as e:
            self._zctx = None
            self._zpub = None
            self._zmon = None
            self._zpol = None

            err("CANNOT enable ZMQ/PUB -> {} - {}".format(self._mp, e))
            return False

        self._count = 0
        msg("Publisher is ENABLED -> {}".format(self._mp))
        return True

    def publish(self, data, pfx=bytearray(), closeFrame=True):
        if not self.isRunning():
            err("Publisher is NOT running")
            return False

        try:
            if closeFrame:
                self._zpub.send(pfx+data, 0)
            else:
                self._zpub.send(pfx+data, zmq.SNDMORE)
                
        except zmq.error.ZMQError as e:
            err("CANNOT publish data -> {}".format(e))
            return False

        return True

    def tick(self):
        if not self.isRunning():
            return
        
        socks = dict(self._zpol.poll(0))

        if self._zmon not in socks:
            return 

        eventPack = zmq_monitor.recv_monitor_message(self._zmon, flags=zmq.DONTWAIT)
        dbg("Publisher ZMQ/EVENT grabbed: {}".format(eventPack))

        evt = eventPack["event"]

        if evt == zmq.EVENT_ACCEPTED:
            startPublishRequired = (self._count == 0)
            self._count += 1
            msg("Subscriber ACCEPTED -> device has {} subscribers".format(self._count))

            if startPublishRequired:
                self._onPubStartCb()

        elif evt == zmq.EVENT_DISCONNECTED:
            self._count -= 1
            msg("Subscriber DISCONNECTED -> device has {} subscribers".format(self._count))

            if self._count == 0:
                self._onPubStopCb()

    def quit(self):
        if not self.isRunning():
            err("Publisher is NOT running")
            return False

        self._zpol.unregister(self._zmon)

        self._zpub.close()
        self._zmon.close()
        self._zctx.term()

        self._zctx = None
        self._zpub = None
        self._zmon = None
        self._zpol = None

        msg("Publisher is DISABLED -> {}".format(self._mp))
        return True

    def isRunning(self):
        return self._zctx is not None

    def subscribers(self):
        return self._count

###############################################################################

class Subscriber():
    def __init__(self):
        self._host = None
        self._port = None
        self._mp = None

        self._onDataComeCb = None

        self._zctx = None
        self._zsub = None
        self._zmon = None
        self._zpol = None

        self._pfx = None

    def setup(self, host, port, onDataComeCb):
        self._host = host
        self._port = port

        self._onDataComeCb = onDataComeCb
        dbg("OnDataCome Callback ENABLED: {}{}".format(self._onDataComeCb.__name__, functSignature(self._onDataComeCb)))
        
        self._mp = "tcp://{}:{}".format(self._host, self._port)

    def init(self, pfx=bytearray()):
        if self.isRunning():
            err("Subscriber is ALREADY running-> {}".format(self._mp))
            return False

        try:
            self._zctx = zmq.Context()
            self._zpol = zmq.Poller()
            self._zsub = self._zctx.socket(zmq.SUB)
            self._zsub.setsockopt(zmq.RCVHWM, 0)
            self._zpol.register(self._zsub, zmq.POLLIN)

            self._zmon = self._zsub.get_monitor_socket()
            self._zpol.register(self._zmon, zmq.POLLIN)

        except zmq.error.ZMQError as e:
            self._zctx = None
            self._zsub = None
            self._zmon = None
            self._zpol = None

            err("CANNOT enable ZMQ/SUB -> {} - {}".format(self._mp, e))
            return False

        self._pfx = pfx
        msg("Subscriber INITIALIZED [pfx: {} B] -> {}".format(len(self._pfx), self._mp))
        
        return True
        
    def subscribe(self):
        if not self.isRunning():
            err("Subscriber is NOT running")
            return False

        try:
            self._zsub.connect(self._mp)
            self._zsub.setsockopt(zmq.SUBSCRIBE, bytes(self._pfx))

        except zmq.error.ZMQError as e:
            err("CANNOT subscribe -> {} - {}".format(self._mp, e))
            return False

        msg("Subscriber is ENABLED -> {}".format(self._mp))
        return True

    def tick(self):
        if not self.isRunning():
            return

        socks = dict(self._zpol.poll(0))
        
        if self._zmon in socks:
            eventPack = zmq_monitor.recv_monitor_message(self._zmon, flags=zmq.DONTWAIT)
            dbg("Subscriber ZMQ/EVENT grabbed: {}".format(eventPack))

            evt = eventPack["event"]

            if evt == zmq.EVENT_CONNECTED:
                msg("Subscriber CONNECTED")

            elif evt == zmq.EVENT_DISCONNECTED:
                msg("Subscriber DISCONNECTED")

        if self._zsub not in socks:
            return

        while self._zsub.getsockopt(zmq.EVENTS) & zmq.POLLIN:
            #print("!!!")
            data = None

            try:
                data = self._zsub.recv()[len(self._pfx):]#flags=zmq.DONTWAIT
                #data = self._zsub.recv_multipart()

            except zmq.Again as e:
                #print("###: {}".format(e))
                return

            self._onDataComeCb(data)
            
    def quit(self):
        if not self.isRunning():
            err("Subscriber is NOT running")
            return False
            
        self._zpol.unregister(self._zsub)
        self._zpol.unregister(self._zmon)

        self._zsub.close()
        self._zmon.close()
        self._zctx.term()

        self._zctx = None
        self._zsub = None
        self._zmon = None
        self._zpol = None

        msg("Subscriber is DISABLED -> {}".format(self._mp))
        return True

    def isRunning(self):
        return self._zctx is not None

    def fileno(self):
        if not self.isRunning():
            return -1

        return self._zsub.fileno()

###############################################################################

class Replier():
    def __init__(self):
        self._addr = None
        self._port = None
        self._mp = None

        self._onRequestCb = None

        self._zctx = None
        self._zrep = None
        self._zmon = None
        self._zpol = None

        self._count = 0

    def setup(self, listenAddress, listenPort, onRequestCb):
        self._addr = listenAddress
        self._port = listenPort

        self._onRequestCb = onRequestCb
        dbg("OnRequestCb Callback ENABLED: {}{}".format(self._onRequestCb.__name__, functSignature(self._onRequestCb)))

        self._mp = "tcp://0.0.0.0:{}".format(self._port)

    def init(self):
        if self.isRunning():
            err("Replier is ALREADY running-> {}".format(self._mp))
            return False

        try:
            self._zctx = zmq.Context()
            self._zpol = zmq.Poller()
            
            self._zrep = self._zctx.socket(zmq.REP)
            self._zrep.bind(self._mp)
            self._zpol.register(self._zrep, zmq.POLLIN) 

            self._zmon = self._zrep.get_monitor_socket()
            self._zpol.register(self._zmon, zmq.POLLIN)

        except zmq.error.ZMQError as e:
            self._zctx = None
            self._zrep = None
            self._zmon = None
            self._zpol = None

            err("CANNOT enable ZMQ/REP -> {} - {}".format(self._mp, e))
            return False

        self._count = 0
        msg("Replier is ENABLED -> {}".format(self._mp))
        return True

    def reply(self, data):
        if not self.isRunning():
            err("Replier is NOT running")
            return False

        try:
            self._zrep.send(data)
                
        except zmq.error.ZMQError as e:
            err("CANNOT reply data -> {}".format(e))
            return False

        return True

    def tick(self):
        if not self.isRunning():
            return
        
        socks = dict(self._zpol.poll(0))

        if self._zmon in socks:
            eventPack = zmq_monitor.recv_monitor_message(self._zmon, flags=zmq.DONTWAIT)
            dbg("Replier ZMQ/EVENT grabbed: {}".format(eventPack))

            evt = eventPack["event"]

            if evt == zmq.EVENT_ACCEPTED:
                startPublishRequired = (self._count == 0)
                self._count += 1
                msg("Requester ACCEPTED -> device has {} requesters".format(self._count))

            elif evt == zmq.EVENT_DISCONNECTED:
                self._count -= 1
                msg("Requester DISCONNECTED -> device has {} requesters".format(self._count))

        elif self._zrep in socks:
            data = self._zrep.recv()
            msg("Request RECEIVED: {}".format(data))
            self._onRequestCb(data)

    def quit(self):
        if not self.isRunning():
            err("Replier is NOT running")
            return False

        self._zpol.unregister(self._zrep)
        self._zpol.unregister(self._zmon)

        self._zrep.close()
        self._zmon.close()
        self._zctx.term()

        self._zctx = None
        self._zrep = None
        self._zmon = None
        self._zpol = None

        msg("Replier is DISABLED -> {}".format(self._mp))
        return True

    def isRunning(self):
        return self._zctx is not None

    def requesters(self):
        return self._count

    def fileno(self):
        if not self.isRunning():
            return -1

        return self._zrep.fileno()

###############################################################################

class Requester():
    def __init__(self):
        self._host = None
        self._port = None
        self._mp = None

        self._zctx = None
        self._zreq = None
        self._zmon = None
        self._zpol = None

    def setup(self, host, port):
        self._host = host
        self._port = port
        self._mp = "tcp://{}:{}".format(self._host, self._port)

    def init(self, pfx=bytearray()):
        if self.isRunning():
            err("Requester is ALREADY running-> {}".format(self._mp))
            return False

        try:
            self._zctx = zmq.Context()
            self._zpol = zmq.Poller()
            self._zreq = self._zctx.socket(zmq.REQ)

        except zmq.error.ZMQError as e:
            self._zctx = None
            self._zreq = None
            self._zpol = None

            err("CANNOT enable ZMQ/REQ -> {} - {}".format(self._mp, e))
            return False

        msg("Requester INITIALIZED -> {}".format(self._mp))
        return True
        
    def connect(self):
        if not self.isRunning():
            err("Requester is NOT running")
            return False

        try:
            self._zreq.connect(self._mp)
            self._zmon = self._zreq.get_monitor_socket()
            self._zpol.register(self._zmon, zmq.POLLIN)
            
        except zmq.error.ZMQError as e:
            self._zctx = None
            self._zreq = None
            self._zpol = None
            self._zmon = None
            err("CANNOT connect -> {} - {}".format(self._mp, e))
            return False

        msg("Requester ENABLED -> {}".format(self._mp))
        return True

    def request(self, data):
        if not self.isRunning():
            err("Requester is NOT running")
            return False

        try:
            self._zreq.send(data)
            data = self._zreq.recv()
            
        except zmq.error.ZMQError as e:
            err("CANNOT request -> {} - {}".format(self._mp, e))
            return bytearray()

        return data

    def requestAsJson(self, pack):
        if not self.isRunning():
            err("Requester is NOT running")
            return False

        req = json.dumps(pack)
        data = self.request(req.encode("utf-8"))
        props = json.loads(data.decode('utf-8'))

        if not props["status"]:
            err("Error RECEIVED from service -> {}".format(props["err"]))
            return None

        return props

    def tick(self):
        if not self.isRunning():
            return

        socks = dict(self._zpol.poll(0))

        if self._zmon in socks:
            eventPack = zmq_monitor.recv_monitor_message(self._zmon, flags=zmq.DONTWAIT)
            dbg("Requester ZMQ/EVENT grabbed: {}".format(eventPack))

            evt = eventPack["event"]

            if evt == zmq.EVENT_CONNECTED:
                msg("Requester CONNECTED")

            elif evt == zmq.EVENT_DISCONNECTED:
                msg("Requester DISCONNECTED")

    def quit(self):
        if not self.isRunning():
            err("Requester is NOT running")
            return False
            
        self._zpol.unregister(self._zmon)

        self._zmon.close()
        self._zreq.close()
        self._zctx.term()

        self._zctx = None
        self._zreq = None
        self._zmon = None
        self._zpol = None

        msg("Requester is DISABLED -> {}".format(self._mp))
        return True

    def isRunning(self):
        return self._zctx is not None

    def fileno(self):
        if not self.isRunning():
            return -1

        return self._zreq.fileno()

###############################################################################
