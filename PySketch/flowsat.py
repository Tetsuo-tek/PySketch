###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

# IMPORTANT: THIS CLASS IS THINKED TO WORK FROM A Sk/PySketch connecting to SkRobot

from PySketch.host          import HostMachine
from PySketch.loop          import EventLoop, LoopTimerMode
from PySketch.flowasync     import FlowAsync
from PySketch.flowsync      import FlowSync
from PySketch.elapsedtime   import ElapsedTime
from PySketch.log           import logger, LogMachine, escapes, msg, dbg, wrn, err, cri, printPair

import sys
import os
import re

from cpuinfo import get_cpu_info

###############################################################################

class FlowSat(FlowAsync):
    def __init__(self):
        super(FlowSat, self).__init__(EventLoop("FlowSat"))

        global logger
        
        if logger is None:
            logger = LogMachine()
            logger.setNodeName(self._nodeName)

        self._loop.setLoopName("{}@FlowSat.LOOP".format(self._nodeName))
        self._loop.slowZone.attach(self.onSlowTick)
        self._loop.oneSecZone.attach(self.onOneSecTick)

        '''
        if loop is None:
            loopName = "{}@FlowSat.LOOP".format(self._nodeName)
            self._loop.setLoopName(loopName)
            wrn("Creating a standalone LOOP: {}".format(loopName))
            self._initSignals(EventLoop(loopName))
        '''
        
        self._userName = "User1"
        self._passwd = "password"

        #self._timer = TimeStepper()
        #self._timer.setInterval(0.020, 0.2)

        self._speedMonitorEnabled = True

        self._tickChrono = ElapsedTime()
        self._checkChrono = ElapsedTime()

        print("###############################################################################")
        self._host = HostMachine()
        self._host.printInfos()
        print("###############################################################################")

###############################################################################

    def newSyncClient(self):
        sync = FlowSync(self._loop)
        
        ok = False

        if self._port == 0:
            ok = sync.localConnect(self._address)
        
        else:
            ok = sync.tcpConnect(self._address, self._port)
        
        if ok:
            ok = sync.login(self._userName, self._passwd)

        if not ok:
            return FlowSync()
        
        else:
            dbg("Sync-connection LOGGED-IN")
            sync.setCurrentDbName(self._userName)

        return sync

###############################################################################

    def setTickTimer(self, fastInterval, checkInterval, slowInterval=0.150, mode=LoopTimerMode.TIMEDLOOP_RT):
        self._loop.setFastInterval(fastInterval, checkInterval)
        self._loop.setSlowInterval(slowInterval)
        self._loop.setTickMode(mode)

    def setLogin(self, userName, password):
        self._userName = userName
        self._passwd = password

###############################################################################

    def setSpeedMonitorEnabled(self, enable):
        self._speedMonitorEnabled = enable

###############################################################################

    def _checkLoopInit(self):
        if not self._loop.isRunning():
            wrn("Starting event-loop ..")
            self._loop.init(False)

###############################################################################

    def connect(self):
        ok = False

        robotAddressVarName = "ROBOT_ADDRESS"

        if robotAddressVarName in os.environ:
            robotAddress = os.environ.get(robotAddressVarName)
            dbg("ROBOT_ADDRESS environment-variable found -> {}".format(robotAddress))

            localPattern = r'^local:([^:]+)$'
            localMatch = re.match(localPattern, robotAddress)

            if localMatch:
                self._address = localMatch.group(1)
                self._port = 0
                dbg("Connecting to ROBOT_ADDRESS [LOCAL]-> {} ..".format(self._address))
                ok = self.localConnect(self._address)
            
            else:
                tcpPattern = r'^tcp:([^:]+)(?::(\d+))?$'
                tcpMatch = re.match(tcpPattern, robotAddress)
            
                if tcpMatch:
                    self._address = tcpMatch.group(1)

                    port = 9000

                    if tcpMatch.group(2) is not None:
                        port = int(tcpMatch.group(2))

                    if port > 0:
                        self._port = port
                    else:
                        self._port = 9000

                    dbg("Connecting to ROBOT_ADDRESS [TCP] -> {}:{} ..".format(self._address, self._port))
                    ok = self.tcpConnect(self._address, self._port)
                
        else:
            self._address = "127.0.0.1"
            self._port = 9000

            wrn("ROBOT_ADDRESS environment-variable NOT found; connecting to default address [TCP] -> 127.0.0.1:9000 ..")
            ok = self.tcpConnect(self._address, self._port)
        
        if ok:
            dbg("Trying to login: {}".format(self._userName))
            ok = self.login(self._userName, self._passwd)
        
        if ok:
            dbg("Logged-in as {}".format(self._userName))
            self.setCurrentDbName(self._userName)

        self._checkLoopInit()
        return ok
    
    def disconnect(self):
        if self.isConnected():
            self.close()

    def quit(self, val=0):
        self._loop.quit(val)

###############################################################################

    def tick(self, looping=True):
        if self.isConnected():
            self.pulse()

        if looping:
            self._loop.tick()

###############################################################################

    def onSlowTick(self):
        if self._speedMonitorEnabled and self._loop._timer._cycleStatus:
            freq = 1/self._loop._timer._currAvg

            if logger is not None and not logger._consLogEnabled:
                return
            
            if logger is None or not logger._consColoursEnabled:
                sys.stdout.write("{} >> [{:>08.1f} Hz; {:.3f}/{:.3f}/{:.3f} s; {:+.6f} s; {:+.3f} %]\r"
                    .format(
                        self._nodeName,
                        freq,
                        self._loop._timer._tickTime,
                        self._loop._timer._t,
                        self._loop._timer._currAvg,
                        self._loop._timer._currOffAvg,
                        self._loop._timer._currErr
                    )
                )

            else:
                sys.stdout.write("{}{} {}>> {}[{:>08.1f} Hz; {:.3f}/{:.3f}/{:.3f} s; {:+.6f} s; {:+.3f} %]\r{}"
                    .format(
                        escapes["RED"],
                        self._nodeName, 
                        escapes["YLW"],
                        escapes["WHT"],
                        freq,
                        self._loop._timer._tickTime,
                        self._loop._timer._t,
                        self._loop._timer._currAvg,
                        self._loop._timer._currOffAvg,
                        self._loop._timer._currErr, 
                        escapes["RESET"]
                    )
                )

            sys.stdout.flush()

###############################################################################

    def onOneSecTick(self):
        if self.isConnected():            
            if self._checkChrono.stop() > 4:
                self.checkService()
                self._checkChrono.start()

###############################################################################
