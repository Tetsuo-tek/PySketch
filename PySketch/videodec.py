###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import struct

import cv2      as cv
import numpy    as np

class PsyvDecoder:
    def __init__(self, w, h):
        self._w = w
        self._h = h
        self._f = np.zeros((h, w, 3), np.uint8)

    def decode(self, data):
        index = 0

        while index < len(data):
            blk_X = struct.unpack("<H", data[index:index+2])[0]
            blk_Y = struct.unpack("<H", data[index+2:index+4])[0]
            jpgLen = struct.unpack("<I", data[index+4:index+8])[0]
            jpgData = data[index+8:index+8+jpgLen]

            index += (8 + jpgLen)

            jpg = np.frombuffer(jpgData, np.uint8)
            blk = cv.imdecode(jpg, cv.IMREAD_COLOR)
            blk_H, blk_W = blk.shape[:2]

            self._f[blk_Y : blk_Y + blk_H, blk_X : blk_X + blk_W] = blk

    def getFrame(self):
        return self._f
