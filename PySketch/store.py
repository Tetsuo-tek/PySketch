###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import chromadb

from chromadb.utils.embedding_functions import SentenceTransformerEmbeddingFunction

from PySketch.log                       import msg, dbg, wrn, err, cri, printPair

###############################################################################

class ChromaDB():
    def __init__(self):
        self._clnt = None
        self._em = None
        self._embeddingModel = None
        self._persitence = False

    def open(self, embeddingModel=None, path=None, device="cpu"):
        if self._clnt is not None:
            err("Chroma is ALREADY open")
            return False

        self._embeddingModel = embeddingModel

        if self._embeddingModel is None:
            wrn("Chroma default internal embedding is DISABLED [embeddingModel=None]")

        else:
            msg("Enabling embedding-model [device: {}]-> {} ..".format(device, self._embeddingModel))
            self._em = SentenceTransformerEmbeddingFunction(model_name=embeddingModel, device=device)

        self._path = path
        self._persitence = (self._path is not None) 

        if self._persitence: 
            self._clnt = chromadb.PersistentClient(path=path)
            msg("VectorStore OPEN [version:{}; persistence: {}; path: {}]".format(self._clnt.get_version(), self._persitence, self._path))

        else:
            self._clnt = chromadb.Client()
            msg("VectorStore OPEN [version:{}; no-persistence]".format(self._clnt.get_version()))

        return True
        
    def close(self):
        if self._clnt is None:
            err("Chroma is NOT open")
            return

        self._clnt = None

        if self._persitence:
            msg("Chroma CLOSED [persistence: {}]".format(self._path))
        else:
            msg("Chroma CLOSED [no-persistence]")

    # algo argument could be algo="ip" (euclidean)
    def openCollection(self, name, algo="cosine", createIfNotExists=True, em=None):
        if self._clnt is None:
            err("Chroma is NOT open")
            return None

        if createIfNotExists:
            collection = self._clnt.get_or_create_collection(
                name=name,
                metadata={"hnsw:space": algo}, 
                embedding_function=em
            )

        else:
            collection = self._engine.get_collection(
                name=name, 
                metadata={"hnsw:space": algo}, 
                embedding_function=em
            )
        
        msg("Collection is READY: {} [{} items] - Similarity algorithm -> {}".format(name, collection.count(), algo))
        return collection
        #return ChromaCollection(engine=self._clnt, em=self._em, name=name, algo="cosine", createIfNotExists=True)

    # list all collections
    # client.list_collections()
    
    def deleteCollection(self, name):
        if self._clnt is None:
            err("Chroma is NOT open")
            return None

        self._clnt.delete_collection(name=name)
        msg("Collection DELETED: {}".format(name))

    def countCollections(self):
        return self._clnt.count_collections()

    def collections(self):
        return self._clnt.list_collections()

    def collection(self, name):
        return self._clnt.get_collection(name=name)

    def internalEmbeddingFunction(self):
        return self._em

    def internalClient(self):
        return self._clnt

    def clear(self):
        self._clnt.client.reset()

    def version(self):
        return self._clnt.get_version()

###############################################################################

'''
class ChromaCollection():
    def __init__(self, engine, em, name, algo="cosine", createIfNotExists=True):
        self._engine = engine
        self._name = name
        #self._indicator = Indicator()
        #chroma_db_impl="duckdb+parquet"
        #metadata = {
        #    "hnsw:space": algo # l2 is the default
        #}# ERROR

        if createIfNotExists:
            self._collection = self._engine.get_or_create_collection(
                name=self._name,
                metadata={"hnsw:space": algo}, 
                embedding_function=em
            )

        else:
            self._collection = self._engine.get_collection(name=self._name, embedding_function=em)#metadata=metadata

        print("Collection is READY: {}".format(self._name))

    def rename(self, name):
        print("Collection name CHANGED: '{}' -> '{}'".format(self._name, name))
        self._name = name
        self._collection.modify(name=self._name) 

    def setItem(self, id, doc, meta={}):
        if len(meta) == 0:
            self._collection.upsert(ids=[id], documents=[doc])

        else:
            self._collection.upsert(ids=[id], metadatas=[meta], documents=[doc])

    def updItem(self, id, doc, meta={}):
        self._collection.update(ids=[id], metadatas=[meta], documents=[doc])

    def delItem(self, id):
        self._collection._collection.delete(ids=[id])

    def delItems(self, where):
        self._collection.delete(where=where)

    def delItemsByID(self, ids):
        self._collection.delete(ids=ids)

    def getItem(self, id, include=None):
        return self._collection.get(ids=[id], include=include)

    def getEmbeddings(self, id):
        return self.getItem(id=id, include=["embeddings"])["embeddings"][0]

    def selectFromText(self, texts, where=None, maxResults=1):
        if where is None:
            return self._collection.query(query_texts=texts, n_results=maxResults)

        return self._collection.query(query_texts=texts, where=where, n_results=maxResults)

    def selectFromEmbeddings(self, embeddings, where=None, maxResults=1):
        if where is None:
            return self._collection.query(embeddings=[embeddings], n_results=maxResults)

        return self._collection.query(embeddings=[embeddings], where=where, n_results=maxResults)

    def count(self):
        return self._collection.count()

'''
###############################################################################
#only testing for now
'''
    def analyzeSimilarity(self, text, maxResults=10, where=None):
        if where is None:
            results = self._collection.query(query_texts=[text], n_results=maxResults)

        else:
            results = self._collection.query(query_texts=[text], where=where, n_results=maxResults)

        q_ids = results["ids"][0]
        q_docs = results["documents"][0]
        q_dists = results["distances"][0]
        q_metas = results["metadatas"][0]
        count = len(q_dists)

        if count == 0:
            return {}

        for id, dist in zip(q_ids, q_dists):
            print("ChromaQuery RESULT item (id: {}; distance: {})".format(id, dist))

        return q_docs
'''
###############################################################################
'''
import chromadb
from sentence_transformers import SentenceTransformer

# Carica il modello di embedding
model = SentenceTransformer('paraphrase-multilingual-MiniLM-L12-v2')

# Inizializza ChromaDB
client = chromadb.Client()

# Crea una collezione
collection = client.create_collection(name="multilingual_text")

# Funzione per ottenere embeddings
def get_embeddings(texts):
    return model.encode(texts)

# Aggiungi frasi e embeddings alla collezione
texts = ["frase1", "frase2", "frase3"]
embeddings = get_embeddings(texts)
collection.add(texts=texts, embeddings=embeddings)

# Ricerca per similarità
query = "frase di ricerca"
query_embedding = get_embeddings([query])[0]
results = collection.query(embedding=query_embedding, n_results=5)

# Visualizza i risultati
for result in results:
    print(result['text'])
'''










'''
    collection.add(
        documents = [student_info, club_info, university_info],
        metadatas = [{"source": "student info"},{"source": "club info"},{'source':'university info'}],
        ids = ["id1", "id2", "id3"]
        )

    collection2.add(
        embeddings = [embeddings],
        documents = [student_info],
        metadatas = [{"source": "student info"}],
        ids = ["id1"]
    )
'''
        
'''
    where={"metadata_field": "is_equal_to_this"}
    where_document={"$contains":"search_string"}

    results = collection.query(
        query_texts=["What is the student name?"], QUERY with NATURAL LANGUAGE
        n_results=2
    )
     
    #.get() ALL RECORDS
    collection.get(
        ids=["id1", "id2", "id3", ...],
        where={"style": "style1"}
    )
    # Only get documents and ids
    collection.get(
        include=["documents"]
    )

    collection.query(
        query_embeddings=[[11.1, 12.1, 13.1],[1.1, 2.3, 3.2], ...],
        include=["documents"]
    )

    Filtering metadata supports the following operators:

    $eq - equal to (string, int, float)
    $ne - not equal to (string, int, float)
    $gt - greater than (int, float)
    $gte - greater than or equal to (int, float)
    $lt - less than (int, float)
    $lte - less than or equal to (int, float)

    {
        "metadata_field": "search_string"
    }
    # is equivalent to
    {
        "metadata_field": {
            "$eq": "search_string"
        }
    }

    Where filters only search embeddings where the key exists.
    If you search collection.get(where={"version": {"$ne": 1}}).
    Metadata that does not have the key version will not be returned.

    "$and": [
        {
            "metadata_field": {
                <Operator>: <Value>
            }
        },
        {
            "metadata_field": {
                <Operator>: <Value>
            }
        }
    ]
'''