#!/usr/bin/env python

###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import traceback
import sys
import os

from threading import Thread

version = "1.0"

asThread = None
thread = None
threadRunning = True

main_thread_function = None

setup_function = None
loop_function = None
close_function = None

mainThName = "main"

setupName = "setup"
loopName = "loop"
closeName = "close"

def load():
    global asThread
    global main_thread_function
    global setup_function
    global loop_function
    global close_function

    print("###############################################################################")
    print("PySketch LOADING ..")
    print("###############################################################################")

    print("PySketch version: {}".format(version))

    if len(sys.argv) < 2:
        print("Sketch path NOT specified")
        return False

    sketchPath = sys.argv[1]

    if not os.path.exists(sketchPath):
        print("Sketch NOT found: '{}'".format(sketchPath))
        return False
    
    sketchAbsPath = os.path.abspath(sketchPath)
    print("Importing sketch: '{}'".format(sketchAbsPath))
    print("###############################################################################")

    sys.path.append(os.path.dirname(sketchAbsPath))

    try:
        sketchName = os.path.splitext(os.path.basename(sketchPath))[0]
        sketch = __import__(sketchName)

        if hasattr(sketch, mainThName):
            main_thread_function = getattr(sketch, mainThName)

        #print("###", sketch, setupName)

        if not hasattr(sketch, setupName):
            print("{}() function NOT found!".format(setupName))
            return False

        setup_function = getattr(sketch, setupName)

        if not hasattr(sketch, loopName):
            print("{}() function NOT found!".format(loopName))
            return False
            
        loop_function = getattr(sketch, loopName)

        if hasattr(sketch, closeName):
            close_function = getattr(sketch, closeName)

    except (ImportError, AttributeError):
        traceback.print_exc()
        print("CANNOT import the Sketch from: '{}'".format(sketchPath))
        return False

    return True
    
'''
import signal

def signalHandler(signal, frame):
    cri("Signal received, exiting ..")
    raise KeyboardInterrupt
'''

def run():
    global thread
    print("PySketch RUNNING ..")
    print("###############################################################################")

    try:
        ok = setup_function()

        if ok:
            while threadRunning:
                if not loop_function():
                    break

        '''
        if thread is not None:
            os.kill(os.getpid(), signal.SIGINT)
            raise KeyboardInterrupt
        '''

    except KeyboardInterrupt:
        print("\rForcing exit with CTRL-C")

    finally:
        print("\nClosing ..")
        
        try:
            if close_function is not None:
                close_function()
                
        except Exception as e:
            print("Closing ERROR: {}".format(e))

        print("\nHALTED")
    
    print("###############################################################################")

###############################################################################

def main():
    global thread
    global threadRunning
    
    print("Main thread is going to die ..")

    try:
        if not load():
            exit(1)

        if main_thread_function:
            print("The PySketch will run on OPTIONAL thread")
            thread = Thread(target=run)
            thread.start()
            print("PySketch thread ENABLED: {}".format(thread))
            
            print("PySketch MAIN exec ..")

            main_thread_function()


        else:
            print("The PySketch will run on the MAIN thread")
            run()

        threadRunning = False

        if thread is not None and thread.is_alive():
            thread.join()
            
    except KeyboardInterrupt:
        print("\rForcing exit ..")

        
    print("\rTerminating ..")
    
###############################################################################

if __name__ == "__main__":
    main()

###############################################################################
