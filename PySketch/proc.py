###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import os
import subprocess

from PySketch.log   import dbg, dbg, wrn, err, cri

def checkExecutable(exe):
    if os.path.isabs(exe) and os.path.exists(exe):
        return True

    paths = os.environ['PATH'].split(os.pathsep)
    basename = os.path.basename(exe)
    
    for path in paths:
        if os.path.exists(os.path.join(path, basename)):
            return True
    
    err("Executable NOT found: {}".format(exe))
    return False

def launchProcess(cli):
    if not checkExecutable(cli[0]):
        return False

    exitCode = 1
    dbg("Launching process: {}".format(cli))

    try:
        exitCode = subprocess.call(cli)

    except Exception as e:
        err("Process ERROR: {}".format(cli))
        err(e)
        return False

    if exitCode != 0:
        err("Process exited with ERRORs: {}".format(cli))
        return False

    return True

def grabProcessOutput(cli):
    result = None

    try:
        result = subprocess.run(cli, capture_output=True, text=True, check=True)

    except subprocess.CalledProcessError as e:
        err("CANNOT execute binary: {} -> ".format(cli, e))
        return None
    
    return result.stdout, result.stderr