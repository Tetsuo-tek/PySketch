import html

from PySketch.log               import msg, dbg, wrn, err, cri

# WuiElement uses JQuery style, where each method returns the object himself, allowing to build concise calls chain

autoClosingTags = [
    "area"
    "base"
    "br"
    "col"
    "embed"
    "hr"
    "img"
    "input"
    "link"
    "meta"
    "source"
    "track"
    "wbr"
]

class WuiElement():
    def __init__(self, elementID=None, tag=None, attrs=None, text=None, parent=None):
        self._parent = None

        self._id = elementID
        self._tag = tag
        self._attrs = attrs
        self._text = text
        self._children = []

        if parent is not None:
            self.setParent(parent)
    
###############################################################################

    def setID(self, elementID):
        self._id = id
        return self

    def setTag(self, tag):
        self._tag = tag
        return self
        
    def setAttributes(self, attrs):
        self._attrs = attrs
        return self
        
    def setText(self, text):
        self._text = text
        return self

    def setParent(self, parent):
        self._parent = parent
        self._parent._children.append(self)
        return self

###############################################################################

    def getID(self):
        return self._id

    def getTag(self):
        return self._tag

    def getAttributes(self):
        return self._attrs

    def getParent(self):
        return self._parent

    def getChildren(self):
        return self._children

###############################################################################

    def toHtml(self, tab=2, level=0):
        html = ""
        isTextNode = self._tag is not None

        if isTextNode:
            tabStr = " " * tab * level
            
            if level > 0:
                html = "\r\n"
            
            html += "{}<{}".format(tabStr, self._tag)

            if self._attrs is not None:
                for k, v in self._attrs.items():
                    html += " {}=\"{}\"".format(k, v)

            html += ">"

            if self._tag in autoClosingTags:
                if len(self._children) > 0:
                    err("Auto-closing tags CANNOT have children [this={}]".format(self._tag))
                    return None
                    
                html += "\r\n{}".format(tabStr)

            else:
                if len(self._children) > 0:
                    level += 1
                    
                    for w in self._children:
                        html += w.toHtml(tab=tab, level=level)
                    
                    html += "\r\n{}".format(tabStr)

                html += "</{}>".format(self._tag)
            
        elif self._text is not None and len(self._text) > 0:
            text = ""

            if self._tag == "pre":
                text = html.escape(self._text)

            else:
                text = self._text

            html += "{}".format(text)

        return html

###############################################################################
"""
### **Tag Autochiudenti**
Questi tag non necessitano di una chiusura esplicita (`</tag>`).
Possono essere chiusi implicitamente al momento della loro apertura.

- `<area>`: Definisce una regione all'interno di una mappa immagine.
- `<base>`: Specifica l'URL base per i link relativi in un documento.
- `<br>`: Inserisce una linea di interruzione.
- `<col>`: Specifica le proprietà delle colonne in una tabella.
- `<embed>`: Incorpora contenuti esterni come video o plugin.
- `<hr>`: Inserisce una linea orizzontale.
- `<img>`: Mostra un'immagine.
- `<input>`: Crea un campo di input per moduli.
- `<link>`: Collega risorse esterne, come fogli di stile.
- `<meta>`: Fornisce metadati sul documento.
- `<source>`: Specifica risorse multimediali per `<audio>` o `<video>`.
- `<track>`: Aggiunge sottotitoli o tracce testuali per `<video>` o `<audio>`.
- `<wbr>`: Suggerisce un punto di interruzione del testo.

---

### **Tag Non Autochiudenti**
Questi tag richiedono una chiusura esplicita (`</tag>`) e possono contenere contenuto.

#### Struttura del documento:
- `<html>`: Radice del documento HTML.
- `<head>`: Contiene informazioni di intestazione.
- `<body>`: Contiene il contenuto visibile della pagina.

#### Testo e contenuto:
- `<div>`: Contenitore generico.
- `<span>`: Contenitore inline generico.
- `<p>`: Paragrafo.
- `<h1>` a `<h6>`: Intestazioni.
- `<a>`: Collegamento ipertestuale.
- `<b>`, `<i>`, `<u>`, `<strong>`, `<em>`: Stile del testo.

#### Liste:
- `<ul>`: Lista non ordinata.
- `<ol>`: Lista ordinata.
- `<li>`: Elemento della lista.
- `<dl>`: Lista di definizioni.
- `<dt>`: Termine della definizione.
- `<dd>`: Descrizione della definizione.

#### Tabelle:
- `<table>`: Definisce una tabella.
- `<thead>`: Intestazione della tabella.
- `<tbody>`: Corpo della tabella.
- `<tfoot>`: Piede della tabella.
- `<tr>`: Riga della tabella.
- `<td>`: Cella della tabella.
- `<th>`: Intestazione della tabella.
- `<caption>`: Didascalia della tabella.

#### Multimediale:
- `<audio>`: Contenitore per audio.
- `<video>`: Contenitore per video.
- `<canvas>`: Area di disegno grafico.
- `<svg>`: Grafica vettoriale.

#### Moduli:
- `<form>`: Modulo HTML.
- `<textarea>`: Area di testo.
- `<button>`: Pulsante.
- `<select>`: Menu a discesa.
- `<option>`: Opzione in un menu `<select>`.
- `<optgroup>`: Raggruppa opzioni in `<select>`.

#### Script e stile:
- `<script>`: Script JavaScript.
- `<style>`: Stili CSS.
- `<noscript>`: Contenuto mostrato quando JavaScript è disabilitato.
"""