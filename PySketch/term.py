###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import sys
import readchar

from PySketch.log   import msg, dbg, wrn, err, cri

def requestForChoice(labels):
    c = len(labels)

    if c == 0:
        err("Selection list is EMPTY!")
        return -1

    elif c == 1:
        return 0

    wrn("Select INDEX in [1,{}] ('q', 'Q', ESC to cancel) ..".format(len(labels)))

    for i, item in enumerate(labels):
        msg("{} - {}".format(i + 1, item))

    loopStep = True
    index = -1

    while loopStep:
        try:
            sys.stdout.write("> ")
            sys.stdout.flush()
            
            choice = sys.stdin.readline().rstrip("\n")
            
        except KeyboardInterrupt:
            err("Forcing exit with CTRL-C")
            return -1

        if choice.lower() == "q":
            loopStep = False

        if len(choice) == 0:
            err("Choice is NOT valid, retry please ('q', 'Q', ESC to cancel) ..")

        else:
            index = int(choice) - 1

            if 0 <= index < c:
                loopStep = False

            else:
                wrn("Selected index is NOT valid ({}), retry please ('q', 'Q', ESC to cancel) ..".format(index))

    if index == -1:
        wrn("Cancelled!")

    return index

def question(text):
    wrn(text)

    sys.stdout.write("> ")
    sys.stdout.flush()

    try:
        return sys.stdin.readline().rstrip("\n")

    except KeyboardInterrupt:
        err("Forcing exit with CTRL-C")
        return None

    return ""

def booleanQuestion(text, dflt=True):
    wrn(text)

    optTrue = "y"
    optFalse = "n"
    
    if dflt:
        optTrue = "Y"
    else:
        optFalse = "N"

    loopStep = True
    ok = False

    while loopStep:
        try:
            sys.stdout.write("[{}/{}]> ".format(optTrue, optFalse))
            sys.stdout.flush()

            choice = readchar.readchar()

            sys.stdout.write("\r       ")
            sys.stdout.flush()
            sys.stdout.write("\r")
            sys.stdout.flush()

        except KeyboardInterrupt:
            err("Forcing exit with CTRL-C")
            return None

        if choice.lower() != "y" and choice.lower() != ""  and choice.lower() != "n":
            err("Selected answer is NOT valid (allowed y or n), retry please ..")

        else:
            if choice == "":
                ok = dflt
            
            else:
                ok = (choice.lower() == "y")

            loopStep = False

    return ok