###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import time

from PySketch.elapsedtime   import ElapsedTime

###############################################################################

class TimeStepper:
    def __init__(self):
        self._tickChrono = ElapsedTime()
        self._avgChrono = ElapsedTime()
        self._avg = 0
        self._currAvg = 0
        self._offAvg = 0
        self._currOffAvg = 0
        self._ratio = -1
        self._c = 0
        self._currErr = 0
        self._tickTime = 0.005
        self._checkInterval = 0.050
        self._t = self._tickTime
        self._currTime = self._tickTime
        self._tempTimeOffset = 0
        self._cycleStatus = False
    
    def setInterval(self, interval, checkInterval):
        self._tickTime = interval
        self._t = self._tickTime
        self._currTime = self._tickTime
        self._currAvg = self._currTime
        self._checkInterval = checkInterval

    def wait(self):
        self._currTime = self._tickChrono.stop()
        self._tickChrono.start()

        if self._avgChrono.stop() < self._checkInterval:
            self._c += 1
            self._avg += self._currTime
            self._offAvg += (self._tickTime - self._currTime)
            self._cycleStatus = False

        else:
            if self._avg == 0:
                self._cycleStatus = False

            else:
                self._currAvg = self._avg / self._c
                self._currOffAvg = self._offAvg / self._c
                
                self._ratio = self._tickTime / self._currAvg
                self._currErr = 1 - self._ratio

                self._t += self._currOffAvg
                #self._t *= self._ratio

                if self._t < 0:
                    self._t = 0.0001

                self._c = 0
                self._avg = 0
                self._offAvg = 0

                self._cycleStatus = True

            self._avgChrono.start()

        time.sleep(self._t)

###############################################################################