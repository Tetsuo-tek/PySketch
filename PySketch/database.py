###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import sqlite3
import os

from datetime           import datetime

from PySketch.fs        import *
from PySketch.log       import msg, dbg, wrn, err, cri, printPair

###############################################################################

class Query():
    def __init__(self, db):
        self._db = db
        self._limit = None
        self._offset = None
        #self._dbCursor = None
        self._iterator = None

    def setLimit(self, limit, offset=0):
        self._limit = limit
        self._offset = offset

    def exec(self, query, values=None):
        if self._limit is not None:
            query += " LIMIT {}".format(self._limit)

        if self._offset is not None and self._offset > 0:
            query += " OFFSET {}".format(self._offset)

        res = None
        #self._dbCursor = self._db.cursor()

        if values is None:
            res = self._db.query(query)
        
        else:
            res = self._db.query(query, values)
        
        if res is None:
            return False

        self._dbCursor = res
        self._iterator = iter(self._dbCursor)
        return True

    def next(self):
        if self._iterator is None:
            return None

        try:
            nextRow = next(self._iterator)

        except StopIteration:
            self._iterator = None
            self._dbCursor = None
            return None

        #print("Columns Description:", self._dbCursor.description)
        columnNames = [description[0] for description in self._dbCursor.description]
        return dict(zip(columnNames, nextRow))

###############################################################################

class Field():
    def __init__(self, db, table, properties):
        self._db = db
        self._table = table
        self._properties = properties

        self.cid = -1
        self.name = None
        self.t = None
        self.pk = False
        self.notNull = False
        self.dfltValue = None

        self.indexedFrom = []

        self._init()

    def _init(self):
        (cid, name, t, notNull, dfltValue, pk) = self._properties
        
        self.cid = cid
        self.name = name
        self.t = t
        self.pk = bool(pk)
        self.notNull = bool(notNull)
        self.dfltValue = dfltValue

        '''
        print("Field: {}.{} [T: {}; CID: {}; Pk: {}; NotNull: {}; Default: {}]"
            .format(
                self._table._name,
                self.name,
                self.t,
                self.cid,
                self.pk,
                self.notNull,
                self.dfltValue
            )
        )
        '''

###############################################################################

class Index():
    def __init__(self, db, table, name, isUnique):
        self._db = db
        self._table = table

        self.name = name
        self.isUnique = isUnique
        self.indexed = None

        self._init()

    def _init(self):
        #print("Index: {}.{} [IsUnique: {}]".format(self._table._name, self.name, self.isUnique))

        ret = self._db.query("PRAGMA index_info({})".format(self.name))

        if ret is None:
            return False

        indexInfos = ret.fetchall()
        self.indexed = []

        for idx in indexInfos:
            seqno, cid, name = idx
            #print("   {} -> {}.{} [CID: {}; SeqNo:{}]".format(self.name, self._table._name, name, cid, seqno))
            self.indexed.append(name)

###############################################################################

class ForeignKey():
    def __init__(self, db, table, properties):
        self._db = db
        self._table = table
        self._properties = properties

        self._init()

    def _init(self):
        (id_, seq, table, from_, to, onUpdate, onDelete, match) = self._properties
        
        self.id = id_
        self.seq = seq
        self.fromTableName = self._table._name
        self.fromFieldName = from_
        self.toTableName = table
        self.toFieldName = to
        self.onUpdate = onUpdate
        self.onDelete = onDelete
        self.match = match
        '''
        print("ForeignKey: {}.{} -> {}.{} [ID: {}, Match: {}; Seq: {}]"
            .format(
                self.fromTableName,
                self.fromFieldName,
                self.toTableName,
                self.toFieldName,
                self.id,
                self.match,
                self.seq
            )
        )

        print("        OnUpdate: {}".format(self.onUpdate))
        print("        OnDelete: {}".format(self.onDelete))
        '''

###############################################################################

class Table():
    def __init__(self, db, name):
        self._db = db
        self._name = name

        self._fields = None
        self._indexes = None
        self._foreignKeys = None

        self._init()

    def _init(self):
        if not self._db.hasTable(self._name):
            err("Table NOT found: {}".format(self._name))
            return

        ret = self._db.query("PRAGMA table_info({})".format(self._name))

        if ret is None:
            return False
        
        # FIELDs
        fields = ret.fetchall()
        self._fields = {}

        for field in fields:
            f = Field(self._db, self, field)
            self._fields[f.name] = f

            #print()

        ret = self._db.query("PRAGMA index_list({})".format(self._name))

        if ret is None:
            return False

        # INDEXes
        indexes = ret.fetchall()
        self._indexes = {}

        for index in indexes:
            name = index[1]
            isUnique = bool(index[2])

            idx = Index(self._db, self, name, isUnique)
            self._indexes[name] = idx

            if len(idx.indexed) == 1:
                f = idx.indexed[0]

                if f in self._fields:
                    field = self._fields[f]
                    field.isUnique = idx.isUnique
                    field.indexedFrom.append(name)
                    #print("\n* UNIQUE field checked on: {} -> {}.{}".format(idx.name, self._name, f))

                else:
                    err("\n!! Field name NOT found: {}.{}".format(self._name, f))

            else:
                for f in idx.indexed:
                    if f in self._fields:
                        err("\n!! Field name NOT found: {}.{}".format(self._name, f))
                        continue

                    field = self._fields[f]
                    field.indexedFrom.append(name)

            #print()

        ret = self._db.query("PRAGMA foreign_key_list({})".format(self._name))
        
        if ret is None:
            return False

        # FKs
        foreignKeys = ret.fetchall()
        self._foreignKeys = {}

        for foreignKey in foreignKeys:
            fk = ForeignKey(self._db, self, foreignKey)
            self._foreignKeys[fk.fromFieldName] = fk

            #print()

    def count(self, where=None, values=None):
        if values is not None and type(values) != tuple:
            err("'values' MUST be a TUPLE of values, instead of: {}".format(type(values)))
            return -1

        query = "SELECT COUNT(*) FROM {}".format(self._name)
        
        if where is not None:
            query += " WHERE {}".format(where)

        if values is None:
            return self._db.oneResult(query)

        return self._db.oneResult(query, values)

    def exists(self, where=None, values=None):
        query = "SELECT EXISTS(SELECT 1 FROM {}".format(self._name)

        if where is None:
            query += ")".format(where)
        
        else:
            query += " WHERE {})".format(where)

        if values is None:
            return bool(self._db.oneResult(query))

        return bool(self._db.oneResult(query, values))

    def fieldNames(self):
        return list(fields.keys())

###############################################################################

    def insert(self, fields, values):
        if type(fields) != list:
            err("'fields' MUST be a LIST of names, instead of: {}".format(type(fields)))
            return False

        if type(values) != tuple:
            err("'values' MUST be a TUPLE of values, instead of: {}".format(type(values)))
            return False
        
        for v in values:
            if not isinstance(v, str) and not isinstance(v, int) and not isinstance(v, float) and not isinstance(v, bytearray):
                err("Data type NOT allowed: {} - {}".format(type(v), v))
                return False

        if "created" in fields:
            err("'created' field is automatic; do NOT set its value! Setting fields: {}".format(fields))
            return False

        tempFields = list(fields)
        #currentDT = datetime.now().strftime('%Y-%m-%d')
        currentDT = datetime.now().isoformat()

        tempFields.append("created")
        values = values + (currentDT,)
        
        valuesSymbols = ["?"] * len(tempFields)
        valuesSymbols = ",".join(valuesSymbols)
        query = "INSERT INTO {} ({}) VALUES ({})".format(self._name, ",".join(tempFields), valuesSymbols)

        if self._db.query(query, values) is None:
            return False

        self._lastID = self._db.lastID()
        return True

    def lastID(self):
        return self._lastID

    def update(self, record, where, values):
        pass
        
    def remove(self, where, values):
        if values is not None and type(values) != tuple:
            err("'values' MUST be a TUPLE of values, instead of: {}".format(type(values)))
            return False

        query = "DELETE FROM {}".format(self._name)

        if where is not None:
            query += " WHERE {}".format(where)

        if values is None:
            return (self._db.query(query) is not None)

        return (self._db.query(query, values) is not None)

    def clear(self):
        return self.remove(where=None, values=None)

###############################################################################

    def select(self, fields=[], where=None, values=None):
        if values is not None and type(values) != tuple:
            err("'values' MUST be a TUPLE of values, instead of: {}".format(type(values)))
            return False

        if len(fields) == 0:
            fields = ["*"]

        query = "SELECT {} FROM {}".format(", ".join(fields), self._name)

        if where is not None:
            query += " WHERE {}".format(where)

        if values is None:
            return self._db.query(query)

        return self._db.query(query, values)

###############################################################################

    def database(self):
        return self._db

    def fieldNames(self, name):
        return list(self._fields.keys())

    def indexNames(self, name):
        return list(self._indexes.keys())

    def getField(self, name):
        if name in self._fields:
            return self._fields[name]

        return None

    def getIndex(self, name):
        if name in self._indexes:
            return self._indexes[name]

        return None

###############################################################################

class Database():
    def __init__(self):
        self._reset()

###############################################################################

    def _reset(self):
        self._path = None
        self._name = None
        self._db = None

###############################################################################

    def open(
            self,
            path,
            storeMode="DEFAULT",
            enableForeignKeys=True,
            synchMode="NORMAL",
            journalMode="WAL",
            autoVacuumMode="INCREMENTAL",
            pageSize=4096,
            cacheSize=500000,
            optimize=True
        ):

        if self._db is not None:
            err("Database is ALREADY open")
            return False

        chk = ["DEFAULT", "FILE", "MEMORY"]

        if storeMode not in chk:
            err("Arguments ERROR - 'storeMode' not in {}: {}".format(chk, storeMode))
            return False
            
        chk = ["OFF", "NORMAL", "FULL", "EXTRA"]

        if synchMode not in chk:
            err("Arguments ERROR - 'synchMode' not in {}: {}".format(chk, synchMode))
            return False

        chk = ["DELETE", "TRUNCATE", "PERSIST", "MEMORY", "WAL", "OFF"]

        if journalMode not in chk:
            err("Arguments ERROR - 'journalMode' not in {}: {}".format(chk, journalMode))
            return False

        chk = ["NONE", "FULL", "INCREMENTAL"]

        if autoVacuumMode not in chk:
            err("Arguments ERROR - 'autoVacuumMode' not in {}: {}".format(chk, autoVacuumMode))
            return False

        self._path = os.path.abspath(path)
        self._name = os.path.basename(self._path)

        try:
            self._db = sqlite3.connect(self._path)

        except sqlite3.Error as e:
            err("Database ERROR ({}) -> {}".format(self._name, e))

            if self.isOpen():
                self.close()
                
            else:
                self._reset()

            return False

        if enableForeignKeys:
            enableForeignKeys = "ON"
        else:
            enableForeignKeys = "OFF"

        err = (
            self.query("PRAGMA temp_store={}".format(storeMode)) is None
            or self.query("PRAGMA encoding=UTF8") is None
            or self.query("PRAGMA synchronous={}".format(synchMode)) is None
            or self.query("PRAGMA journal_mode={}".format(journalMode)) is None
            or self.query("PRAGMA cache_size={}".format(cacheSize)) is None      
            or self.query("PRAGMA page_size={}".format(pageSize)) is None      
            or self.query("PRAGMA auto_vacuum={}".format(autoVacuumMode)) is None      
            or self.query("PRAGMA foreign_keys={}".format(enableForeignKeys)) is None
        )

        #PRAGMA locking_mode = EXCLUSIVE;
        #PRAGMA busy_timeout = 5000;
        #PRAGMA mmap_size = 268435456
        #PRAGMA user_version = 1;

        self.optimize()

        if err:
            if self.isOpen():
                self.close()

            else:
                self._reset()

            return False

        msg("Database OPEN ({}): {}".format(self._name, self._path))
        return True

    def close(self):
        if self._db is None:
            err("Database is NOT open yet")
            return False

        self._db.close()
        msg("Database CLOSED ({}): {}".format(self._name, self._path))
        self._reset()

        return True

###############################################################################

    def count(self, query, values=None):
        if values is not None and type(values) != tuple:
            err("'values' MUST be a TUPLE of values, instead of: {}".format(type(values)))
            return -1

        query = "SELECT COUNT(*) FROM ({})".format(query)
    
        if values is None:
            return self.oneResult(query)

        return self.oneResult(query, values)

    def query(self, query, values=None): 
        res = None

        cursor = self.cursor()

        try:
            if values is None:
                res = cursor.execute(query)
            else:
                res = cursor.execute(query, values)

        except sqlite3.IntegrityError as e:
            err("Integrity ERROR ({}) -> {} - {}".format(self._name, e, query))
            return None

        except ValueError as e:
            err("ValueError ERROR ({}) -> {} - {}".format(self._name, e, query))
            return None

        except sqlite3.Error as e:
            err("Database ERROR ({}) -> {} - {}".format(self._name, e, query))
            return None
        
        return cursor

    def oneResult(self, query, values=None):
        cursor = self.query(query, values)

        if cursor is None:
            return None
        
        return cursor.fetchone()[0]

    def lastID(self):
        return self.query("SELECT last_insert_rowid()").fetchone()[0]

    def cursor(self):
        return self._db.cursor()

###############################################################################

    def begin(self):
        wrn("Database BEGIN!")
        self.query("BEGIN")

    def commit(self):
        if self._db is None:
            err("Database is NOT open yet!")
            return False
         
        try:
            wrn("Database COMMIT!")
            self._db.commit()

        except sqlite3.Error as e:
            err("Database ERROR ({}) -> {}".format(self._name, e))
            self._db.rollback()
            return False

        return True

    def rollback(self):
        if self._db is None:
            err("Database is NOT open yet!")
            return False
         
        try:   
            wrn("Database ROLLBACK!")
            self._db.rollback()

        except sqlite3.Error as e:
            err("Database ERROR({}) -> {}".format(self._name, e))
            return False

        return True

###############################################################################

    def createTable(self, tableName, fields):
        if self.hasTable(tableName):
            err("Table ALREADY exists: {}".format(tableName))
            return False

        if type(fields) != dict:
            err("'fields' MUST be a DICT of 'fieldName':'formalType', instead of: {}".format(type(fields)))
            return False

        if "id" in fields:
            del fields["id"]

        if "created" in fields:
            del fields["created"]

        #IF NOT EXISTS
        query = "CREATE TABLE {} (\n" \
            "   id INTEGER PRIMARY KEY,".format(tableName)

        for name, props in fields.items():
            query += "\n   {} {},".format(name, props)
            
        query += "\n   created DATETIME\n)\n"

        return (self.query(query) is not None)

    def dropTable(self, tableName, fields):
        if not self.hasTable(tableName):
            err("Table NOT found: {}".format(tableName))
            return False

        #IF NOT EXISTS
        query = "DROP TABLE {}".format(tableName)

        return (self.query(query) is not None)

###############################################################################

    def vacuum(self):
        return (self.query("VACUUM") is not None)

    def optimize(self):
        return (self.query("PRAGMA optimize") is not None)
        
    def checkIntegrity(self):
        return self.query("PRAGMA integrity_check")

###############################################################################

    def isOpen(self):
        return (self._db is not None)

    def size():
        if not self.isOpen():
            retiurn -1
        
        return os.path.getsize(self._path)

    def hasTable(self, name):
        return (self.count("SELECT name FROM sqlite_master WHERE type='table' AND name='{}'".format(name)) == 1)

    def tables(self):
        ret = self.query("SELECT name FROM sqlite_master WHERE type='table'")

        if ret is None:
            return []

        l = []
        tables = ret.fetchall()

        for row in tables:
            tableName = row[0]

            if tableName == "sqlite_stat1":
                continue

            l.append(tableName)

        return l

###############################################################################
