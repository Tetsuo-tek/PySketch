###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

# IMPORTANT: THIS CLASS IS THINKED TO WORK FROM A Sk/PySketch connecting to other Satellites in P2P form, through ZeroMQ protocol

from PySketch.host          import HostMachine
from PySketch.signal        import functSignature, Signal, emit
from PySketch.zmq           import Publisher, Subscriber, Replier, Requester
from PySketch.server        import findFreePort
from PySketch.log           import logger, LogMachine, escapes, msg, dbg, wrn, err, cri, printPair

###############################################################################

class FlowSpotPublisherChannel:
    def __init__(self, publisher, loop) :
        self._loop = loop

        self.port = -1
        self.name = ""
        self.mime = ""

        self._svr = publisher

        self.publishStartRequired   = Signal(self._loop)
        self.publishStopRequired    = Signal(self._loop)

    def publish(self, data):
        pass

    def _onPublishReqStartCb(self):
        self.publishStartRequired.trigger(self)
        
    def _onPublishReqStopCb(self):
        

class FlowSpotReplierChannel:
    def __init__(self, replier):
        self.port = -1
        self.name = ""
        self.mime = ""

        self._svr = None

    def request(self, data):
        pass

###############################################################################

class FlowSpot():
    def __init__(self, loop) :
        self._loop = loop

        self._listenAddress = None

        self._publishersBasePort = -1
        self._repliersBasePort = -1

        self._channels = {} # by name

        self._subscribers = []
        self._requesters = []

        self._onStartChanPubReqCb   = None
        self._onStopChanPubReqCb    = None
        self._onGrabDataCb          = None

        self.publishStartRequired   = Signal(self._loop)
        self.publishStopRequired    = Signal(self._loop)
        self.subscribedDataCome     = Signal(self._loop)

        self._loop.slowZone.attach(self.onSlowTick)
        self._loop.oneSecZone.attach(self.onOneSecTick)

###############################################################################

    def setup(self, listenAddress="0.0.0.0", publishersBasePort=20000, repliersBasePort=30000)
        self._listenAddress = listenAddress
        self._publishersBasePort = publishersBasePort
        self._repliersBasePort = repliersBasePort

###############################################################################

    def setStartChanPubReqCallBack(self, cb):
        self._onStartChanPubReqCb = cb
        dbg("Callback ENABLED: {}{}".format(cb.__name__, functSignature(cb)))

    def setStopChanPubReqCallBack(self, cb):
        self._onStopChanPubReqCb = cb
        dbg("Callback ENABLED: {}{}".format(cb.__name__, functSignature(cb)))

    def setGrabDataCallBack(self, cb):
        self._onGrabDataCb = cb
        dbg("Callback ENABLED: {}{}".format(cb.__name__, functSignature(cb)))

###############################################################################
# SVR SECTION

    def addStreamingChannel(self, name, props={}, listenAddress=None, listenPort=-1):
        if name in self._channels:
            err("A SpotChannel with this name ALREADY extists: {}".format(name))
            return None

        pub = Publisher()

        if listenAddress is None:
            listenAddress = self._listenAddress
        
        if listenPort == -1:
            listenPort = findFreePort(maxPort=self._publishersBasePort)
        
        if listenPort == -1:
            err("No free ports found between {} and 65535".format(self._publishersBasePort, maxPort))
            return None
            
        pub.setup()

        self._channels[name] = pub
        self._loop.slowZone.attach(pub.tick)

        return pub

    def addServiceChannel(self, name, props={}, listenAddress=None, listenPort=-1):
        pass

    def addBlobChannel(self, name, props={}, listenAddress=None, listenPort=-1):
        pass

    def removeChannel(self, name):
        if name not in self._channels:
            err("SpotChannel NOT found: {}".format(name))
            return False

        ch = self._channels[name]
        ch.quit()

        del self._channels[name]
        return True

###############################################################################

    def subscribe(self, spotAddress=None, spotPort=-1):
        pass

###############################################################################

    def service(self, spotAddress=None, spotPort=-1):
        pass

###############################################################################

    def onSlowTick(self):
        pass

    def onOneSecTick(self):
        pass

###############################################################################