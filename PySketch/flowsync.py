###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import ctypes

from PySketch.flowproto     import FlowCommand, Flow_T, FlowChanID, FlowChannel_T
from PySketch.abstractflow  import FlowChannel, AbstractFlow, ProtocolRecvError
from PySketch.log           import msg, dbg, wrn, err, cri, printPair

from typing import Any

###############################################################################

class FlowSync(AbstractFlow):
    def __init__(self, loop) :
        super(FlowSync, self).__init__(loop)

    def existsOptionalPairDb(self, dbName):
        if (self._sck is None) or (not self._sck.isConnected()):
            err("SocketDevice is NOT initialized yet")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_EXISTS_DATABASE)
        self._p.sendString(dbName)
        self._p.sendEndOfTransaction()

        self._sck.waitForReadyRead()

        cmd = self._p.recvStartOfCmdTransaction()
        exists = self._p.recvBool()

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return False

        return exists

    def addDatabase(self, dbName):
        if (self._sck is None) or (not self._sck.isConnected()):
            err("SocketDevice is NOT initialized yet")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_ADD_DATABASE)
        self._p.sendString(dbName)
        self._p.sendEndOfTransaction()

        self._sck.waitForReadyRead()

        cmd = self._p.recvStartOfCmdTransaction()

        ok = self._p.recvBool()

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return False
        
        return ok
    
    def getCurrentDbName(self):
        if (self._sck is None) or (not self._sck.isConnected()):
            err("SocketDevice is NOT initialized yet")
            return ""
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_GET_DATABASE)
        self._p.sendEndOfTransaction()
        
        self._sck.waitForReadyRead()

        cmd = self._p.recvStartOfCmdTransaction()

        sz = self._p.recvSize()
        dbName = self._p.recvString(sz)

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return ""
        
        return dbName

    def variablesKeys(self):
        if (self._sck is None) or (not self._sck.isConnected()):
            err("SocketDevice is NOT initialized yet")
            return []
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_GET_VARIABLES_KEYS)
        self._p.sendEndOfTransaction()

        self._sck.waitForReadyRead()

        cmd = self._p.recvStartOfCmdTransaction()
        keys = self._p.recvList()

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return []
        
        return keys

    def getAllVariables(self):
        if (self._sck is None) or (not self._sck.isConnected()):
            err("SocketDevice is NOT initialized yet")
            return {}
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_GET_VARIABLES_JSON)
        self._p.sendEndOfTransaction()

        self._sck.waitForReadyRead()

        cmd = self._p.recvStartOfCmdTransaction()

        sz = self._p.recvSize()
        var = self._p.recvJSON(sz)

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return {}

        return var

    def existsVariable(self, key):
        if (self._sck is None) or (not self._sck.isConnected()):
            err("SocketDevice is NOT initialized yet")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_EXISTS_VARIABLE)
        self._p.sendString(key)
        self._p.sendEndOfTransaction()

        self._sck.waitForReadyRead()

        cmd = self._p.recvStartOfCmdTransaction()

        exists = self._p.recvBool()

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return False
        
        return exists

    def getVariable(self, key):
        if (self._sck is None) or (not self._sck.isConnected()):
            err("SocketDevice is NOT initialized yet")
            return Any()
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_GET_VARIABLE_JSON)
        self._p.sendString(key)
        self._p.sendEndOfTransaction()

        self._sck.waitForReadyRead()

        cmd = self._p.recvStartOfCmdTransaction()

        sz = self._p.recvSize()
        var = self._p.recvJSON(sz)

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return Any
        
        return var

    def dataGrabbingRegister(self, chanID):
        if (self._sck is None) or (not self._sck.isConnected()):
            err("SocketDevice is NOT initialized yet")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_GRAB_REGISTER_CHAN)
        self._p.sendChanID(chanID)
        
        # NO RESPONSE
        return self._p.sendEndOfTransaction()

    def dataGrabbingUnRegister(self, chanID):
        if (self._sck is None) or (not self._sck.isConnected()):
            err("SocketDevice is NOT initialized yet")
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_GRAB_UNREGISTER_CHAN)
        self._p.sendChanID(chanID)
        
        # NO RESPONSE
        return self._p.sendEndOfTransaction()

    def grabLastChannelData(self, chanID):
        if (self._sck is None) or (not self._sck.isConnected()):
            err("SocketDevice is NOT initialized yet")
            return bytearray()
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_GET_VARIABLE)
        self._p.sendString(chanID)
        self._p.sendEndOfTransaction()

        self._sck.waitForReadyRead()

        cmd = self._p.recvStartOfCmdTransaction()

        sz = self._p.recvSize()
        data = self._p.recvBuffer(sz)

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return bytearray()
        
        return data

    def sendServiceRequest(self, chanID, cmd, val):
        if (self._sck is None) or (not self._sck.isConnected()):
            err("SocketDevice is NOT initialized yet")
            return None
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_EXEC_SERVICE_REQUEST)
        self._p.sendChanID(chanID)
        self._p.sendString(cmd)
        self._p.sendJSON(val)
        self._p.sendEndOfTransaction()

        self._sck.waitForReadyRead()

        cmd = self._p.recvStartOfCmdTransaction()

        sz = self._p.recvSize()
        val = self._p.recvJSON(sz)

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return None
        
        dbg("Channel SYNC-REQUEST grabbed [ChanID: {}]".format(chanID))
        return val

###############################################################################

    def updateChannels(self):
        if (self._sck is None) or (not self._sck.isConnected()):
            err("SocketDevice is NOT initialized yet")
            return False
        
        if len(self._channelsNames) > 0:
            self._resetChannels()
        
        chansNames = self._getChannelsList()

        for chanName in chansNames:
            ch = self._getChannelProperties(chanName)
            self._channelsNames[ch.name] = ch
            self._channelsIndexes[ch.chanID] = ch

        return True

    def _getChannelsList(self):
        self._p.sendStartOfTransaction(FlowCommand.FCMD_GET_CHANS_LIST)
        self._p.sendEndOfTransaction();
        
        self._sck.waitForReadyRead()

        cmd = self._p.recvStartOfCmdTransaction()
        chansNames = self._p.recvList()

        if len(chansNames) == 0:
            wrn("Channels list is EMPTY")

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return []
            
        return chansNames

    def _getChannelProperties(self, name):
        ch = FlowChannel()
        ch.isPublishingEnabled = False

        self._p.sendStartOfTransaction(FlowCommand.FCMD_GET_CHAN_PROPS)
        self._p.sendString(name)
        self._p.sendEndOfTransaction()

        self._sck.waitForReadyRead()

        cmd = self._p.recvStartOfCmdTransaction()
        
        ch.chan_t = self._p.recvChanType()
        ch.chanID = self._p.recvChanID()

        sz = self._p.recvSize()
        ch.hashID = self._p.recvString(sz)

        if ch.chan_t == FlowChannel_T.StreamingChannel:
            ch.flow_t = self._p.recvFlowType()
            ch.t = self._p.recvVariantType()

            sz = self._p.recvSize()
            ch.mime = self._p.recvString(sz)

            '''
            sz = self._p.recvSize()
            ch.udm = self._p.recvString(sz)

            sz = self._p.recvSize()
            ch.hasHeader = self._p.recvJSON(sz)
            '''

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return FlowChannel()
        
        return ch

    def _getChannelHeader(self):
        return bytearray()

###############################################################################


    def _onOpenUnixSocket(self):
        return True

    def _onOpenTcpSocket(self):
        return True
    
    def _onLogin(self):
        return True

    def _onOpen(self):
        pass

    def _onClose(self):
        pass

    def _onDisconnected(self):
        pass

###############################################################################

