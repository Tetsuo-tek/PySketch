###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import os

from PySketch.fs        import *
from PySketch.database  import Database, Table
from PySketch.log       import msg, dbg, wrn, err, cri, printPair

###############################################################################

class FileItem():
    def __init__(self, fileIndex):
        self._fileIndex = fileIndex
        self._filePath = None
        self._fileName = None
        self._suffix = None
        self._sz = -1
        self._hash = None
        self._id = None

    def initialize(self, filePath):
        self._filePath = os.path.abspath(filePath)
        self._fileName = os.path.basename(self._filePath)
        _, self._suffix = os.path.splitext(self._filePath)

        if len(self._suffix) > 0 and self._suffix.startswith("."):
            self._suffix = self._suffix[1:]

        if not pathExists(self._filePath):
            err("File NOT found: {}".format(self._filePath))
            return False

        if not isReadable(self._filePath):
            err("CANNOT read file: {}".format(self._filePath))
            return False

        self._sz = os.path.getsize(self._filePath)

        if self._sz == 0:
            err("File exists but is EMPTY: {}".format(self._filePath))
            self._hash = None
            self._id = None
            return False

        self._hash = md5Digest(self._filePath)

        if self._fileIndex._table.exists("md5 = ? AND path = ?", (self._hash, self._filePath)):
            self._id = self._fileIndex.getID("md5 = ? AND path = ?", (self._hash, self._filePath))
            dbg("FileItem exists [rid={}]".format(self._id))

        else:
            table = self._fileIndex._table

            if table.exists("md5 = ?", (self._hash, )):
                wrn("Another path with the same hash-md5 is FOUND, removing it: {}".format(self._hash))

                if table.remove("path = ?", (self._filePath,)):
                    table.database().commit()

            if table.exists("path = ?", (self._filePath, )):
                wrn("File path is ALREADY stored, but it has modification and requires to UPDATE: {}", self._filePath)

            self._id = None

        return True

    def store(self):
        #if self.hashExists(self._hash):
        if self._id is not None:
            dbg("File ALREADY exists without modifications: {} -> {}",format(seld._hash, self._filePath))
            return False

        table = self._fileIndex._table

        if table.exists("path = ?", (self._filePath,)):
            wrn("File ALREADY exists, but it has modifications (removing): {}".format(self._filePath))

            if table.remove("path = ?", (self._filePath,)):
                table.database().commit()

        fields = ["path", "md5", "name", "size", "suffix"]
        values = (self._filePath, self._hash, self._fileName, self._sz, self._suffix)

        if not table.insert(fields, values):
            return False
        
        table.database().commit()
        self._id = table.database().lastID()
        msg("FileItem successfully STORED [rid={}]: {}".format(self._id, self._filePath))
        return True

    def isStoredPath(self):
        return self._fileIndex._table.exists("path = ?", (self._filePath,))

    def size(self):
        return self._sz
    
    def md5(self):
        return self._hash

    def path(self):
        return self._filePath

    def name(self):
        return self._fileName

    def suffix(self):
        return self._suffix

    def itemID(self):
        return self._id

###############################################################################

class FileIndex():
    def __init__(self):
        self._db = None
        self._name = None
        self._table = None

        self._fields = {
            "path": "TEXT UNIQUE NOT NULL",
            "md5": "TEXT UNIQUE NOT NULL",
            "name": "TEXT NOT NULL",
            "suffix": "TEXT",
            "size": "INTEGER NOT NULL"
        }

    def init(self, db, name="FileIndex"):
        self._db = db
        self._name = name

        if not self._db.hasTable(self._name):
            if self._db.createTable(self._name, self._fields):
                self._db.commit()
            else:
                return False

        self._table = Table(self._db, self._name)
        return True
    
    def fileItem(self, filePath):
        item = FileItem(self)
        
        if item.initialize(filePath):
            return item
        
        return None

    def getID(self, where, values=None):
        query = "SELECT id FROM {} WHERE {}".format(self._table._name, where)
        return self._table.database().oneResult(query, values)

    def database(self):
        return self._db

###############################################################################
