###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import socket
import select

from PySketch.signal        import functSignature
from PySketch.log           import msg, dbg, wrn, err, cri, printPair

###############################################################################

class TcpServer:
    def __init__(self):
        self._svr = None
        self._port = -1
        self._addr = "0.0.0.0"
        self._sckCheck = None
        self._onAcceptCb = None
        self._maxQueuedSockets = 4

###############################################################################

    def setup(self, listenAddr, listenPort, acceptCallBack, maxQueuedSockets=10):
        self._port = listenPort
        self._addr = listenAddr

        self._onAcceptCb = acceptCallBack

        dbg("Callback ENABLED: {}{}".format(self._onAcceptCb.__name__, functSignature(self._onAcceptCb)))
        self._maxQueuedSockets = maxQueuedSockets

###############################################################################
    
    def open(self):
        if self._port == -1 or self._onAcceptCb is None:
            err("Server is NOT set")
            return False

        try:
            self._svr = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self._svr.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self._svr.bind((self._addr, self._port))
            self._svr.listen(self._maxQueuedSockets)
            #self._svr.setblocking(False)
            self._sckCheck = [self._svr]

        except socket.error as e:
            self._svr = None
            self._sckCheck = None
            err("CANNOT listen on -> {}:{} - {}".format(self._addr, self._port, e))
            return False

        msg("Listening ENABLED on -> {}:{}".format(self._addr, self._port))
        return True

    def close(self):
        self._svr.close()
        
        msg("Listening is DISABLED -> {}:{}".format(self._addr, self._port))
        
        self._svr = None
        self._sckCheck = None

    def isListening(self):
        return (self._sckCheck is not None)

###############################################################################

    def tick(self):
        if not self.isListening():
            return

        scks, _, _ = select.select(self._sckCheck, [], self._sckCheck, 0)

        # FORMAL REDUNDANCY
        if self._svr in scks:
            clnt, addr = self._svr.accept()
            self._onAcceptCb(clnt, addr)
        
###############################################################################
###############################################################################

def findFreePort(minPort=1024, maxPort=65535):
    for port in range(minPort, maxPort):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            try:
                s.bind(("127.0.0.1", port))
                dbg("Free TCP port FOUND: {}".format(port))
                return port

            except OSError:
                continue

    err("No free ports found between {} and {}".format(minPort, maxPort))
    return -1

###############################################################################
###############################################################################