
###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import sys
import os
import shutil
import pathlib
import hashlib
import json
import requests

from base64                 import b64encode
from PySketch.log           import msg, dbg, wrn, err, cri

###############################################################################
# Fs OPERATIONs

def listDirectories(path, onlyDirectories=True):
    if not os.path.exists(path):
        err("Directory NOT found: {}".format(path))
        return []

    if not isReadable(path):
        err("CANNOT list directories: {}".format(path))
        return False

    items = os.listdir(path)
    
    if onlyDirectories:
        return [dir for dir in items if os.path.isdir(os.path.join(path, dir))]
    
    return items

def createDirectory(path):
    if os.path.exists(path):
        wrn("Directory ALREADY exists: {}".format(path))
        return True
        
    '''
    if not isWritable(os.path.dirname(path)):
        err("CANNOT create directory: {}".format(path))
        return False
    '''

    try:
        os.makedirs(path)
        dbg("Directory successfully CREATED: {}".format(path))
        
    except OSError as e:
        err("CANNOT create directory: {} - {}".format(path, e))
        return False
        
    return True

def removeDirectory(path):
    if not isWritable(path):
        err("CANNOT remove directory: {}".format(path))
        return False

    try:
        shutil.rmtree(path)
        dbg("Directory successfully REMOVED: {}".format(path))

    except OSError as e:
        err("CANNOT remove directory: {} - {}".format(path, e))
        return False

    return True

def changeDir(path):
    if not isReadable(path) or not isExecutable(path):
        err("CANNOT enter directory: {}".format(path))
        return False

    try:
        os.chdir(path)

    except Exception as e:
        err("ChangeDirectory ERROR: {} - {}".format(path, e))
        return False

    dbg("Current directory successfully CHANGED: {}".format(path))
    return True

def copyFile(src, target):
    if not isReadable(src):
        err("Copying source is NOT readable: {} -> {}".format(src, target))
        return False

    try:
        shutil.copyfile(src, target)

    except Exception as e:
        err("File copy ERROR: {} -> {} - {}".format(src, target, e))
        return False

    dbg("File COPIED: {} -> {}".format(src, target))
    return True

def rename(src, target):
    if not isWritable(src):
        err("Renaming source is NOT readable: {} -> {}".format(src, target))
        return False

    try:
        os.rename(src, target)

    except Exception as e:
        err("Rename ERROR: {} -> {} - {}".format(src, target, e))
        return False

    dbg("File RENAMED: {} -> {}".format(src, target))
    return True

def removeFile(target):
    if not isWritable(target):
        err("Removing target is NOT readable: {}".format(target))
        return False

    try:
        os.remove(target)

    except Exception as e:
        err("Remove ERROR: {} - {}".format(target, e))
        return False

    return True
    
def symLink(src, destLink):
    if not isReadable(src):
        err("Linking source is NOT readable: {} -> {}".format(src, destLink))
        return False

    try:
        if os.path.exists(destLink):
            if not isWritable(destLink):
                err("Target is NOT writable: {} -> {}".format(src, destLink))
                return False
            os.remove(destLink)

        os.symlink(os.path.abspath(src), destLink)

    except Exception as e:
        err("Symlink ERROR: {} -> {} - {}".format(src, destLink, e))
        return False

    dbg("Symlink CREATED: {} -> {}".format(src, destLink))
    return True

def md5Digest(src, hashType="md5"):
    """
    Compute the hash of a file.

    Args:
        src (str | bytes | bytearray): Path (when iit is str) to the file to hash or the content data to hash.
        hashType (str): The type of hash to compute. Supported types include:
                         'md5', 'sha1', 'sha224', 'sha256', 'sha384', 'sha512', 'blake2b', 'blake2s'. 
                         Defaults to 'md5'.
    Returns:
        str: hash as a hexadecimal string, or None if an error occurred.
    """
    
    hasher = None

    try:
        hasher = getattr(hashlib, hashType.lower())()

    except Exception as e:
        err(e)
        return None

    hashStr = None

    if type(src) == str:
        if not isReadable(src):
            err("Digesting source is NOT readable: {}".format(src))
            return None
    
        try:
            with open(src, 'rb') as f:
                while True:
                    chunk = f.read(8192)

                    if len(chunk) == 0:
                        break
                        
                    hasher.update(chunk)

            hashStr = hasher.hexdigest()

        except Exception as e:
            err("Hashing ERROR [{}]: {} - {}".format(hashType.upper(), src, e))
            return None

        dbg("File HASHED [{}]: {} -> {}".format(hashType.upper(), src, hashStr))

    else:
        hasher.update(src)
        hashStr = hasher.hexdigest()
        dbg("Buffer HASHED [{}]: {} B -> {}".format(hashType.upper(), len(src), hashStr))

    return hashStr

def encode64(src, asString=False):
    if not pathExists(src):
        err("File NOT found: {}".format(src))
        return None

    data = readFile(src)

    if data is None:
        return None

    b64 = ""

    try:
        if asString:
            b64 = b64encode(data).decode('utf-8')
        else:
            b64 = b64encode(data)

    except Exception as e:
        err("B64 encoding ERROR: {} - {}".format(src, e))
        return None

    dbg("File ENCODED [B64]: {}".format(src))
    return b64

def pathExists(path):
    return os.path.exists(path)

def baseName(path):
    return os.path.basename(path)

def pathNameSuffix(path):
    return (pathlib.Path(path).suffix).lstrip(".").lower()

def pathNameSuffixes(path):
    suffixes = []

    for s in pathlib.Path(path).suffixes:
        suffixes.append(s.lstrip(".").lower())

    return suffixes

def absolutePath(path):
    return os.path.abspath(path)

def isDir(path):
    return os.path.isdir(path)

def isLink(path):
    return os.path.islink(path)

def linkTarget(link):
    if not isLink(link):
        return False

    return os.readlink(link)

def isFile(path):
    return os.path.isfile(path)

def isReadable(path):
    return os.access(path, os.R_OK)

def isWritable(path):
    return os.access(path, os.W_OK)

def isExecutable(path):
    return os.access(path, os.X_OK)

def size(path):
    if not pathExists(path) or not isFile(path):
        return 0
    
    return os.path.getsize(path)
        
###############################################################################
# FILE I/O

def readFile(filePath, mode="b"):
    if not os.path.exists(filePath):
        err("File NOT found: {}".format(filePath))
        return None

    if not isReadable(filePath):
        err("File is NOT readable: {}".format(filePath))
        return False

    data = None

    try:
        file = open(filePath, "r{}".format(mode))
        data = file.read()
        file.close()

    except Exception as e:
        err("Reading file ERROR: {} - {}".format(filePath, e))
        return None

    dbg("File READ: {}".format(filePath))
    return data

def writeFile(filePath, data, mode="b"):
    '''
    if not isWritable(os.path.dirname(filePath)):
        err("Parent directory is NOT writable: {}".format(os.path.dirname(filePath)))
        return False
    '''

    if os.path.exists(filePath) and not isWritable(filePath):
        err("File ALREADY exists and is NOT writable: {}".format(filePath))
        return False

    try:
        file = open(filePath, "w{}".format(mode))
        file.write(data)
        file.close()

    except Exception as e:
        err("Writing file ERROR: {} - {}".format(filePath, e))
        return False

    dbg("File WROTE: {}".format(filePath))
    return True

def writeJson(filePath, data):
    return writeFile(filePath, json.dumps(data), mode="")
    
def readJson(filePath):
    txt = readFile(filePath, mode="")

    if txt is None:
        return None
    
    try:
        obj = json.loads(txt)

    except Exception as e:
        err("JSON ERRORR: {} - {}".format(filePath, e))
        return None

    return obj
    
###############################################################################
# INTERNET I/O

def download(url, filePath):
    dbg("Downloading file from HTTP server [URL -> {}] to '{}'".format(url, filePath))

    try:
        response = requests.get(url, stream=True)

        if response.status_code == 200:
            with open(filePath, "wb") as file:
                for byte in response.iter_content(1024):
                    file.write(byte)
        else:
            err("HTTP response is NOT valid: {}".format(response.status_code))
            return False

    except requests.exceptions.RequestException as e:
       err("DOWNLOAD REQUEST ERROR -> {}".format(e))
       return False

    except Exception as e:
       err("GENERIC DOWNLOAD ERROR -> {}".format(e))
       return False

    return True

###############################################################################