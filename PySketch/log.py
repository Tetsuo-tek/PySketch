###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import sys
import time
import queue

sysLogTypes = None

if sys.platform == "linux":
    import syslog

    sysLogTypes = {
        "D" : syslog.LOG_DEBUG,
        "M" : syslog.LOG_INFO,
        "W" : syslog.LOG_WARNING,
        "E" : syslog.LOG_ERR,
        "F" : syslog.LOG_CRIT
    }

###############################################################################

escapes = {
    "RED" : "\033[91m",  # Rosso chiaro
    "GRN" : "\033[92m",  # Verde chiaro
    "YLW" : "\033[93m",  # Giallo chiaro
    "BLU" : "\033[94m",  # Blu chiaro
    "MGN" : "\033[95m",  # Magenta chiaro
    "CYN" : "\033[96m",  # Ciano chiaro
    "WHT" : "\033[97m",  # Bianco
    "GRY" : "\033[90m",  # Grigio
    "DRED": "\033[31m",  # Rosso scuro
    "DGRN": "\033[32m",  # Verde scuro
    "DYLW": "\033[33m",  # Giallo scuro
    "DBLU": "\033[34m",  # Blu scuro
    "DMGN": "\033[35m",  # Magenta scuro
    "DCYN": "\033[36m",  # Ciano scuro
    "DWHT": "\033[37m",  # Bianco scuro (Grigio chiaro)
    "RESET": "\033[0m",  # Reset a colori di default
    "BOLD" : "\033[1m",  # Grassetto
    "UNDL" : "\033[4m",  # Sottolineato
    "BLINK": "\033[5m",  # Lampeggiante
    "INVERT": "\033[7m"  # Invertito (sfondo e testo)
}

###############################################################################

logger = None

class LogMachine:
    def __init__(self):
        global logger

        if logger is not None:
            logger.err("ONLY one instance of LogMachine is ALLOWED!")
            exit(1)
        
        logger = self

        self._nodeName = ""
        self._consLogEnabled = True
        self._consColoursEnabled = True
        self._sysLogEnabled = False
        self._lastRawSz = 0
        self._logsQueue = None

###############################################################################

    def setNodeName(self, name):
        self._nodeName = name
        dbg("Logger NodeName: {}".format(self._nodeName))

###############################################################################

    def enableConsoleLog(self, enabled):
        self._consLogEnabled = enabled

    def enableSystemLog(self, enabled):
        if sys.platform != "linux":
            err("CANNOT set syslog on system that is not Linux")
            return

        if not enabled and self._sysLogEnabled:
            syslog.closelog()

        elif enabled and not self._sysLogEnabled:
            syslog.openlog(ident="{}".format(self._nodeName), logoption=syslog.LOG_PID, facility=syslog.LOG_USER)
        
        else:
            return

        self._sysLogEnabled = enabled

    def enableLogsQueue(self, enabled):
        if enabled:
            self._logsQueue = queue.Queue()
        else:
            self._logsQueue = None

    def enableConsoleColors(self, enabled):
        self._consColoursEnabled = enabled

    # ALSO ON SYSLOG

    def _p(self, t, color, txt):
        if self._sysLogEnabled and sys.platform == "linux":
            self._syslog(t, txt)

        if self._logsQueue is not None:
            self._logsQueue.put(
                {
                    "ts" : time.time_ns() // 1000,
                    "log_T" : t,
                    "msg" : txt
                }
            )

        if not self._consLogEnabled:
            return
            
        if self._lastRawSz > 0:
            emptyStr = " " * self._lastRawSz
            print("{}\r".format(emptyStr), end="")
            self._lastRawSz = 0

        if self._consColoursEnabled:
            print("{} => {}{}{}".format(t, color, txt, escapes["RESET"]))

        else:
            print("{} => {}".format(t, txt))

    def dbg(self, txt):
        if self._consColoursEnabled:
            self._p("D", escapes["BLU"], txt)

        else:
            self._p("D", "", txt)

    def msg(self, txt):
        if self._consColoursEnabled:
            self._p("M", escapes["GRN"], txt)
        else:

            self._p("M", "", txt)

    def wrn(self, txt):
        if self._consColoursEnabled:
            self._p("W", escapes["YLW"], txt)
            
        else:
            self._p("W", "", txt)

    def err(self, txt):
        if self._consColoursEnabled:
            self._p("E", escapes["RED"], txt)

        else:
            self._p("E", "", txt)

    def cri(self, txt):
        if self._consColoursEnabled:
            self._p("F", escapes["DRED"], txt)

        else:
            self._p("F", "", txt)

    def getLogsQueue(self):
        return self._logsQueue

    def logsQueueIsEmpty(self):
        if self._logsQueue is None:
            return True

        return self._logsQueue.empty()

    # ONLY ON CONSOLE
    
    def raw(self, txt, flushing=False):
        if not self._consLogEnabled:
            return

        self._lastRawSz = len(txt)
        print(txt, end="", flush=flushing)

    def _syslog(self, t, txt):
        if sys.platform != "linux":
            err("CANNOT set syslog on system that is not Linux")
            return

        if self._sysLogEnabled:
            syslog.syslog(sysLogTypes[t], txt)

###############################################################################

def dbg(txt):
    if logger is None:
        return

    logger.dbg(txt)

def msg(txt):
    if logger is None:
        print("Logger is NONE", logger, txt)
        return

    logger.msg(txt)

def wrn(txt):
    if logger is None:
        return

    logger.wrn(txt)

def err(txt):
    if logger is None:
        return

    logger.err(txt)

def cri(txt):
    if logger is None:
        return

    logger.cri(txt)

def raw(txt, flushing=False):
    if logger is None:
        return

    logger.raw(txt, flushing)

###############################################################################

def printPair(k, v):
    if logger is None:
        return

    if logger._consColoursEnabled:
        logger.raw("{}{} {}: {}{}{}\n".format(escapes["YLW"], k, escapes["WHT"], escapes["CYN"], v, escapes["RESET"]))
    
    else:
        logger.raw("{}: {}\n".format(k, v))

###############################################################################

def getGlobalLoggerInstance():
    return logger

'''
import syslog

###############################################################################

escapes = {
    "RED" : "\033[91m",  # Rosso chiaro
    "GRN" : "\033[92m",  # Verde chiaro
    "YLW" : "\033[93m",  # Giallo chiaro
    "BLU" : "\033[94m",  # Blu chiaro
    "MGN" : "\033[95m",  # Magenta chiaro
    "CYN" : "\033[96m",  # Ciano chiaro
    "WHT" : "\033[97m",  # Bianco
    "GRY" : "\033[90m",  # Grigio
    "DRED": "\033[31m",  # Rosso scuro
    "DGRN": "\033[32m",  # Verde scuro
    "DYLW": "\033[33m",  # Giallo scuro
    "DBLU": "\033[34m",  # Blu scuro
    "DMGN": "\033[35m",  # Magenta scuro
    "DCYN": "\033[36m",  # Ciano scuro
    "DWHT": "\033[37m",  # Bianco scuro (Grigio chiaro)
    "RESET": "\033[0m",  # Reset a colori di default
    "BOLD" : "\033[1m",  # Grassetto
    "UNDL" : "\033[4m",  # Sottolineato
    "BLINK": "\033[5m",  # Lampeggiante
    "INVERT": "\033[7m"  # Invertito (sfondo e testo)
}

sysLogTypes = {
    "D" : syslog.LOG_DEBUG,
    "M" : syslog.LOG_INFO,
    "W" : syslog.LOG_WARNING,
    "E" : syslog.LOG_ERR,
    "F" : syslog.LOG_CRIT
}

###############################################################################

logger = None

class LogMachine:
    def __init__(self):
        global logger

        if logger is not None:
            logger.err("ONLY one instance of LogMachine is ALLOWED!")
            exit(1)
        
        logger = self

        self._nodeName = ""
        self._consLogEnabled = True
        self._consColoursEnabled = True
        self._sysLogEnabled = False

###############################################################################

    def setNodeName(self, name):
        self._nodeName = name
        dbg("Logger NodeName: {}".format(self._nodeName))

###############################################################################

    def enableConsoleLog(self, enabled, withColours):
        self._consLogEnabled = enabled
        self._consColoursEnabled = withColours

    def enableSystemLog(self, enabled):
        if not enabled and self._sysLogEnabled:
            syslog.closelog()

        elif enabled and not self._sysLogEnabled:
            syslog.openlog(ident="{}".format(self._nodeName), logoption=syslog.LOG_PID, facility=syslog.LOG_USER)
        
        else:
            return

        self._sysLogEnabled = enabled

    def enableConsoleColors(self, enabled):
        self._consLogEnabled = enabled

    # ALSO ON SYSLOG
    def dbg(self, txt):
        self._print("D", escapes["BLU"], txt)
        self._syslog("D", txt)

    def msg(self, txt):
        self._print("M", escapes["GRN"], txt)
        self._syslog("M", txt)

    def wrn(self, txt):
        self._print("W", escapes["YLW"], txt)
        self._syslog("W", txt)

    def err(self, txt):
        self._print("E", escapes["RED"], txt)
        self._syslog("E", txt)

    def cri(self, txt):
        self._print("F", escapes["DRED"], txt)
        self._syslog("F", txt)

    # ONLY ON CONSOLE
    def raw(self, txt, flushing=False):
        if self._consLogEnabled:
            print(txt, end="", flush=flushing)

    def _print(self, t, col, txt):
        if self._consLogEnabled:
            if self._consColoursEnabled:
                print("{} => {}{}{}".format(t, col, txt, escapes["RESET"]))

            else:
                print("{} => {}".format(t, txt))

    def _syslog(self, t, txt):
        if self._sysLogEnabled:
            syslog.syslog(sysLogTypes[t], txt)

###############################################################################

def dbg(txt):
    if logger is None:
        return

    logger.dbg(txt)

def msg(txt):
    if logger is None:
        return

    logger.msg(txt)

def wrn(txt):
    if logger is None:
        return

    logger.wrn(txt)

def err(txt):
    if logger is None:
        return

    logger.err(txt)

def cri(txt):
    if logger is None:
        return

    logger.cri(txt)

def raw(txt, flushing=False):
    if logger is None:
        return

    logger.raw(txt, flushing)

###############################################################################

def printPair(k, v):
    if logger is None:
        return

    if logger._consColoursEnabled:
        logger.raw("{}{} {}: {}{}{}\n".format(escapes["YLW"], k, escapes["WHT"], escapes["CYN"], v, escapes["RESET"]))
    
    else:
        logger.raw("{}: {}\n".format(k, v))

###############################################################################
'''