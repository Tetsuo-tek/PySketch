###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import sys
import os

import platform
import distro
import multiprocessing
import psutil

from cpuinfo                import get_cpu_info

from PySketch.log           import msg, dbg, wrn, err, cri, escapes, raw, printPair

class HostMachine():
    def __init__(self):
        dbg("Grabbing HOST-MACHINE informations ..")
        self._host = {}

        cpuName = "None"
        info = get_cpu_info()

        #if sys.version_info >= (3, 0) and sys.version_info < (3, 11):
        if "brand_raw" in  info:
            cpuName = get_cpu_info()["brand_raw"]
        elif "brand" in info:
            cpuName = get_cpu_info()["brand"]

        self._host["cpu-name"] = cpuName
        self._host["cpu-arch"] = platform.machine()
        self._host["cpu-cores"] = multiprocessing.cpu_count()

        self._host["cpu-freq-min"] = -1
        self._host["cpu-freq-max"] = -1

        if platform.system() == "Linux":
            self._cpuFreq = psutil.cpu_freq(percpu=False)

            if self._cpuFreq is not None:
                self._host["cpu-freq-min"] = self._cpuFreq.min
                self._host["cpu-freq-max"] = self._cpuFreq.max
            
        self._host["os"] = platform.system()
        self._host["k-rel"] = platform.release()
        self._host["k-vers"] = platform.version()
        self._host["exe-format"] = platform.architecture()

        if platform.system() == "Linux":
            self._host["distro-name"] = distro.name()
            self._host["distro-version"] = distro.version()
            self._host["distro-codename"] = distro.codename()
            
        self._host["host-name"] = platform.node()
        self._host["py-vers"] = platform.python_version()
        self._host["user"] = os.getenv('USER')
        self._host["current-dir"] = os.getcwd()

    def printInfos(self):
        printPair("CPU name", self._host["cpu-name"])

        printPair("CPU architecture", self._host["cpu-arch"])
        printPair("CPU cores", self._host["cpu-cores"])

        printPair("CPU freq-min MHz", self._host["cpu-freq-min"])
        printPair("CPU freq-max MHz", self._host["cpu-freq-max"])

        printPair("Operating System", self._host["os"])
        printPair("Kernel Release", self._host["k-rel"])
        printPair("Kernel Version", self._host["k-vers"])
        printPair("EXE-Format", self._host["exe-format"])

        if platform.system() == "Linux":
            printPair("Distro name", self._host["distro-name"])
            printPair("Distro version", self._host["distro-version"])
            printPair("Distro codename", self._host["distro-codename"])

        printPair("Host Name", self._host["host-name"])
        printPair("Python Version", self._host["py-vers"])
        printPair("Current system-user", self._host["user"])
        printPair("Current directory", self._host["current-dir"])
        raw(escapes["RESET"])

    def infos(self):
        return self._host
        
###############################################################################