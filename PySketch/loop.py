###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

from PySketch.log           import msg, dbg, wrn, err, cri, printPair
from PySketch.elapsedtime   import ElapsedTime
from PySketch.timestepper   import TimeStepper
from PySketch.signal        import Signal

import time

from queue                  import Queue
from enum                   import Enum

###############################################################################

class LoopTimerMode(Enum):
    TIMEDLOOP_RT = 0
    TIMEDLOOP_SLEEPING = 1
    EVENTLOOP_WAITING = 2
    FREELOOP = 3

###############################################################################

class EventLoop():
    def __init__(self, name):
        self._enabled = False

        self._name                  = name

        self._fastInterval          = 0.010
        self._slowInterval          = 0.200
        self._checkTickTime         = 0.080
        
        self._mode                  = LoopTimerMode.TIMEDLOOP_RT

        self._slowChrono            = ElapsedTime()
        self._oneSecChrono          = ElapsedTime()

        self.started                = Signal(self)
        self.fastZone               = Signal(self)
        self.slowZone               = Signal(self)
        self.oneSecZone             = Signal(self)
        self.changed                = Signal(self)
        self.quitting               = Signal(self)
        self.terminated             = Signal(self)

        self._invokingSlots         = Queue()

        self._timer                 = TimeStepper()

    def setLoopName(self, name):
        self._name = name

    def _loopLog(self, logFunct, txt):
        logFunct("Loop {} - {}".format(self._name, txt))

    def setFastInterval(self, secs, checkingTimePeriod):
        self._fastInterval = secs
        self._checkTickTime = checkingTimePeriod

        self._timer.setInterval(self._fastInterval, self._checkTickTime)

        self._loopLog(dbg, "Setup Fast interval [checked each {:.4f} s]: {:.4f} s"
            .format(self._checkTickTime, self._fastInterval))

        self.changed.trigger()

    def setSlowInterval(self, secs):
        self._slowInterval = secs
        self._loopLog(dbg, "Setup Slow interval: {:.4f} s".format(self._slowInterval))

        self.changed.trigger()

    def setTickMode(self, mode):
        self._mode = mode
        self._loopLog(dbg, "Setup tick mode: {}".format(self._mode))

        self.changed.trigger()

    def init(self, withLoop=True):
        if self.isRunning():
            self._loopLog(err, "Loop is ALREADY enabled")
            return
            
        self._enabled = True

        self._loopLog(dbg, "Started [fast: {:.4f} s; slow: {:.4f} s; mode: {}]"
            .format(self._fastInterval, self._slowInterval, self._mode))

        if withLoop:
            while self._enabled:
                self.tick()

        self.started.trigger()

    def invokeSlot(self, slot, args=None):
        self._invokingSlots.put(
            {
                "args": args,
                "slot": slot
            }
        )

    def _invokeSlots(self):
        while not self._invokingSlots.empty():
            slotPack = self._invokingSlots.get()
            args = slotPack["args"]

            if args is None:
                slotPack["slot"]()

            elif type(args) == list or type(args) == tuple:
                slotPack["slot"](*args)

            elif type(args) == dict:
                slotPack["slot"](**args)

    def tick(self):
        if not self.isRunning():
            return
        
        self._invokeSlots()
        self.fastZone.trigger()

        if self._slowChrono.stop() >= self._slowInterval:
            self.slowZone.trigger()
            self._slowChrono.start()

        if self._oneSecChrono.stop() >= 1.0:
            self.oneSecZone.trigger()
            self._oneSecChrono.start()

        if self._mode == LoopTimerMode.TIMEDLOOP_RT:
            self._timer.wait()

        elif self._mode == LoopTimerMode.TIMEDLOOP_SLEEPING:
            time.sleep(self._fastInterval)

        elif self._mode == LoopTimerMode.EVENTLOOP_WAITING:
            pass # NOT implemented yet; actually, it is as FREELOOP mode, doing anything

    def quit(self, val=0):
        if not self.isRunning():
            self._loopLog(err, "Loop is ALREADY disabled")
            return

        self._enabled = False

        self._loopLog(dbg, "Closing ...")

        self.quitting.trigger()
        self._invokeSlots()
        self.terminated.trigger()#MUST BE CONNECTED AS DIRECT

        self._loopLog(wrn, "Terminated [EXIT value: {}]".format(val))

    def isRunning(self):
        return self._enabled
    