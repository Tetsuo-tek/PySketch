###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################
# CONTAINER SCHEMA

'''
BEGIN

    1 (UINT8)       - HasIndex: 0 (False) / 1 (True)
    
        # CHANNELS DECLARATION SEQUENCE
        1.
            1 (UINT8)   - ChanID (> 0)
            1 (UINT8)   - ChannelType(Enum)
            4 (UINT32)  - Channels properties lenght
            n (data)    - Channels properties bytearray
        2.
            ...
        3.
            ...

>>>

    # DATA SEGMENTS
    1)
        1 (UINT8)       - IsLast: 0 (False)
        8 (UINT64)      - Segment timestamp

            # CHANNELS DATA SEQUENCE
            1 (UINT8)   - ChanID (> 0)
            4 (UINT32)  - Channels data lenght
            n (data)    - Channels data bytearray

    2)
        ...
    3)
        ...

<<<
    # SEGMENTS INDEX
    1)
        8 (UINT64)      - Segment timestamp
        8 (UINT64)      - Segment address

    2)
        ...
    3)
        ...

    8 (UINT64)          - Index address

END
'''

###############################################################################


import os
import struct

from enum                   import Enum

from PySketch.log           import escapes, msg, dbg, wrn, err, cri, raw, printPair
from PySketch.fs            import *

###############################################################################

class ChannelType(Enum):
    NONE = 0
    Video = 1
    Audio = 2
    Data = 3

###############################################################################

class Recorder():
    def __init__(self):
        self._filePath = None
        self._file = None
        self._channels = {}
        self._index = []

    def addChannel(self, t, props):
        if self.isOpen():
            err("CANNOT add channel when the file is open: {}".format(self._filePath))
            return False

        chanID = len(self._channels)+1
        self._channels[chanID] = {
                "id" : chanID,
                "type" : t,
                "props" : props #MUST be a bytearray
            }

        msg("Data-channel ADDED: {} - {} - {}".format(chanID, t, props))

    def open(self, fileName):
        if self.isOpen():
            err("File is ALREADY open: {}".format(self._filePath))
            return False

        filePath = os.path.abspath(fileName)

        if not isWritable(os.path.dirname(filePath)):
            err("Parent directory is NOT writable: {}".format(filePath))
            return False

        if os.path.exists(filePath) and not isWritable(filePath):
            err("File ALREADY exists and is NOT writable: {}".format(filePath))
            return False

        chCount = len(self._channels)
        
        if chCount == 0:
            err("One channel is REQUIRED at least")
            return False

        try:
            self._file = open(filePath, "wb")
            self._file.write(struct.pack("<B", 0))

        except Exception as e:
            err("Opening file for write ERROR: {} - {}".format(filePath, e))
            self._file = None
            return False

        for k, v in self._channels.items():
            try:
                self._file.write(struct.pack("<B", k))
                self._file.write(struct.pack("<B", v["type"].value))

                props = v["props"]
                
                self._file.write(struct.pack("<I", len(props)))
                self._file.write(props)

            except Exception as e:
                err("Writing file-header ERROR: {} - {}".format(filePath, e))
                return False

        self._filePath = filePath
        msg("Multimedia file open: {}".format(self._filePath))
        return True

    def step(self, ts):
        if not self.isOpen():
            err("File is NOT open yet")
            return False

        try:
            self._file.write(struct.pack("<B", 0))
            self._index.append((ts, self._file.tell()))
            self._file.write(struct.pack("<Q", ts))

        except Exception as e:
            err("Stepping file ERROR: {} - {}".format(self._filePath, e))
            return False

        return True

    def write(self, chanID, data):
        if not self.isOpen():
            err("File is NOT open yet")
            return False

        try:
            self._file.write(struct.pack("<B", chanID))
            self._file.write(struct.pack("<I", len(data)))
            self._file.write(data)

        except Exception as e:
            err("Writing file ERROR: {} - {}".format(self._filePath, e))
            return False

        return True

    def close(self):
        if not self.isOpen():
            err("File is NOT open yet")
            return False

        try:
            self._file.write(struct.pack("<B", 0))

            indexPos = self._file.tell()

            for pos in self._index:
                #print("###", pos[0], pos[1])
                self._file.write(struct.pack("<Q", pos[0]))
                self._file.write(struct.pack("<Q", pos[1]))

            self._file.write(struct.pack("<Q", indexPos))
            self._file.seek(0)
            self._file.write(struct.pack("<B", 1))
            self._file.close()

        except Exception as e:
            err("Closing file ERROR: {} - {}".format(self._filePath, e))
            return False

        msg("Multimedia file CLOSED: {}".format(self._filePath))
        return True

    def isOpen(self):
        return (self._file is not None)

###############################################################################

class Player():
    def __init__(self):
        self._filePath = None
        self._file = None
        self._channels = {}
        self._hasIndex = False
        self._index = {}
        self._indexPos = -1
        self._firstSegment = None
        self._lastSegment = None

    def open(self, fileName):
        if self.isOpen():
            err("File is ALREADY open: {}".format(self._filePath))
            return False

        filePath = os.path.abspath(fileName)

        if os.path.exists(filePath) and not isReadable(filePath):
            err("File ALREADY exists and is NOT readable: {}".format(filePath))
            return False

        try:
            self._file = open(filePath, "rb")
            self._hasIndex = (self._readBinary(1, "<B") == 1)

        except Exception as e:
            err("Opening file for read ERROR: {} - {}".format(filePath, e))
            self._file = None
            return False

        # COULD BE IMPROVED WITH DYNAMIC INDEXING ON START, OR MAKING INDEX OFFLINE UPDATING THE FILE
        if not self._hasIndex:
            err("Index NOT found [file corrupted]: {}".format(filePath))
            return False

        self._loadChannelsDecl()
        self._dataPos = self._file.tell()

        self._loadIndex()
        self._file.seek(self._dataPos)

        self._filePath = filePath
        return True

    def _loadChannelsDecl(self):
        self._channels = {}

        while True:
            chanID = self._readBinary(1, "<B")
            
            if chanID == 0:
                self._file.seek(-1, 1)
                break

            t =  self._readBinary(1, "<B")
            propsLen =  self._readBinary(4, "<I")
            props = self._file.read(propsLen)

            self._channels[chanID] = {
                "id" : chanID,
                "type" : t,
                "props" : props # MUST be a bytearray
            }

            msg("Data-channel CHECKED: {} - {} - {}".format(chanID, t, props))

    def _loadIndex(self):
        self._file.seek(-8, 2)
        end = self._file.tell()
        self._indexPos =  self._readBinary(8, "<Q")
        self._file.seek(self._indexPos)

        while True:
            if end == self._file.tell():
                break

            ts = self._readBinary(8, "<Q")
            pos = self._readBinary(8, "<Q")

            self._index[ts] = pos
            #print("!!!", ts, pos)

        l = list(self._index.keys())
        self._firstSegment = l[0]
        self._lastSegment = l[-1]

    def step(self, packs):
        self._readBinary(1, "<B")
        ts =  self._readBinary(8, "<Q")

        while True:
            chanID =  self._readBinary(1, "<B")
            
            if chanID == 0:
                self._file.seek(-1, 1)
                break

            dataLen =  self._readBinary(4, "<I")
            data = self._file.read(dataLen)

            packs.put(
                {
                    "chanID" : chanID,
                    "data" : data
                }
            )
            
        return ts

    def _readBinary(self, count, mode):
        b = self._file.read(count)

        if b is None or len(b) != count:
            return None

        val, = struct.unpack(mode, b)
        return val

    def close(self):
        if not self.isOpen():
            err("File is NOT open yet")
            return False

        try:
            self._file.close()

        except Exception as e:
            err("Closing file ERROR: {} - {}".format(self._filePath, e))
            return False

        return True

    def isOpen(self):
        return (self._file is not None)

###############################################################################