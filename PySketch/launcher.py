#!/usr/bin/env python

###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################


##### DEPRECATED #####


import os
import sys
import signal
import shutil
import json
import tempfile
import subprocess
import argparse

args = None

currDir = os.getcwd()
wrk = tempfile.gettempdir()

###############################################################################

def clean():
    if os.path.exists(wrk):
        shutil.rmtree(wrk)
        print("Working directory REMOVED")
        
    print("Sketch TERMINATED")

def handleInterruption(signal, frame):
    print("Sketch INTERRUPTED")
    sys.exit(0)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="PySketch launcher")
    parser.add_argument("--engine", help="The PySketch engine directory")
    parser.add_argument("--sketch", help="The pysketch-file path to launch")
    parser.add_argument("--cli", help="The pysketch command line")
    parser.add_argument("--wrk-dir", help="The pysketch working directory (default: {})".format(wrk), default=wrk)

    args = parser.parse_args()

    if args.engine is None or args.sketch is None or args.cli is None:
        print("Required arguments NOT found: '--engine', '--sketch' and -'-cli'")
        exit(1)

    signal.signal(signal.SIGINT, handleInterruption)

    try:
        if not os.path.exists(args.engine):
            print("Engine directory NOT found: {}".format(args.sketch))
            exit(1)
            
        if not os.path.exists(args.sketch):
            print("Sketch file NOT found: {}".format(args.sketch))
            exit(1)

        engine = os.path.abspath(args.engine)
        engineName = os.path.basename(engine)
        print("Engine selected: {} ({})".format(engineName, engine))

        sketch = os.path.abspath(args.sketch)
        sketchName = os.path.basename(sketch)
        print("Sketch selected: {} ({})".format(sketchName, sketch))

        wrk = os.path.join(os.path.abspath(args.wrk_dir), sketchName)
        print("Working directory selected: {}".format(wrk))

        if os.path.exists(wrk):
            print("Directory ALREADY exists")

        else:
            os.makedirs(wrk)
            print("Directory successfully CREATED: {}".format(wrk))

        os.chdir(wrk)
        print("CHANGED current path to working directory: {}".format(wrk))

        engineDestLink = os.path.join(wrk, "PySketch")

        if os.path.exists(engineDestLink):
            os.remove(engineDestLink)

        os.symlink(engine, engineDestLink)
        print("Engine LINKED [{}]: {} -> {}".format(engineName, engine, engineDestLink))

        sketchDestLink = os.path.join(wrk, sketchName)

        if os.path.exists(sketchDestLink):
            os.remove(sketchDestLink)

        os.symlink(sketch, sketchDestLink)
        print("Sketch LINKED [{}]: {} -> {}".format(sketchName, sketch, sketchDestLink))

        args = json.loads(args.cli)
        cli = ["PySketch/executor.py", "./{}".format(sketchName)] 

        if len(args) > 0:
            cli.extend(args)

        print("Sketch start:", cli)
        subprocess.call(cli)

    except Exception as e:
        print(e)

    finally:
        clean()

