from PySketch.signal            import Signal

###############################################################################
# Qt

from PySide6.QtWidgets          import QTextBrowser
from PySide6.QtGui              import QTextCursor

###############################################################################

class HtmlView(QTextBrowser):
    def __init__(self, loop, parent=None):
        super().__init__(parent)

        self._l = loop

        self._autoscroll = True

        self.setOpenExternalLinks(False)
        self.setOpenLinks(False)
        self.anchorClicked.connect(self._onChatClickedLink)
        self.setStyleSheet("background-color: rgb(10, 10, 10); color: rgb(255, 255, 255);")

        self.document().setDefaultStyleSheet(
            """
            h1, h2, h3, h4, h5, h6 {
                color: Yellow;
            }

            p {
                color: white;
            }

            pre {
                color: grey;
            }

            a {
                /*text-decoration: none;*/
                color: orange;
                font-weight: bold;
            }

            a {
                /*text-decoration: none;*/
                color: orange;
                font-weight: bold;
            }

            a:hover {
                color: darkorange;
            }
            """
        )

        self._cursor = self.textCursor()

        self.urlRequested   = Signal(self._l)

###############################################################################

    def setAutoscroll(self, enable):
        self._autoscroll = bool(enable)

        if self._autoscroll:
            self.verticalScrollBar().setValue(self.verticalScrollBar().maximum())

###############################################################################

    def append(self, html):
        self._cursor.movePosition(QTextCursor.End)
        self._cursor.insertHtml(html)
        self._currentPosition = self._cursor.position()
        self._scrollDown()

    def replaceTail(self, htmlChunck):
        self._cursor.setPosition(self._currentPosition)
        self._cursor.insertHtml(htmlChunck)
        self._cursor.setPosition(self._cursor.position())
        self._cursor.movePosition(QTextCursor.End, QTextCursor.KeepAnchor)
        self._cursor.removeSelectedText()
        self._scrollDown()

###############################################################################

    def _scrollDown(self):
        if self._autoscroll:
            self.verticalScrollBar().setValue(self.verticalScrollBar().maximum())

###############################################################################

    def _onChatClickedLink(self, url):
        self.urlRequested.trigger([url])

###############################################################################