from PySide6.QtWidgets  import (
    QTabWidget,
    QWidget,
    QLabel,
    QPushButton,
    QHBoxLayout
)

from PySide6.QtCore     import Qt

###############################################################################

class ClosableTab(QWidget):
    def __init__(self, text, on_close_callback, parent=None):
        super().__init__(parent)

        self._layout = QHBoxLayout()
        self._layout.setContentsMargins(0, 0, 0, 0)
        self._layout.setSpacing(5)
        
        self.label = QLabel(text)
        self.close_button = QPushButton("✖")
        self.close_button.setFixedSize(16, 16)
        self.close_button.setStyleSheet("border: none;")
        self.close_button.clicked.connect(on_close_callback)
        
        self._layout.addWidget(self.label)
        self._layout.addWidget(self.close_button)
        self._layout.setAlignment(Qt.AlignLeft)
        self.setLayout(self._layout)

###############################################################################

class TabWidget(QTabWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setTabsClosable(False)  # Keep this off, we handle close buttons manually

    def addClosableTab(self, widget, title):
        index = self.addTab(widget, "")  # Add a tab without a default label
        self.setTabEnabled(index, True)  # Ensure the tab is enabled
        
        def closeCallback():
            self.removeTab(index)
            widget.deleteLater()

        customTab = ClosableTab(title, closeCallback)
        self.tabBar().setTabButton(index, QTabWidget.LeftSide, customTab)

###############################################################################