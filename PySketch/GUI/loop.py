from PySide6.QtCore         import Qt, QTimer

from PySketch.loop          import EventLoop, LoopTimerMode

class QtEventLoop():
    def __init__(self, appName):
        self._l = EventLoop(appName)
        self._l.setFastInterval(-1, -1)

        self._timer = QTimer()
        self._timer.setTimerType(Qt.PreciseTimer)
        self._timer.timeout.connect(self._tick)

    def start(self, fastTimeInterval, slowTimeInterval):
        self._l.setSlowInterval(slowTimeInterval)
        self._l.setTickMode(LoopTimerMode.FREELOOP)
        self._l.init(False)

        self._timer.start(fastTimeInterval*1000)

    def stop(self):
        self._timer.stop()

    def _tick(self):
        self._l.tick()

    def ticker(self):
        return self._l


