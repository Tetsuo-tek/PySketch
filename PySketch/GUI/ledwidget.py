from PySide6.QtWidgets      import QLabel, QFrame
from PySide6.QtCore         import Qt, QTimer

class LedWidget(QLabel):
    def __init__(self, parent=None):
        super().__init__(parent)

        self._blinking = False
        self._state = False

        self._backgroundStyle_ON = ""
        self._backgroundStyle_OFF = ""

        self._msecsToOFF = 150

        self._timer = QTimer(self)
        self._timer.setTimerType(Qt.CoarseTimer)
        self._timer.timeout.connect(self._timeout)

        self.setFrameShape(QFrame.Panel)
        self.setFrameShadow(QFrame.Sunken)

    def setup(self, onColor, offColor, w=10, h=5):
        self.setMinimumSize(w, h)
        self.setMaximumSize(w, h)
        self._backgroundStyle_ON = "background-color: rgb({}, {}, {});".format(onColor.red(), onColor.green(), onColor.blue())
        self._backgroundStyle_OFF = "background-color: rgb({}, {}, {});".format(offColor.red(), offColor.green(), offColor.blue())

        self.setStyleSheet(self._backgroundStyle_OFF)

    def pulse(self, persistenceMsecs=100):
        if self._timer.isActive():
            self._timer.stop()

        self.on()
        self.setStyleSheet(self._backgroundStyle_ON)

        #self._timer.setInterval(persistenceMsecs)
        #self._timer.setSingleShot(True)
        #self._timer.start()

        QTimer.singleShot(persistenceMsecs, self._timeout)

    def on(self):
        self._state = True
        self.setStyleSheet(self._backgroundStyle_ON)

    def _off(self):
        self._state = False
        self.setStyleSheet(self._backgroundStyle_OFF)

    def off(self):
        if self._blinking:
            self._blinking = False
            self._timer.stop()

        self._off()

    def toggle(self):
        if self._state:    
            self._off()

        else:
            self.on()

    def blink(self, msecs=1000):
        self._blinking = True
        self.on()

        self._timer.setSingleShot(False)
        self._timer.setInterval(msecs)
        self._timer.start()

    def _timeout(self):
        if self._blinking:
            self.toggle()

        else:
            self.off()

    def state(self):
        return self._state

    def isBlinking(self):
        return self._blinking