import cv2                  as cv
import numpy                as np

import pyqtgraph            as pg

pg.setConfigOptions(imageAxisOrder="row-major")
pg.setConfigOptions(antialias=True)

from PySide6.QtWidgets      import QFrame
from PySide6.QtCore         import QObject

from PySketch.log           import dbg, err

class VideoWidget(QObject):
    def __init__(self, layout, parent=None):
        super().__init__(parent)
        self._props = None
        self._isTheFirstFrame = False
        self._isRawPublish = False
        self._w = -1
        self._h = -1
        self._pxFmt = None
        self._f = None
        self._layout = layout
        self.active = False

    def open(self, props):
        self._props = props
        self._isTheFirstFrame = True

        dbg("VideoWidget OPEN: {}".format(self._props))
        self._w = self._props["w"]
        self._h = self._props["h"]
        self._depth = self._props["depth"]

        self._isRawPublish = (self._props["codec"] == "RAW")
        widget = pg.GraphicsLayoutWidget()

        widget.setFrameShape(QFrame.Panel)
        widget.setFrameShadow(QFrame.Sunken)

        widget.setStyleSheet("background-color: rgb(0, 0, 0);")

        widget.ci.layout.setSpacing(0)
        widget.ci.layout.setContentsMargins(0, 0, 0, 0)
        widget.setBackground(background=None)

        frame_vb = pg.ViewBox()
        self.image_ii = pg.ImageItem()
        frame_vb.addItem(self.image_ii)
        frame_vb.setAspectLocked(True)
        frame_vb.invertY(True)

        widget.addItem(frame_vb)

        image_lut = pg.HistogramLUTItem()
        image_lut.setImageItem(self.image_ii)
        image_lut.plot.setData([])

        widget.addItem(image_lut)

        self._layout.addWidget(widget)

        pxFmtStr = self._props["pxFormat"]

        if pxFmtStr == "Mono8":
            self._pxFmt = np.uint8

        elif pxFmtStr == "Mono16":
            self._pxFmt = np.uint16

        self.active = True
        self._isTheFirstFrame = True

    def close(self):
        dbg("VideoWidget CLOSE")
        self.image_ii.clear()
        self._clearLayout(self._layout)
        self.active = False

    def _clearLayout(self, layout):
        while layout.count():
            item = layout.takeAt(0)
            widget = item.widget()

            if widget is not None:
                widget.deleteLater()

            elif item.layout() is not None:
                self._clearLayout(item.layout())

    def setData(self, data):
        self._f = None

        if self._isRawPublish:
            d = np.frombuffer(data, self._pxFmt)
            self._f = d.reshape((self._h, self._w, self._depth))

        else:
            d = np.frombuffer(data, np.uint8)
            self._f = cv.imdecode(d, cv.IMREAD_UNCHANGED)

        self.image_ii.setImage(image=self._f, autoLevels=self._isTheFirstFrame)

        if self._isTheFirstFrame:
            #print(f"M={np.max(f)}, m={np.min(f)}")
            self._isTheFirstFrame = False

    def getCurrentFrame(self):
        return self._f

    def saveCurrentFrame(self, path="./output.png"):
        if self._f is None:
            err("Frame is NOT valid")
            return False

        if not path.lower().endswith(".png"):
            err("Frame can be saved ONLY as PNG")
            return False

        f = self._f = cv.cvtColor(self._f, cv.COLOR_RGB2BGR)
        cv.imwrite(path, f,  [cv.IMWRITE_PNG_COMPRESSION, 9])
        dbg("Curent frame SAVED: {}".format(path))
        return True

    def isOpen(self):
        return self.active
