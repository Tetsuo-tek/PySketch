from PySide6.QtWidgets import QPlainTextEdit, QFrame
from PySide6.QtGui import QTextCursor

import json

class LoggerWidget(QPlainTextEdit):
    def __init__(self, parent=None):
        super().__init__(parent)
        
        # Configura lo stile di base del widget
        self.setReadOnly(True)
        self.setStyleSheet("""
            QPlainTextEdit {
                background-color: black;
                color: green;
                font-family: monospace;
                font-size: 10px;
            }
        """)

        self.setFrameShape(QFrame.Panel)
        self.setFrameShadow(QFrame.Sunken)
        
    def _append(self, message, color=None):
        if color is None:
            self.appendPlainText(message)

        else:
            self.appendHtml("<span style=\"color:{}\">{}</span>".format(color, message))

        self.moveCursor(QTextCursor.End)

    def append(self, txt):
        pack = json.loads(txt)
        t = pack["log_T"]
        message = pack["msg"]

        color = None

        if t == "M":
            color = "green"

        elif t == "W":
            color = "yellow"

        elif t == "E":
            color = "red"

        elif t == "F":
            color = "darkred"

        elif t == "D":
            color = "darkCyan"

        self._append(message, color)


