###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import cv2                  as cv

from picamera2              import Picamera2

from PySketch.signal        import functSignature
from PySketch.elapsedtime   import ElapsedTime
from PySketch.log           import msg, dbg, wrn, err, cri, printPair

class PiCamera():
    def __init__(self):
        self._cap = None
        self._w = -1
        self._h = -1
        self._fps = -1
        self._t = -1
        self._f = None
        self._chrono = ElapsedTime()
        self._fpsAvg = -1
        self._fpsCount = -1
        self._onCapTickCb = None
        self._capTimeChrono = ElapsedTime()
        self._capTime = 0.0
        self._lastTickTime = 0
        self._master = None
        self._slave = None
        self._controls = {}

    def init(self, res, fps, onCapTickCb):
        if self._cap is not None:
            err("PiCamera is ALREADY open")
            return False

        self._fps = fps
        self._t = 1.0 / self._fps
        self._fpsAvg = 0.0
        self._fpsCount = 0
        self._w = int(res[0])
        self._h = int(res[1])

        try:

            self._cap = Picamera2()
            #msg("PiCamera modes: {}".format(self._cap.sensor_modes))
            for mode in self._cap.sensor_modes:
                msg("PiCamera mode: {}".format(mode))

            #camera_config = self._cap.create_video_configuration(raw=self._cap.sensor_modes[4])
            #camera_config = self._cap.create_video_configuration({"size": (self._w, self._h)}, raw={"format": "SRGGB10", "size": (2032, 1520)}, buffer_count=4)
            #camera_config = self._cap.create_video_configuration({"size": (self._w, self._h)})
            #self._cap.configure(camera_config)
            
            #self._cap.controls.FrameRate = self._fps

            #self._cap.controls.ScalerCrop = (0, 0, 3280, 2464) # Dimensioni dell'intero sensore (per Raspberry Pi Camera v2)
            
            self._cap.resolution = (self._w, self._h)
            self._cap.controls.FrameRate = self._fps

            self._controls = self.properties()["controls"]
            self._cap.start()

        except Exception as e:
            err("CANNOT open PiCamera - {}".format(e))
            return False

        if self._cap.camera is None:
            err("CANNOT enable PiCamera")
            return False
            
        if onCapTickCb is None:
            wrn("OnCaptureTick Callback DISABLED")
        
        else:
            self._onCapTickCb = onCapTickCb
            dbg("OnCaptureTick Callback ENABLED: {}{}".format(self._onCapTickCb.__name__, functSignature(self._onCapTickCb)))

        msg("PiCamera ENABLED [Res: {}x{}; FPS: {}; t: {} s]".format(self._w, self._h, self._fps, self._t))
        return True

    def setAsSynchMaster(self, master):
        self._master = master
        msg("Camera is MASTER")

    def setAsSynchSlave(self, slave):
        self._slave = slave
        msg("Camera is SLAVE")

    # IT WILL NOT SLOW DOWN TO self._t THE TICK (NO WAIT OTHER THAN CAPTURING JOB TIME)
    def tick(self):
        if self._cap is None:
            err("PiCamera is NOT open yet")
            return False

        self._checkSynch()

        self._lastTickTime = self._chrono.stop()
        self._capTimeChrono.start()

        try:
            self._f = self._cap.capture_array()

        except Exception as e:
            err("CANNOT capture frame from PiCamera - {}".format(e))
            return False

        self._chrono.start()
        self._capTime = self._capTimeChrono.stop()

        self._checkSynch()

        self._f = cv.cvtColor(self._f, cv.COLOR_RGB2BGR)

        if self._onCapTickCb is not None:
            self._onCapTickCb(self._f)

        return True

    def _checkSynch(self):
        if self._master is not None:
            self._master.release()
            
        elif self._slave is not None:
            self._slave.wait()

    def quit(self):
        if self._cap is None:
            err("PiCamera is NOT open yet")
            return False

        self._cap.close()
        self._cap = None

        return True

    def setControlValue(self, name, value):
        if name not in self._controls:
            err("Control NOT found: {}".format(name))
            return False

        ctrl = self._controls[name]
        t = type(value).__name__

        if t != ctrl["t"]:
            err("Control '{}' value is NOT valid [must be '{}']: {}".format(name, ctrl["t"], t))
            return False

        try:
            self._cap.set_controls({name : value})

        except Exception as e:
            err("CANNOT set control value on '{}' control - {}".format(name,  e))
            return False

        return True

    def getControlValue(self, name):
        if name not in self._controls:
            err("Control NOT found: {}".format(name))
            return None

        return self._cap.camera_controls[name][2]

    def hasControl(self, name):
        return name in self._controls

    def properties(self):
        ctrls = {}

        for name, value in self._cap.camera_controls.items():
            if name == "FrameDurationLimits" or name == "ColourGains" or name == "ScalerCrop":
                continue

            minVal, maxVal, currentVal = value
            t = type(minVal)

            ctrls[name] = {
                "t" : t.__name__,
                "min" : minVal,
                "max" : maxVal,
                "curr" : currentVal
            }

            msg("Checked PiCamera CTRL -> name: {}; t: {}; min: {}; max: {}; current: {}"
                .format(
                    name,
                    t.__name__,
                    minVal,
                    maxVal,
                    currentVal
                )
            )

        return {
            "driver" : self.__class__.__name__,
            "w" : self._w,
            "h" : self._h,
            "fps" : self._fps,
            "interval" : self._t,
            "controls" : ctrls
        }
    
###############################################################################
