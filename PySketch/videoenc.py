###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import struct

import cv2      as cv
import numpy    as np

from PySketch.log   import msg, dbg, wrn, err, cri

###############################################################################
# CLASSes

class BlocksLayer:
    def __init__(self, level, x, y, w, h, minBlkSide):
        self._level = level
        self._divider = 2
        self._minBlkSide = minBlkSide

        self._x = x
        self._y = y

        self._w = w
        self._h = h

        self._blocks = np.empty((self._divider, self._divider), dtype=object)
        self._count = self._divider * self._divider

        self._blk_W = int(self._w/self._divider)
        self._blk_H = int(self._h/self._divider)

        '''
        print("##")
        print("Layer depth: {}".format(self._level))
        print("Origin: ({},{})".format(self._x, self._y))
        print("Frame size: {}x{}".format(self._w, self._h))
        print("Block size: {}x{}".format(self._blk_W, self._blk_H))
        print("Blocks count: {}\n".format(self._count))
        '''
        col = 0
        row = 0

        for y in range(self._y, self._y+self._h, self._blk_H):
            for x in range(self._x, self._x+self._w, self._blk_W):
                self._blocks[col, row] = Block(self._level, x, y, self._blk_W, self._blk_H, self._minBlkSide)
                col += 1
                if col == self._divider:
                    col = 0
                    row += 1

    def check(self, gray, threshold, changed):
        for row in range(0, self._divider):
            for col in range(0, self._divider):
                blk = self._blocks[col, row]
                subBlocks = []
                if blk.check(gray, threshold, subBlocks):
                    changed.extend(subBlocks)

    def block_W(self):
        return self._blk_W

    def block_H(self):
        return self._blk_H

class Block:
    def __init__(self, level, x, y, w, h, minBlkSide):
        self._level = level
        self._minBlkSide = minBlkSide

        self._x = x
        self._y = y

        self._w = w
        self._h = h

        self._pixelsCount = self._w * self._h
        self._maxValue = self._pixelsCount * 255

        self._pt1 = (self._x, self._y)
        self._pt2 = (self._x + self._w, self._y + self._h)

        #print("Block [#{}]: pt1->{} - pt2->{} - sz->{}x{}".format(self._level, self._pt1, self._pt2, self._w, self._h))
        
        self._curr_ROI = np.zeros((self._h, self._w, 1), np.uint8)
        self._prev_ROI = np.zeros((self._h, self._w, 1), np.uint8)

        self._kernel = np.ones((3,3), np.uint8)

        self._blk_RGB = np.zeros((self._h, self._w, 3), np.uint8)

        #self._prevCount = 0
        self._lastAvg = 0
        self._lastAvgDiff = 0
        self._changeIndex = 0

        if self._w > self._minBlkSide:
            self._subTree = BlocksLayer(self._level+1, self._x, self._y, self._w, self._h, self._minBlkSide)

        else:
            self._subTree = None

    def check(self, gray, threshold, subBlocks):
        '''
        self._curr_ROI = gray[self._y : self._y + self._h, self._x : self._x + self._w]
        self._difference = cv.absdiff(self._prev_ROI, self._curr_ROI)
        self._difference = cv.dilate(self._difference, self._kernel, iterations=2)
        avg = cv.mean(self._difference)[0]

        self._lastAvgDiff = int(abs(avg - self._lastAvg))

        if self._lastAvgDiff > 20:
            subBlocks.append(self)

        elif self._subTree is not None:
            #if self._lastAvgDiff >= 2:
            self._subTree.check(gray, threshold, subBlocks)

        self._lastAvg = avg
        '''
        #'''
        self._changeIndex = self._calculateDifference(gray, threshold)
        
        if self._changeIndex > 0.05:
            subBlocks.append(self)

        elif self._subTree is not None:
            if self._changeIndex > 0.0001:
                self._subTree.check(gray, threshold, subBlocks)
        #'''

        self._prev_ROI = self._curr_ROI.copy()
        return len(subBlocks)

    def _calculateDifference(self, gray, threshold):
        self._curr_ROI = gray[self._y : self._y + self._h, self._x : self._x + self._w]

        self._difference = cv.absdiff(self._prev_ROI, self._curr_ROI)
        _, self._difference = cv.threshold(self._difference, threshold, 255, cv.THRESH_BINARY)
        self._difference = cv.dilate(self._difference, self._kernel, iterations=1)

        return np.sum(self._difference) / self._maxValue

    def origin(self):
        return self._pt1

    def diff_GRAY(self):
        return self._difference

class PsyvEncoder:
    def __init__(self, w, h, fps, minBlkSide):
        self._w = w
        self._h = h
        self._fps = fps
        self._minBlkSide = minBlkSide

        self._maxSide = self._w
        
        if self._w < self._h:
            self._maxSide = self._h

        self._canvasSide = self._minBlkSide

        self._levels = -1

        while self._canvasSide < self._maxSide:
            self._canvasSide *= 2
            self._levels += 1

        self._canvasSide /= 2

        self._canvasSide = int(self._canvasSide)
        self._layersTreeRoot_Y  = BlocksLayer(1, 0, 0, self._canvasSide, self._canvasSide, self._minBlkSide)

        self._ratio_W = self._w / self._canvasSide
        self._ratio_H = self._h / self._canvasSide

        msg("Current resolution: {}x{}".format(self._w, self._h))
        msg("Computing resolution: {}x{}".format(self._canvasSide, self._canvasSide))
        msg("Minimim quad-block: {}x{}".format(self._minBlkSide, self._minBlkSide))
        msg("Hierarchy levels: {}".format(self._levels))
        msg("Ratios W/H: {}/{}".format(self._ratio_W, self._ratio_H))
        msg("FPS: {}".format(self._fps))

    def encode(self, f):
        self._f = f
        self._curr_YUV = cv.cvtColor(self._f, cv.COLOR_BGR2YUV)
        self._curr_Y = self._curr_YUV[:,:,0]
        self._curr_Y = cv.resize(self._curr_Y, (self._canvasSide, self._canvasSide), interpolation=cv.INTER_NEAREST)

        changed = []
        self._layersTreeRoot_Y.check(self._curr_Y, int(cv.mean(self._curr_Y)[0]*0.4), changed)
        return changed

    def produce(self, blk, compressionParams):
        pt1 = blk.origin()
        pt2 = blk._pt2

        pt1_TEMP = (int(pt1[0] * self._ratio_W), int(pt1[1] * self._ratio_H))
        pt2_TEMP = (int(pt2[0] * self._ratio_W), int(pt2[1] * self._ratio_H))

        blkFrameData = self._f[pt1_TEMP[1] : pt2_TEMP[1], pt1_TEMP[0] : pt2_TEMP[0]]
        ok, jpg = cv.imencode('.jpg', blkFrameData, compressionParams)

        if not ok:
            err("Image encoding error")
            return bytearray()

        blkData = bytearray(jpg)

        dataSegment = bytearray()
        dataSegment.extend(struct.pack("<H", pt1_TEMP[0]))
        dataSegment.extend(struct.pack("<H", pt1_TEMP[1]))
        dataSegment.extend(struct.pack("<I", len((blkData))))
        dataSegment.extend(blkData)

        return dataSegment

    def getBlockCanvas(self, blk):
        pt1 = blk.origin()
        pt2 = blk._pt2

        pt1_TEMP = (int(pt1[0] * self._ratio_W), int(pt1[1] * self._ratio_H))
        pt2_TEMP = (int(pt2[0] * self._ratio_W), int(pt2[1] * self._ratio_H))

        w = pt2_TEMP[0] - pt1_TEMP[0]
        h = pt2_TEMP[1] - pt1_TEMP[1]

        return (pt1_TEMP, pt2_TEMP, w, h)

    def getBlockDifference(self, blk):
        pt1_TEMP, pt2_TEMP, w, h = self.getBlockCanvas(blk)
        diff = cv.resize(blk.diff_GRAY(), (w, h), interpolation=cv.INTER_NEAREST)
        diff_RGB = cv.cvtColor(diff, cv.COLOR_GRAY2BGR)
        return (pt1_TEMP, pt2_TEMP, w, h, diff_RGB)


