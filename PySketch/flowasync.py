###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import ctypes
import sys
import struct
import json
import inspect

from collections            import deque

from PySketch.flowproto     import FlowResponse, FlowCommand, Flow_T, FlowChanID, FlowChannel_T, Variant_T
from PySketch.abstractflow  import FlowChannel, AbstractFlow, ProtocolRecvError
from PySketch.signal        import functSignature, Signal, emit
from PySketch.log           import msg, dbg, wrn, err, cri, printPair

###############################################################################

class FlowChannelData():
    def __init__(self):
        self.chanID = -1
        self.data = b""

###############################################################################

class FlowAsync(AbstractFlow):
    def __init__(self, loop) :
        super(FlowAsync, self).__init__(loop)

        self._maxDataQueueCount = 1000
        self._subscribedChannels = []
        self._channelsData = deque()
        self._currentData = FlowChannelData()
        self._lastResponse = FlowResponse.FRSP_NORSP
        self._currentDbName = ""

        self._onNewChanCb           = None
        self._onDelChanCb           = None
        self._onChanHeaderChangedCb = None
        self._onStartChanPubReqCb   = None
        self._onStopChanPubReqCb    = None
        self._onGrabDataCb          = None
        self._onServiceRequest      = None
        self._onServiceResponse     = None

        #self._loop = None
        self.channelAdded           = Signal(self._loop)
        self.channelRemoved         = Signal(self._loop)
        self.channelHeaderChanged   = Signal(self._loop)
        self.publishStartRequired   = Signal(self._loop)
        self.publishStopRequired    = Signal(self._loop)
        self.subscribedDataCome     = Signal(self._loop)
        self.serviceRequest         = Signal(self._loop)
        self.serviceResponse        = Signal(self._loop)

###############################################################################

    '''
    # FlowSat start this meth, because it set the LOOP here
    def _initSignals(self, loop):
        self._loop = loop

        self.channelAdded           = Signal(self._loop)
        self.channelRemoved         = Signal(self._loop)
        self.channelHeaderChanged   = Signal(self._loop)
        self.publishStartRequired   = Signal(self._loop)
        self.publishStopRequired    = Signal(self._loop)
        self.subscribedDataCome     = Signal(self._loop)
        self.serviceRequest         = Signal(self._loop)
        self.serviceResponse        = Signal(self._loop)
    '''

    def setNewChanCallBack(self, cb):
        self._onNewChanCb = cb
        dbg("Callback ENABLED: {}{}".format(cb.__name__, functSignature(cb)))

    def setDelChanCallBack(self, cb):
        self._onDelChanCb = cb
        dbg("Callback ENABLED: {}{}".format(cb.__name__, functSignature(cb)))

    def setChanHeaderCallBack(self, cb):
        self._onChanHeaderChangedCb = cb
        dbg("Callback ENABLED: {}{}".format(cb.__name__, functSignature(cb)))

    def setStartChanPubReqCallBack(self, cb):
        self._onStartChanPubReqCb = cb
        dbg("Callback ENABLED: {}{}".format(cb.__name__, functSignature(cb)))

    def setStopChanPubReqCallBack(self, cb):
        self._onStopChanPubReqCb = cb
        dbg("Callback ENABLED: {}{}".format(cb.__name__, functSignature(cb)))

    def setGrabDataCallBack(self, cb):
        self._onGrabDataCb = cb
        dbg("Callback ENABLED: {}{}".format(cb.__name__, functSignature(cb)))

    def setRequestCallBack(self, cb):
        self._onServiceRequest = cb
        dbg("Callback ENABLED: {}{}".format(cb.__name__, functSignature(cb)))

    def setResponseCallBack(self, cb):
        self._onServiceResponse = cb
        dbg("Callback ENABLED: {}{}".format(cb.__name__, functSignature(cb)))

###############################################################################

    def addStreamingChannel(self, flow_t, t, name, mime="", props={}):
        if (self._sck is None) or (not self._sck.isConnected()):
            err("SocketDevice is NOT initialized yet")
            return False

        self._p.sendStartOfTransaction(FlowCommand.FCMD_ADD_STREAMING_CHAN)

        self._p.sendFlowType(flow_t)
        self._p.sendVariantType(t)
        self._p.sendString(name)

        if len(mime) == 0:
            self._p.sendSize(0)

        else:
            self._p.sendString(mime)

        if len(props) == 0:
            self._p.sendSize(0)

        else:
            self._p.sendJSON(props)

        self._p.sendEndOfTransaction()

        # NO RESPONSE
        msg("REQUESTED to ADD a new StreamingChannel: {} -> props: {}".format(name, props))
        return True
    
    def addServiceChannel(self, name):
        if (self._sck is None) or (not self._sck.isConnected()):
            err("SocketDevice is NOT initialized yet")
            return False
        
        l = []
        l.append(name)

        self._p.sendStartOfTransaction(FlowCommand.FCMD_ADD_SERVICE_CHAN)
        self._p.sendJSON(l)
        self._p.sendEndOfTransaction()

        # NO RESPONSE
        msg("REQUESTED to ADD a new ServiceChannel: {}".format(name))
        return True

    def addBlobChannel(self, name, props={}):
        if (self._sck is None) or (not self._sck.isConnected()):
            err("SocketDevice is NOT initialized yet")
            return False

        self._p.sendStartOfTransaction(FlowCommand.FCMD_ADD_BLOB_CHAN)
        self._p.sendString(name)

        if len(props) == 0:
            self._p.sendSize(0)

        else:
            self._p.sendJSON(props)

        self._p.sendEndOfTransaction()

        # NO RESPONSE
        msg("REQUESTED to ADD a new BlobChannel: {}".format(name))
        return True
        
    def removeChannel(self, chanID):
        if (self._sck is None) or (not self._sck.isConnected()):
            err("SocketDevice is NOT initialized yet")
            return False
        
        if chanID not in self._channelsIndexes:
            err("Channel NOT found: {}".format(chanID))
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_DEL_CHAN)
        self._p.sendChanID(chanID)
        self._p.sendEndOfTransaction()

        # NO RESPONSE
        msg("REQUESTED to REMOVE a channel: {}".format(self._channelsIndexes[chanID].name))
        return True

    def setChannelHeader(self, chanID, data):
        if (self._sck is None) or (not self._sck.isConnected()):
            err("SocketDevice is NOT initialized yet")
            return False
        
        if chanID not in self._channelsIndexes:
            err("Channel NOT found: {}",format(chanID))
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_SET_CHAN_HEADER)
        self._p.sendChanID(chanID)
        self._p.sendBuffer(data)
        self._p.sendEndOfTransaction()

        msg("Setup HEADER [{} B] for a channel: {}".format(len(data), self._channelsIndexes[chanID].name));
        return True
    
###############################################################################

    def attach(self, sourceID, targetID):
        if sourceID not in self._channelsIndexes:
            err("Channel NOT found: {}".format(sourceID))
            return False
        
        if targetID not in self._channelsIndexes:
            err("Channel NOT found: {}".format(targetID))
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_ATTACH_CHAN)
        self._p.sendChanID(sourceID)
        self._p.sendChanID(targetID)
        self._p.sendEndOfTransaction()

        # NO RESPONSE
        msg("REQUESTED to ATTACH channels: {} -> {}".format(self._channelsIndexes[sourceID].name, self._channelsIndexes[targetID].name))
        return True
    
    def detach(self, sourceID, targetID):
        if sourceID not in self._channelsIndexes:
            err("Channel NOT found: {}".format(sourceID))
            return False
        
        if targetID not in self._channelsIndexes:
            err("Channel NOT found: {}".format(targetID))
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_ATTACH_CHAN)
        self._p.sendChanID(sourceID)
        self._p.sendChanID(targetID)
        self._p.sendEndOfTransaction()

        # NO RESPONSE
        msg("REQUESTED to DETACH channels: {} -> {}".format(self._channelsIndexes[sourceID].name, self._channelsIndexes[targetID].name))
        return True

###############################################################################

    def sendServiceRequest(self, chanID, cmd, val):
        if (self._sck is None) or (not self._sck.isConnected()):
            err("SocketDevice is NOT initialized yet")
            return False
        
        if chanID not in self._channelsIndexes:
            err("Channel NOT found: {}".format(chanID))
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_EXEC_SERVICE_REQUEST)
        self._p.sendChanID(chanID)
        self._p.sendString(cmd)
        self._p.sendJSON(val)
        self._p.sendEndOfTransaction()

        # NO RESPONSE
        dbg("Channel ASYNC-REQUEST sent: {}".format(self._channelsIndexes[chanID].name))
        return True
    
    def sendServiceResponse(self, chanID, hash, val):
        self._p.sendStartOfTransaction(FlowCommand.FCMD_RETURN_SERVICE_RESPONSE)
        self._p.sendChanID(chanID)
        self._p.sendString(hash)
        self._p.sendJSON(val)
        self._p.sendEndOfTransaction()

###############################################################################

    def publish(self, chanID, b):
        return self._publishPacketizedData(chanID, b)
    
    def publishString(self, chanID, text):        
        return self._publishPacketizedData(chanID, text.encode('utf-8'))
    
    def publishJSON(self, chanID, val):
        try:
            text = json.dumps(val, ensure_ascii=False)

        except json.JSONEncodeError as e:
            err("Error during JSON encoding:", e)
            return False
        
        return self._publishPacketizedData(chanID, text.encode('utf-8'))

    def publishUInt8(self, chanID, val):
        return self._publishPacketizedData(chanID, struct.pack("<B", val))

    def publishInt8(self, chanID, val):
        return self._publishPacketizedData(chanID, struct.pack("<b", val))

    def publishUInt16(self, chanID, val):
        return self._publishPacketizedData(chanID, struct.pack("<H", val))

    def publishInt16(self, chanID, val):
        return self._publishPacketizedData(chanID, struct.pack("<h", val))

    def publishUInt32(self, chanID, val):
        return self._publishPacketizedData(chanID, struct.pack("<I", val))

    def publishInt32(self, chanID, val):
        return self._publishPacketizedData(chanID, struct.pack("<i", val))

    def publishUInt64(self, chanID, val):
        return self._publishPacketizedData(chanID, struct.pack("<Q", val))

    def publishInt64(self, chanID, val):
        return self._publishPacketizedData(chanID, struct.pack("<q", val))

    def publishFloat(self, chanID, val):
        return self._publishPacketizedData(chanID, struct.pack("<f", val))

    def publishDouble(self, chanID, val):
        return self._publishPacketizedData(chanID, struct.pack("<d", val))

###############################################################################

    def _publishPacketizedData(self, chanID, data):
        if self._sck is None: # or not self._sck.isConnected():
            err("SocketDevice is NOT initialized yet")
            return False

        while not self._sck.canWrite():
            wrn("Socket is NOT ready to write (too much data for this tick interval), retrying to publish [chanID: {}]".format(chanID))
            if not self._sck.isConnected():
                return False;

        # NO RESPONSE
        self._p.sendStartOfTransaction(FlowCommand.FCMD_PUBLISH)
        self._p.sendChanID(chanID)
        self._p.sendBuffer(data)
        return self._p.sendEndOfTransaction()
        
###############################################################################

    def isSubscribed(self, chanID):
        return (chanID in self._subscribedChannels)

    def subscribeChannel(self, chanID):
        if chanID not in self._channelsIndexes:
            err("Channel NOT FOUND: {}".format(chanID))
            return False

        if chanID in self._subscribedChannels:
            err("Channel is ALREADY subscribed: {}".format(self._channelsIndexes[chanID].name))
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_SUBSCRIBE_CHAN)
        self._p.sendChanID(chanID)
        self._p.sendEndOfTransaction()

        # NO RESPONSE
        msg("Sent SUBSCRIBE for a channel: {}".format(self._channelsIndexes[chanID].name))
        return True

    def unsubscribeChannel(self, chanID):
        if chanID not in self._channelsIndexes:
            err("Channel NOT FOUND: {}".format(chanID))
            return False
            
        if chanID not in self._subscribedChannels:
            err("Channel is ALREADY subscribed: {}".format(self._channelsIndexes[chanID].name))
            return False
        
        self._p.sendStartOfTransaction(FlowCommand.FCMD_UNSUBSCRIBE_CHAN)
        self._p.sendChanID(chanID)
        self._p.sendEndOfTransaction()
        
        # NO RESPONSE
        msg("Sent UNSUBSCRIBE for a channel: {}".format(self._channelsIndexes[chanID].name))
        return True

    def hasNextData(self):
        return len(self._channelsData) > 0

    def nextData(self):
        if not self.hasNextData():
            return False
        
        self._currentData = self._channelsData.popleft()
        return True

    def getCurrentData(self):
        return self._currentData
    
###############################################################################

    def getCurrentDbName(self):
        return self._currentDbName

###############################################################################

    def _onOpenUnixSocket(self):
        return True

    def _onOpenTcpSocket(self):
        return True
    
    def _onLogin(self):
        self._p.sendStartOfTransaction(FlowCommand.FCMD_SET_ASYNC)
        self._p.sendEndOfTransaction();

        # NO RESPONSE
        return True

    def _onOpen(self):
        pass

    def _onClose(self):
        pass

    def _onDisconnected(self):
        self._subscribedChannels = []
        self._channelsData = deque()
        self._currentData = FlowChannelData()
        self._lastResponse = FlowResponse.FRSP_NORSP

###############################################################################

    def pulse(self):
        if self._sck is None:
            return
        
        if not self._sck.isConnected():
            self._onDisconnected()
            self._sck = None
            return
        
        self._sck.waitForReadyRead()

        if self._sck.bytesAvailable() > 0:
            self._analyze()

            # MUST consume also without cb and slots
            while self.nextData():
                currentData = self.getCurrentData()
                
                if self._onGrabDataCb is not None:
                    self._onGrabDataCb(currentData.chanID, currentData.data)

                emit(self.subscribedDataCome, (currentData.chanID,currentData.data))

###############################################################################

    def _analyze(self):
        '''
        if self._lastResponse == FlowResponse.FRSP_NORSP and self._sck.bytesAvailable() > ctypes.sizeof(ctypes.c_int16):
            self._lastResponse = self._p.recvStartOfRspTransaction()
        '''
        self._lastResponse = self._p.recvStartOfRspTransaction()
        #print(self._lastResponse)
        ##

        if self._lastResponse == FlowResponse.FRSP_CHK_FLOW:
            self._recvCheckFlow()
            
        elif self._lastResponse == FlowResponse.FRSP_CURRENTDB_CHANGED:
            self._recvCurrentDbChanged()

        elif self._lastResponse == FlowResponse.FRSP_CHANNEL_ADDED:
            self._recvChanAdded()

        elif self._lastResponse == FlowResponse.FRSP_CHANNEL_REMOVED:
            self._recvChanRemoved()

        elif self._lastResponse == FlowResponse.FRSP_CHANNEL_HEADER:
            self._recvChanHeader()

        elif self._lastResponse == FlowResponse.FRSP_SEND_RESPONSE_TO_REQUESTER:
            self._recvRspToRequester()

        elif self._lastResponse == FlowResponse.FRSP_SEND_REQUEST_TO_SERVICE:
            self._recvReqToService()

        elif self._lastResponse == FlowResponse.FRSP_CHANNEL_PUBLISH_START:
            self._recvPubStartReq()

        elif self._lastResponse == FlowResponse.FRSP_CHANNEL_PUBLISH_STOP:
            self._recvPubStopReq()

        elif self._lastResponse == FlowResponse.FRSP_SUBSCRIBED_DATA:
            self._recvSubscribedData()

        else:
            ProtocolRecvError(self._sck, "Command UNKNOWN: {}".format(self._lastResponse))
            
###############################################################################
# FRSP_CHK_FLOW

    def _recvCheckFlow(self):
        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return
    
        self._lastResponse = FlowResponse.FRSP_NORSP

###############################################################################
# FRSP_CURRENTDB_CHANGED

    def _recvCurrentDbChanged(self):
        sz = self._p.recvSize()
        self._currentDbName = self._p.recvString(sz)

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return

        self._lastResponse = FlowResponse.FRSP_NORSP

###############################################################################
# FRSP_CHANNEL_ADDED

    def _recvChanAdded(self):
        ch = FlowChannel()
        ch.isPublishingEnabled = False

        ch.chan_t = self._p.recvChanType()
        ch.chanID = self._p.recvChanID()

        sz = self._p.recvSize()
        ch.name = self._p.recvString(sz)
        #print("!!!!!!!!", ch.name)

        sz = self._p.recvSize()
        ch.hashID = self._p.recvString(sz)
        
        if ch.chan_t == FlowChannel_T.StreamingChannel:
            ch.flow_t = self._p.recvFlowType()
            ch.t = self._p.recvVariantType()

            sz = self._p.recvSize()
            ch.mime = self._p.recvString(sz)

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return

        alreadyExists = ch.chanID in self._channelsIndexes

        if alreadyExists:
            wrn("Channel is ALREADY stored: {} [ChanID: {}]".format(ch.name, ch.chanID))
        
        else:
            self._channelsNames[ch.name] = ch
            self._channelsIndexes[ch.chanID] = ch

        self._lastResponse = FlowResponse.FRSP_NORSP

        if not alreadyExists:
            if self._onNewChanCb is not None:
                self._onNewChanCb(ch)

            emit(self.channelAdded, (ch,))
            
###############################################################################
# FRSP_CHANNEL_REMOVED

    def _recvChanRemoved(self):
        chanID = self._p.recvChanID()

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return
            
        notExists = chanID not in self._channelsIndexes

        if notExists:
            wrn("Channel is NOT stored: [ChanID: {}]".format(chanID))
        
        else:
            ch = self._channelsIndexes[chanID]

            if chanID in self._subscribedChannels:
                wrn("AUTOMATIC UNSUBSCRIBE relative to removed channel: [ChanID: {}; name: {}]".format(ch.chanID, ch.name))
                self._subscribedChannels.remove(chanID)

            del self._channelsNames[ch.name]
            del self._channelsIndexes[ch.chanID]

        self._lastResponse = FlowResponse.FRSP_NORSP

        if not notExists:
            if self._onDelChanCb is not None:
                self._onDelChanCb(ch)

            emit(self.channelRemoved, (ch,))

###############################################################################
# FRSP_CHANNEL_HEADER

    def _recvChanHeader(self):
        chanID = self._p.recvChanID()

        sz = self._p.recvSize()
        header = None

        if sz > 0:
            header = self._p.recvBuffer(sz)

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return

        notExists = chanID not in self._channelsIndexes

        if notExists:
            wrn("Channel is NOT stored: [ChanID: {}]".format(chanID))
            self._lastResponse = FlowResponse.FRSP_NORSP
            return
        
        ch = self._channelsIndexes[chanID]
        ch.header = header
        ch.hasHeader = True

        self._lastResponse = FlowResponse.FRSP_NORSP

        if self._onChanHeaderChangedCb is not None:
            self._onChanHeaderChangedCb(chanID, header)

        emit(self.channelHeaderChanged, (chanID, header))

###############################################################################
# FRSP_SEND_RESPONSE_TO_REQUESTER

    def _recvRspToRequester(self):
        chanID = self._p.recvChanID()
        sz = self._p.recvSize()
        val = self._p.recvJSON(sz)

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return

        self._lastResponse = FlowResponse.FRSP_NORSP
        
        if self._onServiceResponse is not None:
            self._onServiceResponse(chanID, val)

        emit(self.serviceResponse, (chanID, val))

###############################################################################
# FRSP_SEND_REQUEST_TO_SERVICE

    def _recvReqToService(self):
        chanID = self._p.recvChanID()

        sz = self._p.recvSize()
        hashStr = self._p.recvString(sz)

        sz = self._p.recvSize()
        cmdName = self._p.recvString(sz)

        sz = self._p.recvSize()
        val = self._p.recvJSON(sz)

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return
            
        self._lastResponse = FlowResponse.FRSP_NORSP

        if self._onServiceRequest is not None:
            self._onServiceRequest(chanID, hashStr, cmdName, val)

        emit(self.serviceRequest, (chanID, hashStr, cmdName, val))

###############################################################################
# FRSP_CHANNEL_PUBLISH_START

    def _recvPubStartReq(self):
        chanID = self._p.recvChanID()

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return

        notExists = chanID not in self._channelsIndexes

        if notExists:
            wrn("Channel is NOT stored: [ChanID: {}]".format(chanID))
        
        else:
            ch = self._channelsIndexes[chanID]
            ch.isPublishingEnabled = True

        self._lastResponse = FlowResponse.FRSP_NORSP

        if not notExists:
            if self._onStartChanPubReqCb is not None:
                self._onStartChanPubReqCb(ch)

            emit(self.publishStartRequired, (ch,))

###############################################################################
# FRSP_CHANNEL_PUBLISH_STOP

    def _recvPubStopReq(self):
        chanID = self._p.recvChanID()

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return

        notExists = chanID not in self._channelsIndexes

        if notExists:
            wrn("Channel is NOT stored: [ChanID: {}]".format(chanID))
        
        else:
            ch = self._channelsIndexes[chanID]
            ch.isPublishingEnabled = False

        self._lastResponse = FlowResponse.FRSP_NORSP
        
        if not notExists:
            if self._onStopChanPubReqCb is not None:
                self._onStopChanPubReqCb(ch)

            emit(self.publishStopRequired, (ch,))

###############################################################################
# FRSP_SUBSCRIBED_DATA

    def _recvSubscribedData(self):
        chanID = self._p.recvChanID()
        
        sz = self._p.recvSize()

        if sz == 0:
            dbg("Receiving subscribed empty data (perhaps it would be a pulse)")
            
        flowData = FlowChannelData()
        flowData.chanID = chanID

        if sz > 0:
            flowData.data = self._p.recvBuffer(sz)

        if not self._p.recvEndOfTransaction():
            ProtocolRecvError(self._sck, "CANNOT RECV recvEndOfTransaction")
            return
        
        self._channelsData.append(flowData)
        self._lastResponse = FlowResponse.FRSP_NORSP

###############################################################################

