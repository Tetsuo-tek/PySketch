###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import ctypes
import json
import struct

from PySketch.socketdevice  import SocketDevice
from PySketch.log           import msg, dbg, wrn, err, cri, printPair

from enum                   import Enum
from typing                 import NewType, Any

FlowChanID = NewType('FlowChanID', ctypes.c_int16)

class FlowChannel_T(Enum):
    ChannelNotValid = 0
    StreamingChannel = 1
    ServiceChannel = 2
    BlobChannel = 3
    
class Variant_T(Enum):
    T_NULL = 0
    T_BOOL = 1
    T_INT8 = 2
    T_UINT8 = 3
    T_INT16 = 4
    T_UINT16 = 5
    T_INT32 = 6
    T_UINT32 = 7
    T_FLOAT = 8
    T_INT64 = 9
    T_UINT64 = 10
    T_DOUBLE = 11
    T_BYTEARRAY = 12
    T_STRING = 13
    T_LIST = 14
    T_PAIR = 15
    T_MAP = 16
    T_RAWPOINTER = 17

class FlowCommand(Enum):
    FCMD_NOCMD = 0

    FCMD_GET_LOGIN_SEED = 1                 # ONLY SYNC
    FCMD_LOGIN = 2                          # ONLY SYNC

    FCMD_SET_ASYNC = 3                      # ONLY SYNC (ASYNC STARTs AS SYNC)
    FCMD_CHK_SERVICE = 4                    # ONLY ASYNC

    FCMD_EXISTS_DATABASE = 5                # ONLY SYNC
    FCMD_ADD_DATABASE = 6                   # ONLY SYNC
    FCMD_DEL_DATABASE = 7                   # ONLY SYNC
    FCMD_LOAD_DATABASE = 8                  # ONLY SYNC
    FCMD_SAVE_DATABASE = 9                  # ONLY SYNC
    FCMD_EXISTS_DATABASE_PROPERTY = 10      # ONLY SYNC 
    FCMD_SET_DATABASE_PROPERTY = 11         # *
    FCMD_SET_DATABASE_PROPERTY_JSON = 12    # *
    FCMD_GET_DATABASE_PROPERTY = 13         # ONLY SYNC
    FCMD_GET_DATABASE_PROPERTY_JSON = 14    # ONLY SYNC
    FCMD_DEL_DATABASE_PROPERTY = 15         # *
    FCMD_SET_DATABASE = 16                  # *
    FCMD_GET_DATABASE = 17                  # ONLY SYNC
    FCMD_GET_VARIABLES_KEYS = 18            # ONLY SYNC
    FCMD_GET_VARIABLES = 19                 # ONLY SYNC
    FCMD_GET_VARIABLES_JSON = 20            # ONLY SYNC
    FCMD_EXISTS_VARIABLE = 21               # ONLY SYNC
    FCMD_SET_VARIABLE = 22                  # *
    FCMD_SET_VARIABLE_JSON = 23             # *
    FCMD_GET_VARIABLE = 24                  # ONLY SYNC
    FCMD_GET_VARIABLE_JSON = 25             # ONLY SYNC
    FCMD_DEL_VARIABLE = 26                  # *
    FCMD_FLUSHALL = 27                      # *

    FCMD_GET_CHANS_LIST = 28                # ONLY SYNC
    FCMD_GET_CHAN_PROPS = 29                # ONLY SYNC
    FCMD_GET_CHAN_HEADER = 30               # ONLY SYNC
    FCMD_SET_CHAN_HEADER = 31               # ONLY ASYNC

    FCMD_ADD_STREAMING_CHAN = 32            # ONLY ASYNC
    FCMD_ADD_SERVICE_CHAN = 33              # ONLY ASYNC
    FCMD_ADD_BLOB_CHAN = 34                 # ONLY ASYNC

    FCMD_DEL_CHAN = 35                      # ONLY ASYNC
    
    FCMD_ATTACH_CHAN = 36                   # ONLY ASYNC
    FCMD_DETACH_CHAN = 37                   # ONLY ASYNC

    FCMD_EXEC_SERVICE_REQUEST = 38          # *
    FCMD_RETURN_SERVICE_RESPONSE = 39       # ONLY ASYNC

    FCMD_GRAB_REGISTER_CHAN = 40            # ONLY SYNC
    FCMD_GRAB_UNREGISTER_CHAN = 41          # ONLY SYNC
    FCMD_GRAB_LASTDATA_CHAN = 42            # ONLY SYNC

    FCMD_PUBLISH = 43                       # ONLY ASYNC

    FCMD_SUBSCRIBE_CHAN = 44                # ONLY ASYNC
    FCMD_UNSUBSCRIBE_CHAN = 45              # ONLY ASYNC

    FCMD_ADD_USER = 46                      # ONLY ASYNC (ADMIN)
    FCMD_DEL_USER = 47                      # ONLY ASYNC (ADMIN)
    FCMD_SET_USER_PERMISSION = 48           # ONLY ASYNC (ADMIN)
    FCMD_GET_USER_PERMISSIONS = 49          # ONLY ASYNC 

    FCMD_KILL_USER = 50                     # ONLY ASYNC (ADMIN)
    FCMD_KILL_CONNECTION = 51               # ONLY ASYNC (ADMIN)

    FCMD_QUIT = 52

class FlowResponse(Enum):
    FRSP_NORSP = 0

    FRSP_CHK_FLOW = 1

    # SENT ONLY TO THE CONNECTION THAT CHANGED DB
    FRSP_CURRENTDB_CHANGED = 2

    # SENT ONLY TO ALL
    FRSP_CHANNEL_ADDED = 3
    FRSP_CHANNEL_REMOVED = 4
    FRSP_CHANNEL_HEADER = 5

    # SENT ONLY TO THE SERVICE
    FRSP_SEND_REQUEST_TO_SERVICE = 6
    
    # SENT ONLY TO THE REQUESTER
    FRSP_SEND_RESPONSE_TO_REQUESTER = 7

    # SENT ONLY TO THE CHANNEL-OWNER
    FRSP_CHANNEL_PUBLISH_START = 8
    FRSP_CHANNEL_PUBLISH_STOP = 9

    # SENT ONLY TO ALL SUBSCRIBERS
    FRSP_SUBSCRIBED_DATA = 10

    FRSP_OK = 11
    FRSP_KO = 12

class Flow_T(Enum):
    FT_BLOB = 0

    FT_TICK = 1
    FT_EVENTS = 2
    FT_PAIRS = 3
    FT_LOGS = 4
    FT_DATETIME_CLOCK = 5

    FT_SYSTEM_PARAMETER = 6
    FT_SYSTEM_SHELL = 7

    FT_GPIO_PIN = 8
    FT_SENSOR = 9

    FT_CTRL_PULSE = 10
    FT_CTRL_SWITCH = 11
    FT_CTRL_VALUE = 12

    FT_AUDIO_DATA = 13
    FT_AUDIO_PREVIEW_DATA = 14
    FT_AUDIO_FFT = 15
    FT_AUDIO_VUMETER = 16
    FT_AUDIO_CLIPPING = 17

    FT_SPEECH_TO_TEXT = 18
    FT_TEXT_TO_SPEECH = 19

    FT_VIDEO_DATA = 20
    FT_VIDEO_PREVIEW_DATA = 21

    FT_CV_MOVEMENT = 22
    FT_CV_EDGES = 23
    FT_CV_GESTURES = 24
    FT_CV_POSTURE = 25
    FT_CV_BONES = 26
    FT_CV_CLOUD_3DPOINTS = 27
    FT_CV_OBJECT_DETECTED_FRAME = 28
    FT_CV_OBJECT_DETECTED_BOX = 29
    FT_CV_OBJECT_RECOGNIZED_IDENTITY = 30

    FT_MULTIMEDIA_DATA = 31
    FT_MULTIMEDIA_PREVIEW_DATA = 32

class FlowProto:
    def __init__(self):
        self._currentSendCmd = FlowCommand.FCMD_NOCMD
        self._currentSendRsp = FlowResponse.FRSP_NORSP
        self._currentRecvCmd = FlowCommand.FCMD_NOCMD
        self._currentRecvRsp = FlowResponse.FRSP_NORSP
        self._buffer = bytearray()
        self._sck = None
    
    def setup(self, sck):
        self._sck = sck

###############################################################################
# SEND (buffered)

    def sendStartOfTransaction(self, cmd):
        if len(self._buffer) > 0:
            err("CMD transaction ALREADY open [sending]: {}".format(self.commandToString(self._currentSendCmd)))
            return False
        
        self._currentSendCmd = cmd
        self._buffer += struct.pack('<H', cmd.value)
        #self._buffer += cmd.value.to_bytes(2, byteorder='little')

        return True

    ''' NO OVERLOAD IN PY2
    def sendStartOfTransaction(self, rsp):
        if len(self._buffer) > 0:
            print("RSP transaction ALREADY open [sending]: {}".format(self.responseToString(self._currentSendRsp)))
            return False
        
        self._currentSendRsp = rsp
        self._buffer += struct.pack('<H', rsp.value)
        #self._buffer += rsp.value.to_bytes(2, byteorder='little')

        return True
    '''

    def sendChanType(self, t):
        self._buffer += struct.pack('<H', t.value)
        #self._buffer += t.value.to_bytes(1, byteorder='little')

    def sendFlowType(self, t):
        self._buffer += struct.pack('<H', t.value)
        #self._buffer += t.value.to_bytes(2, byteorder='little')

    def sendVariantType(self, t):
        self._buffer += struct.pack('<H', t.value)
        #self._buffer += t.value.to_bytes(2, byteorder='little')
    
    def sendChanID(self, chanID):
        self._buffer += struct.pack('<H', chanID)
        #self._buffer += chanID.to_bytes(2, byteorder='little')
    
    def sendSize(self, sz):
        self._buffer += struct.pack('<I', sz)
        #self._buffer += sz.to_bytes(4, byteorder='little')
    
    def sendBool(self, val):
        self._buffer += struct.pack('<B', val)
        #self._buffer += val.to_bytes(1, byteorder='little')

    def sendUInt8(self, val):
        self._buffer += struct.pack("<B", val)

    def sendInt8(self, val):
        self._buffer += struct.pack("<b", val)

    def sendUInt16(self, val):
        self._buffer += struct.pack("<H", val)

    def sendInt16(self, val):
        self._buffer += struct.pack("<h", val)

    def sendUInt32(self, val):
        self._buffer += struct.pack("<I", val)

    def sendInt32(self, val):
        self._buffer += struct.pack("<i", val)

    def sendUInt64(self, val):
        self._buffer += struct.pack("<Q", val)

    def sendInt64(self, val):
        self._buffer += struct.pack("<q", val)

    def writeFloat(self, val):
        self._buffer += struct.pack("<f", val)

    def sendDouble(self, val):
        self._buffer += struct.pack("<d", val)

    def sendString(self, text):
        b = text.encode('utf-8')

        sz = len(b)
        self.sendSize(sz)

        if sz == 0:
            return

        self._buffer += b
    
    def sendJSON(self, val):
        # IT WILL NOT ENCODE AS \uXXXX, leaving multichar as they are
        self.sendString(json.dumps(val, ensure_ascii=False))
    
    def sendBuffer(self, b):
        sz = len(b)
        self.sendSize(sz)

        if sz == 0:
            return
        
        self._buffer += b
    
    def sendEndOfList(self):
        val = 0
        self._buffer += struct.pack('<I', val)
        #self._buffer += val.to_bytes(4, byteorder='little')
    
    def sendEndOfMap(self):
        val = 0
        self._buffer += struct.pack('<I', val)
        #self._buffer += val.to_bytes(4, byteorder='little')

    def sendEndOfTransaction(self):
        isConnected = self._sck.isConnected()

        ok = False
        
        if not isConnected:
            err("CANNOT send protocol-transaction [isOpen: {}]".format(isConnected))
        
        else:
            val = 0
            self._buffer += struct.pack('<I', val)
            #self._buffer += val.to_bytes(4, byteorder='little')
            
            ok = self._sck.write(self._buffer)

        self._buffer = bytearray()

        self._currentSendCmd = FlowCommand.FCMD_NOCMD
        self._currentSendRsp = FlowResponse.FRSP_NORSP

        return ok

###############################################################################
# RECV (unbuffered)

    def recvStartOfCmdTransaction(self):
        if self._currentRecvCmd != FlowCommand.FCMD_NOCMD:
            err("CMD transaction ALREADY open [receiving]: {}".format(self.commandToString(self._currentRecvCmd)))
            return FlowCommand.FCMD_NOCMD
        
        cmd = FlowCommand(self._sck.readUInt16())
        self._currentRecvCmd = cmd
        return cmd
    
    def recvStartOfRspTransaction(self):
        if self._currentRecvRsp != FlowResponse.FRSP_NORSP:
            err("RSP transaction ALREADY open [receiving]: {}".format(self.commandToString(self._currentRecvRsp)))
            return FlowResponse.FRSP_NORSP
        
        rsp = FlowResponse(self._sck.readUInt16())
        self._currentRecvRsp = rsp
        return rsp

    def recvChanType(self):
        return FlowChannel_T(self._sck.readUInt8())

    def recvFlowType(self):
        return Flow_T(self._sck.readUInt16())

    def recvVariantType(self):
        return Variant_T(self._sck.readUInt16())

    def recvChanID(self):
        return self._sck.readInt16()

    def recvSize(self):
        return self._sck.readUInt32()

    def recvBool(self):
        return bool(self._sck.readUInt8())

    def recvUInt8(self):
        return self._sck.readUInt8()

    def recvInt8(self):
        return self._sck.readInt8()

    def recvUInt16(self):
        return self._sck.readUInt16()

    def recvInt16(self):
        return self._sck.readInt16()

    def recvUInt32(self):
        return self._sck.readUInt32()

    def recvInt32(self):
        return self._sck.readInt32()

    def recvUInt64(self):
        return self._sck.readUInt64()

    def recvInt64(self):
        return self._sck.readInt64()

    def reacvFloat(self):
        return self._sck.readFloat()

    def recvDouble(self):
        return self._sck.readDouble()

    def recvString(self, sz):
        if sz == 0:
            return ""
        
        return self._sck.readString(sz)
    
    def recvJSON(self, sz):
        text = self.recvString(sz)

        if len(text) == 0:
            return None
    
        try:
            return json.loads(text)

        except json.JSONDecodeError as e:
            err(e)

    def recvBuffer(self, sz):
        if sz == 0:
            return ""
        
        return self._sck.read(sz)
    
    def recvList(self):
        sz = 0
        l = []

        while True:
            sz = self.recvSize()

            if sz > 0:
                item = self.recvString(sz)

                if not item:
                    return l
                
                l.append(item)
            
            else:
                return l                

    def recvEndOfTransaction(self):
        ret = self._sck.readUInt32()

        if ret != 0:
            err("FAILED to RECV EndOfTransaction [CurrentRsp: {}; ret: {}!=0]".format(self.commandToString(self._currentRecvCmd), ret))
            return False

        self._currentRecvCmd = FlowCommand.FCMD_NOCMD
        self._currentRecvRsp = FlowResponse.FRSP_NORSP

        return True

###############################################################################
# REQ

###############################################################################
# ANSWER

###############################################################################

    def commandToString(self, cmd):
        return cmd.name
    
    def commandToBin(self, cmd):
        return FlowCommand[cmd]

    def responseToString(self, rsp):
        return rsp.name
    
    def responseToBin(self, rsp):
        return FlowResponse[rsp]

    def flowTypeToString(self, flow_t):
        return flow_t.name

    def flowTypeToBin(self, flow_t):
        return Flow_T[flow_t]

