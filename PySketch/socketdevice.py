###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import socket
import select
import fcntl
import termios
import sys
import errno
import ctypes
import struct
import array

from PySketch.log           import msg, dbg, wrn, err, cri, printPair

###############################################################################

class SocketDevice():
    def __init__(self):
        self.sock = None

    def setSocket(self, clnt):
        self.sock = clnt
        
    # unix local connection
    def localConnect(self, path):
        try:
            self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
            self.sock.connect(path)
            return True

        except socket.error as e:
            err("Connection failed: {}".format(e))
        
        return False
    
    # tcp/ip connection
    def tcpConnect(self, host, port):
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.connect((host, port))
            self.sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
            return True

        except socket.error as e:
            err("Connection failed: {}".format(e))

        return False
    
    def disconnect(self):
        try:
            if self.sock is not None:
                self.sock.close()
                #self.sock = None

        except socket.error as e:
            err("Disconnect failed: {}".format(e))
        
    def isConnected(self):
        if self.sock is None:
            return False
            
        try:
            data = self.sock.recv(1, socket.MSG_PEEK | socket.MSG_DONTWAIT)
            
            if len(data) == 0 and errno != errno.EAGAIN:
                return False

        except socket.error as e:
            error = e.args[0]

            if error == errno.EAGAIN:
                return True
            
            else:
                err("Socket error: {}".format(str(error)))
                return False
        
        return True
    
###############################################################################

    def canWrite(self):
        try:
            r, w, e = select.select([], [self.sock], [self.sock], 0)
            
            if self.sock in w:
                return True
            
            elif self.sock in e:
                err('Error occurred on socket')
        
        except select.error as e:
            err("Select failed: {}".format(e))

        return False
    
    def waitForReadyRead(self, timeout=0):
        try:
            r, w, e = select.select([self.sock], [], [self.sock], timeout)
            
            if self.sock in r:
                return True
            
            elif self.sock in e:
                err('Error occurred on socket')
        
        except select.error as e:
            err("Select failed: {}".format(e))

        return False

###############################################################################

    def bytesAvailable(self):
        try:
            #bytes_available = fcntl.ioctl(self.sock.fileno(), termios.FIONREAD, bytes(4))
            bytes_available = fcntl.ioctl(self.sock.fileno(), termios.FIONREAD, struct.pack('I', 0))
            return struct.unpack('I', bytes_available)[0]
            #return int.from_bytes(bytes_available, byteorder=sys.byteorder)

        except socket.error as e:
            err("CANNOT get bytes available: {}".format(e))

        return 0
    
###############################################################################

    def readString(self, sz = 0):
        b = self.read(sz)
        if len(b) == 0:
            return ""
        return b.decode('utf-8')

    def writeString(self, text, sz = 0):
        encoded = text.encode('utf-8')
        self.write(encoded, sz)

###############################################################################

    def readBinary(self, count, mode):
        b = self.read(count)

        if len(b) == 0:
            return None

        val, = struct.unpack(mode, b)
        return val

    def readUInt8(self):
        return self.readBinary(1, "<B")

    def readInt8(self):
        return self.readBinary(1, "<b")

    def readUInt16(self):
        return self.readBinary(2, "<H")

    def readInt16(self):
        return self.readBinary(2, "<h")

    def readUInt32(self):
        return self.readBinary(4, "<I")

    def readInt32(self):
        return self.readBinary(4, "<i")
        
    def readUInt64(self):
        return self.readBinary(8, "<Q")

    def readInt64(self):
        return self.readBinary(8, "<q")

    def readFloat(self):
        return self.readBinary(4, "<f")

    def readDouble(self):
        return self.readBinary(8, "<d")

###############################################################################

    def writeUInt8(self, val):
        self.write(struct.pack("<B", val))

    def writeInt8(self, val):
        self.write(struct.pack("<b", val))

    def writeUInt16(self, val):
        self.write(struct.pack("<H", val))

    def writeInt16(self, val):
        self.write(struct.pack("<h", val))

    def writeUInt32(self, val):
        self.write(struct.pack("<I", val))

    def writeInt32(self, val):
        self.write(struct.pack("<i", val))

    def writeUInt64(self, val):
        self.write(struct.pack("<Q", val))

    def writeInt64(self, val):
        self.write(struct.pack("<q", val))

    def writeFloat(self, val):
        self.write(struct.pack("<f", val))

    def writeDouble(self, val):
        self.write(struct.pack("<d", val))

###############################################################################

    def read(self, sz):
        if sz == 0:
            err("Read failed [sz==0]")
            return bytearray()
        
        data = bytearray()
        remaining = sz

        try:
            while remaining > 0:
                chunk = self.sock.recv(remaining)

                if not chunk:
                    raise socket.error("CANNOT read chunk")

                data += chunk
                remaining -= len(chunk)

            return data
        
        except socket.error as e:
            err("Read failed: {}".format(e))

        return bytearray()
    
    def write(self, data):
        try:
            self.sock.sendall(data)
            return True

        except socket.error as e:
            err("Write failed: {}".format(e))
        
        return False

###############################################################################

